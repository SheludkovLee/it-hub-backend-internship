# Задание

### 1. Сделать CRUD для юзера (без подтверждения почты)

### 2. Сделать CRUD для сущности дисциплины по аналогии с ШД. Поля:

    name: string;
    description: string;
    templateDisciplineId: string;
    studyHoursCount: number;
    maxScore: number;
    code: string;
    materials: string;

### 3. При удалении ШД - должна удаляться Д

### 4. В ШД добавить реализацию поля disciplinesCount

### 5. Сделать CRUD для тем внутри дисциплины. Поля:

     name: string;
     order: number;
     methodologicalType: DisciplineTopicMethodologicalType;
     studyHoursCount: number;
     isCheckPoint: boolean;
     maxScore?: number;

### 6. Реализовать репликацию юзеров в контекст learning


