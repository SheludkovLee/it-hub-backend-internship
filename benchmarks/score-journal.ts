import * as autocannon from 'autocannon';

const reqBody = {
  query:
    'query GetTeacherSummaryTasksJournal(\n  $input: GetDisciplineScoreJournalInput!\n  $name: String\n  $outOfGroup: Boolean = false\n) {\n  getDisciplineScoreJournal(input: $input) {\n    learningGroups @skip(if: $outOfGroup) {\n      averageScore\n      discipline {\n        id\n        name\n        __typename\n      }\n      topics {\n        ...LearningGroupTopicFragment\n        __typename\n      }\n      __typename\n    }\n    soloStudents {\n      topics {\n        id\n        topicId\n        status\n        studentId\n        contentBlocks {\n          student {\n            id\n            user {\n              id\n              firstName\n              lastName\n              middleName\n              __typename\n            }\n            __typename\n          }\n          testScore\n          testInterval {\n            from\n            to\n            __typename\n          }\n          contentBlock {\n            ... on TaskDisciplineTopicContentBlock {\n              id\n              name\n              kind\n              __typename\n            }\n            ... on TestDisciplineTopicContentBlock {\n              id\n              name\n              kind\n              canBePassed\n              canBeUpdated\n              __typename\n            }\n            __typename\n          }\n          passDate\n          contentBlockId\n          studentId\n          topicId\n          taskDeadline\n          task {\n            topicId\n            id\n            lastActivityDate\n            lastActivityType\n            scoreInPercent\n            contentBlockId\n            answers {\n              id\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment LearningGroupTopicFragment on LearningGroupTopic {\n  topicId\n  learningGroup {\n    id\n    name\n    __typename\n  }\n  students {\n    studentId\n    status\n    topicId\n    __typename\n  }\n  averageTopicScore\n  contentBlocks {\n    contentBlockId\n    canBeSentAfterDeadline\n    deadline\n    learningGroupId\n    topicId\n    testInterval {\n      from\n      to\n      __typename\n    }\n    students(name: $name) {\n      student {\n        id\n        user {\n          id\n          firstName\n          lastName\n          middleName\n          __typename\n        }\n        __typename\n      }\n      passDate\n      testScore\n      testInterval {\n        from\n        to\n        __typename\n      }\n      taskDeadline\n      contentBlock {\n        ... on TaskDisciplineTopicContentBlock {\n          id\n          name\n          kind\n          __typename\n        }\n        ... on TestDisciplineTopicContentBlock {\n          id\n          name\n          kind\n          canBePassed\n          canBeUpdated\n          __typename\n        }\n        __typename\n      }\n      task {\n        id\n        lastActivityDate\n        lastActivityType\n        scoreInPercent\n        contentBlockId\n        answers {\n          id\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n',
  variables: {
    outOfGroup: false,
    input: {
      disciplineId: 'd361fd26-77f6-474d-bf93-f93f918477b3',
      filters: { query: '' },
    },
    name: '',
  },
  operationName: 'GetTeacherSummaryTasksJournal',
};

async function start() {
  const client = autocannon(
    {
      url: 'http://0.0.0.0:3000/graphql',
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        authorization:
          'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIwNmQ2NzQxOS1jYWY4LTRlZWItYjQxNy1lNjE3OTY2MTM5MDUiLCJpYXQiOjE2OTIyNjU2NzYsImV4cCI6MTY5MjM1MjA3Nn0.CDl182aKQAB8d4rf8pyQk2dO9-KNbnAwTNe_8yYbNDw',
      },
      body: JSON.stringify(reqBody),
      connections: 100,
      duration: 30,
    },
    (err, result) => {
      if (err) {
        console.error(err);
      }

      console.log(result);
    },
  );

  client.on('reqError', console.error);
}

start();
