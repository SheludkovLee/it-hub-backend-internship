import { exec } from 'child_process';
import { config as dotEnvConfig } from 'dotenv';

dotEnvConfig();

async function findBreakingChanges(): Promise<void> {
  const currentEnv = process.env.APP_ENVIRONMENT as string;

  const commands: Record<string, string> = {
    development: 'exit',
    staging:
      'rover graph check ithub-staging@staging --schema ./src/generated-schema.gql',
    preproduction:
      'rover graph check ithub-staging@preprod --schema ./src/generated-schema.gql',
    production: 'exit',
  };

  exec(commands[currentEnv], (err, stdout, stderr) => {
    if (err) {
      throw err;
    }
    if (stdout) {
      console.log(stdout);
    }
    if (stderr) {
      throw stderr;
    }
  });
}

findBreakingChanges();
