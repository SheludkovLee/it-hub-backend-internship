import { Controller, Get } from '@nestjs/common';

import { Public } from './common';

@Public()
@Controller()
export class AppController {
  constructor() {}

  @Get()
  getHello(): string {
    return 'Hello, IThub 👨‍💻';
  }
}
