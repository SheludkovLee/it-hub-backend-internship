import { registerEnumType } from '@nestjs/graphql';

export enum SortType {
  ASC = 'ASC',
  DESC = 'DESC',
}

registerEnumType(SortType, {
  name: 'SortType',
});

export interface PaginationQueryPayload<TFilter = unknown, TSort = unknown> {
  filters?: TFilter;
  sorts?: TSort;
  page: number;
  pageSize: number;
}

export interface PaginatedResponse<TData> {
  items: TData[];
  page: number;
  perPage: number;
  total: number;
  totalPages: number;
  hasMore: boolean;
}
