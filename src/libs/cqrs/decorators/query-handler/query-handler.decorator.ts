import { Constructor } from 'src/libs/types';

import { QueryBase } from '../../base-classes';
import { queryDecoratorKey, queryHandlerDecoratorKey } from '../constants';

export function QueryHandler(query: Constructor<QueryBase>) {
  return (target: Constructor<any>): Constructor<any> => {
    if (!query) {
      throw new Error(`Query for ${target.name} is not provided`);
    }

    const token = Reflect.getMetadata(queryDecoratorKey, query) || '';

    Reflect.defineMetadata(queryHandlerDecoratorKey, token, target);

    return target;
  };
}
