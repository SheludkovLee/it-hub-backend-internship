export const commandDecoratorKey = 'commandToken';
export const queryDecoratorKey = 'queryToken';
export const commandHandlerDecoratorKey = 'listenToCommands';
export const queryHandlerDecoratorKey = 'listenToQueries';
