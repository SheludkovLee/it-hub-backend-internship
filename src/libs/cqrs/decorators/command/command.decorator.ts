import { Constructor } from 'src/libs/types';

import { commandDecoratorKey } from '../constants';

export function Command(token: string) {
  return (target: Constructor<any>): Constructor<any> => {
    if (!token) {
      throw new Error(`${target.name} command token is not provided`);
    }

    Reflect.defineMetadata(commandDecoratorKey, token, target);

    return target;
  };
}
