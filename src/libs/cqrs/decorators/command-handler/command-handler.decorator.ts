import { Constructor } from 'src/libs/types';

import { CommandBase } from '../../base-classes';
import { commandDecoratorKey, commandHandlerDecoratorKey } from '../constants';

export function CommandHandler(command: Constructor<CommandBase>) {
  return (target: Constructor<any>): Constructor<any> => {
    if (!command) {
      throw new Error(`Command for ${target.name} is not provided`);
    }

    const token = Reflect.getMetadata(commandDecoratorKey, command) || '';

    Reflect.defineMetadata(commandHandlerDecoratorKey, token, target);

    return target;
  };
}
