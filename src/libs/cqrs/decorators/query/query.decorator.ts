import { Constructor } from 'src/libs/types';

import { queryDecoratorKey } from '../constants';

export function Query(token: string) {
  return (target: Constructor<any>): Constructor<any> => {
    if (!token) {
      throw new Error(`${target.name} query token is not provided`);
    }

    Reflect.defineMetadata(queryDecoratorKey, token, target);

    return target;
  };
}
