import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { ICommand } from './command.interface';
import { ICommandHandler } from './command-handler.interface';

export interface ICommandBus {
  register(token: string, handler: ICommandHandler): void;
  execute<TResult>(command: ICommand): Promise<Result<TResult, ExceptionBase>>;
}
