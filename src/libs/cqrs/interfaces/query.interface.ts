export interface IQuery<
  Payload extends Record<string, any> = Record<string, any>,
> {
  token: string;
  payload: Payload;
  dateOccurred: string;
}
