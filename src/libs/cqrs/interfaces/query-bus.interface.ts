import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { IQuery } from './query.interface';
import { IQueryHandler } from './query-handler.interface';

export interface IQueryBus {
  register(token: string, handler: IQueryHandler): void;
  execute<TResult>(command: IQuery): Promise<Result<TResult, ExceptionBase>>;
}
