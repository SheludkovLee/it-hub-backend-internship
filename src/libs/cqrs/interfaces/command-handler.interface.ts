import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { ICommand } from './command.interface';

export interface ICommandHandler<
  ReturnType = any,
  Command extends ICommand = ICommand,
> {
  handle(command: Command): Promise<Result<ReturnType, ExceptionBase>>;

  execute(command: Command): Promise<Result<ReturnType, ExceptionBase>>;
}
