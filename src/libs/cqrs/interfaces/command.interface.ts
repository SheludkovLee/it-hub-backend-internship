export interface SerializedCommand<T> {
  token: string;
  dateOccurred: string;
  payload: T;
  trxId: string;
  initiatorUserId?: string;
}

export interface ICommand<
  Payload extends Record<string, any> = Record<string, any>,
> {
  token: string;
  trxId: string;
  dateOccurred: string;
  initiatorUserId?: string;
  payload: Payload;

  toJSON(): SerializedCommand<Payload>;
  setTrxId(trxId: string): void;
}
