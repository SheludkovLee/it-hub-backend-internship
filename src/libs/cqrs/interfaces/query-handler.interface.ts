import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { QueryBase } from '../base-classes';

export interface IQueryHandler<
  Query extends QueryBase = QueryBase,
  ReturnType = any,
> {
  handle(query: Query): Promise<Result<ReturnType, ExceptionBase>>;
}
