import { ExceptionBase, NotFoundException } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { IQuery, IQueryBus, IQueryHandler } from '../../interfaces';

export class QueryBus implements IQueryBus {
  protected subscribers = new Map<string, IQueryHandler>();

  public register(token: string, handler: IQueryHandler): void {
    this.subscribers.set(token, handler);
  }

  public async execute<TResult>(
    query: IQuery,
  ): Promise<Result<TResult, ExceptionBase>> {
    const handler = this.subscribers.get(query.token);

    if (!handler) {
      return Result.fail(
        new NotFoundException(`Query handler for ${query.token} not found`),
      );
    }

    return handler.handle(query);
  }
}
