import * as _ from 'lodash';
import { UnitOfWorkPort } from 'src/libs/database';
import {
  AsyncDomainEventsPublisher,
  DomainEventsPublisher,
} from 'src/libs/domain';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { commandHandlerDecoratorKey } from '../../decorators';
import { ICommand, ICommandBus, ICommandHandler } from '../../interfaces';

export abstract class CommandHandlerBase<
  UnitOfWork extends UnitOfWorkPort,
  Command extends ICommand,
  ReturnType,
> implements ICommandHandler<ReturnType, Command>
{
  protected readonly unitOfWork: UnitOfWork;
  protected readonly domainEventsPublisher: DomainEventsPublisher;
  protected readonly asyncDomainEventsPublisher: AsyncDomainEventsPublisher;
  private readonly commandBus: ICommandBus;

  constructor(
    unitOfWork: UnitOfWork,
    domainEventsPublisher: DomainEventsPublisher,
    asyncDomainEventsPublisher: AsyncDomainEventsPublisher,
    commandBus: ICommandBus,
  ) {
    this.commandBus = commandBus;

    const commandToken = Reflect.getMetadata(
      commandHandlerDecoratorKey,
      this.constructor,
    );

    if (!commandToken) {
      throw new Error(
        `${this.constructor.name} must have '@CommandHandler' decorator`,
      );
    }

    this.commandBus.register(commandToken, this);

    this.unitOfWork = unitOfWork;
    this.domainEventsPublisher = domainEventsPublisher;
    this.asyncDomainEventsPublisher = asyncDomainEventsPublisher;
  }

  public abstract handle(
    command: Command,
  ): Promise<Result<ReturnType, ExceptionBase>>;

  public async execute(
    command: Command,
  ): Promise<Result<ReturnType, ExceptionBase>> {
    const trxId = command.trxId;

    const hasCommands = this.unitOfWork.hasTrx(trxId);

    await this.unitOfWork.startFromId(trxId);

    const allDomainEvents = [];

    const callback = async () => {
      const result = await this.handle(command);

      if (result.isErr) {
        return result;
      }

      const aggregates = this.unitOfWork.getAggregates(trxId);
      const uniqAggregates = _.uniq(aggregates);

      const domainEvents = _(
        uniqAggregates.map((aggregate) => aggregate.domainEvents),
      )
        .flatten()
        .uniqBy(({ id }) => id)
        .value();

      allDomainEvents.push(...domainEvents);

      for (const aggregate of aggregates) {
        aggregate.clearEvents();
      }

      const emitBulkResult = await this.domainEventsPublisher.emitBulk(
        domainEvents,
        trxId,
        command.initiatorUserId,
      );
      if (emitBulkResult.isErr) {
        return emitBulkResult.mapValue(() => null);
      }

      return result;
    };

    if (hasCommands) {
      return callback();
    }

    const result = await this.unitOfWork.execute(trxId, callback);

    this.asyncDomainEventsPublisher.emitBulk(
      allDomainEvents,
      command.initiatorUserId,
    );

    return result;
  }
}
