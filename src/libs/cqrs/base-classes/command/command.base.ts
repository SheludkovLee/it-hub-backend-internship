import { DateVO, UUID } from 'src/libs/domain';

import { commandDecoratorKey } from '../../decorators';
import { ICommand, SerializedCommand } from '../../interfaces';

export interface CreateCommandProps<T> {
  payload: T;
  trxId?: string;
  initiatorUserId?: string;
}

export abstract class CommandBase<Payload extends Record<string, any> = any>
  implements ICommand<Payload>
{
  token: string;
  trxId: string;
  dateOccurred: string;
  initiatorUserId?: string;
  payload: Payload;

  constructor(props: CreateCommandProps<Payload>) {
    const token = Reflect.getMetadata(commandDecoratorKey, this.constructor);
    if (!token) {
      throw new Error(
        `${this.constructor.name} must have '@Command' decorator`,
      );
    }
    this.token = token;

    this.trxId = props.trxId || UUID.generate().value;
    this.payload = props.payload;
    this.dateOccurred = DateVO.now().toISOString();
    this.initiatorUserId = props.initiatorUserId;
  }

  public toJSON(): SerializedCommand<Payload> {
    return {
      token: this.token,
      payload: this.payload,
      trxId: this.trxId,
      initiatorUserId: this.initiatorUserId,
      dateOccurred: this.dateOccurred,
    };
  }

  setTrxId(trxId: string) {
    this.trxId = trxId;
  }
}
