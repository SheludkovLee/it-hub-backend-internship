import { Logger } from '@nestjs/common';
import { UnitOfWorkPort } from 'src/libs/database';
import { ExceptionBase, NotFoundException } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { ICommand, ICommandBus, ICommandHandler } from '../../interfaces';

export class CommandBus implements ICommandBus {
  constructor(private readonly unitOfWork: UnitOfWorkPort) {}

  protected subscribers = new Map<string, ICommandHandler>();

  public register(token: string, handler: ICommandHandler): void {
    this.subscribers.set(token, handler);
  }

  public async execute<TResult>(
    command: ICommand,
  ): Promise<Result<TResult, ExceptionBase>> {
    const handler = this.subscribers.get(command.token);

    if (!handler) {
      return Result.fail(
        new NotFoundException(`Command handler for ${command.token} not found`),
      );
    }

    return handler.execute(command);
  }

  public async executeManyInOneTrx<TResult>(
    ...commands: ICommand[]
  ): Promise<Result<TResult[], ExceptionBase>> {
    const trxId = await this.unitOfWork.start();

    for (const command of commands) {
      command.setTrxId(trxId);
    }

    const commandExecuters = commands.map((command) => {
      const handler = this.subscribers.get(command.token);

      if (!handler) {
        throw new NotFoundException(
          `Command handler for ${command.token} not found`,
        );
      }

      return () => handler.execute(command);
    });

    return this.unitOfWork.execute(trxId, async () => {
      const results: TResult[] = [];

      for (const executeCmd of commandExecuters) {
        const result = await executeCmd();
        if (result.isErr) {
          return result;
        } else {
          results.push(result.unwrap());
        }
      }

      return Result.ok(results);
    });
  }

  public async executeManyInOneTrxWithoutStopOnError<TResult>(
    ...commands: ICommand[]
  ): Promise<Result<TResult[], ExceptionBase>> {
    const trxId = await this.unitOfWork.start();

    for (const command of commands) {
      command.setTrxId(trxId);
    }

    const commandExecuters = commands.map((command) => {
      const handler = this.subscribers.get(command.token);

      if (!handler) {
        throw new NotFoundException(
          `Command handler for ${command.token} not found`,
        );
      }

      return async () => {
        const result = await handler.execute(command);

        if (result.isErr) {
          Logger.error(result.error);
        }

        return result;
      };
    });

    return this.unitOfWork.execute(trxId, async () => {
      const results: TResult[] = [];

      for (const executeCmd of commandExecuters) {
        const result = await executeCmd();
        if (result.isErr) {
          continue;
        } else {
          results.push(result.unwrap());
        }
      }

      return Result.ok(results);
    });
  }
}
