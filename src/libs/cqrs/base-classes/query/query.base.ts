import { DateVO } from 'src/libs/domain';

import { queryDecoratorKey } from '../../decorators';
import { IQuery } from '../../interfaces';

export abstract class QueryBase<
  Payload extends Record<string, any> = Record<string, any>,
> implements IQuery<Payload>
{
  token: string;
  dateOccurred: string;
  payload: Payload;

  constructor(payload: Payload) {
    const token = Reflect.getMetadata(queryDecoratorKey, this.constructor);
    if (!token) {
      throw new Error(`${this.constructor.name} must have '@Query' decorator`);
    }
    this.token = token;
    this.payload = payload;
    this.dateOccurred = DateVO.now().toISOString();
  }
}
