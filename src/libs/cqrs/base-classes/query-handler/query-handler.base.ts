import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { queryHandlerDecoratorKey } from '../../decorators';
import { IQueryBus, IQueryHandler } from '../../interfaces';
import { QueryBase } from '../query';

export abstract class QueryHandlerBase<
  Query extends QueryBase,
  ReturnType,
  ReadRepository,
> implements IQueryHandler<Query, ReturnType>
{
  private readonly queryBus: IQueryBus;
  protected readonly repository: ReadRepository;

  constructor(queryBus: IQueryBus, repository: ReadRepository) {
    this.queryBus = queryBus;
    const queryToken = Reflect.getMetadata(
      queryHandlerDecoratorKey,
      this.constructor,
    );

    if (!queryToken) {
      throw new Error(
        `${this.constructor.name} must have '@QueryHandler' decorator`,
      );
    }
    this.queryBus.register(queryToken, this);
    this.repository = repository;
  }

  abstract handle(query: Query): Promise<Result<ReturnType, ExceptionBase>>;
}
