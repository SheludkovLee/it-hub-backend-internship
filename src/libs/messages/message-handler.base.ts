import { Logger } from '@nestjs/common';
import * as Sentry from '@sentry/node';
import { ICommandBus } from 'src/libs/cqrs';
import { UnitOfWorkPort } from 'src/libs/database';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { BoundedContext } from '../../../database/types';
import { IMessage } from './message.interface';
import { IMessageBus } from './message-bus.interface';
import { IMessageHandler } from './message-handler.interface';

export abstract class MessageHandler<
  Message extends IMessage,
  UnitOfWork extends UnitOfWorkPort = UnitOfWorkPort,
> implements IMessageHandler
{
  constructor(
    protected readonly commandBus: ICommandBus,
    protected readonly messageBus: IMessageBus,
    protected readonly unitOfWork: UnitOfWork,
    public readonly context: BoundedContext,
  ) {
    this.messageBus.register(this.getMessageToken(), this);
  }

  abstract getMessageToken(): Message['token'];

  getContext(): BoundedContext {
    return this.context;
  }

  async handle(message: Message): Promise<Result<any, ExceptionBase>> {
    try {
      const result = await this.execute(message);

      Logger.log({
        msg: `Handle message from context ${message.context}, id=${message.id} token=${message.token}`,
        payload: message.payload,
        result: result.unwrap(),
        handlerClassName: this.constructor.name,
      });

      await this.unitOfWork
        .getMessageInboxRepository()
        .insertAsProcessed(message)
        .then((res) => res.unwrap());

      return result;
    } catch (e) {
      Logger.error({
        msg: `Handle message from context ${message.context}, id=${message.id} token=${message.token}`,
        payload: message.payload,
        err: e,
        handlerClassName: this.constructor.name,
      });

      Sentry.captureException(e, (scope) => {
        scope.setTag('kind', 'message-handler');
        scope.setExtra('message', message);
        scope.setExtra('handler', this.constructor.name);
        return scope;
      });

      await this.unitOfWork
        .getMessageInboxRepository()
        .insertAsFailed(message, e)
        .then((res) => res.unwrap());

      return Result.fail(e);
    }
  }

  abstract execute(message: Message): Promise<Result<any, ExceptionBase>>;
}
