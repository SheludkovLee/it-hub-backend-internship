import { UUID } from '../domain';
import { IMessage } from './message.interface';

type Meta = {
  trxId?: string;
  dateOccurred: string;
};

type Props<Payload> = {
  payload: Payload & { initiatorUserId?: string };
  meta: Meta;
};

export abstract class Message<
  Payload extends Record<string, unknown> = Record<string, unknown>,
> implements IMessage<Payload>
{
  readonly token: string;
  readonly context: string;
  readonly id: string;
  readonly payload: Payload & { initiatorUserId?: string };
  readonly trxId: string;
  readonly dateOccurred: string;

  constructor(props: Props<Payload>) {
    const { payload, meta } = props;

    this.id = UUID.generate().value;
    this.token = this.getToken();
    this.context = this.getContext();
    this.payload = payload;
    this.trxId = meta.trxId;
    this.dateOccurred = meta.dateOccurred;
  }

  abstract getToken(): string;
  abstract getContext(): string;
}
