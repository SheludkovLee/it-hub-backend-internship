export interface IMessage<
  Payload extends Record<string, unknown> = Record<string, unknown>,
> {
  id: string;
  token: string; // TODO: add token enum
  context: string; // TODO: add bounded countext enum
  payload: Payload & { initiatorUserId?: string };
  trxId: string;
  dateOccurred: string;
}
