import { ExceptionBase } from '../exceptions';
import { Result } from '../utils';
import { IMessage } from './message.interface';
import { IMessageHandler } from './message-handler.interface';

export interface IMessageBus {
  register(messageToken: string, messageHandler: IMessageHandler): void;
  registerSyncHandler(
    messageToken: string,
    messageHandler: IMessageHandler,
  ): void;
  syncPublish(message: IMessage): Promise<Result<any, ExceptionBase>>;
}
