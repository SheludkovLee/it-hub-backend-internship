import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { BoundedContext } from '../../../database/types';
import { IMessage } from './message.interface';

export interface IMessageHandler {
  handle(message: IMessage): Promise<Result<any, ExceptionBase>>;

  getContext(): BoundedContext;
}
