import { UnitOfWorkPort } from 'src/libs/database';
import {
  DomainEventBase,
  DomainEventsPublisher,
  listenToEventsDecoratorKey,
} from 'src/libs/domain';
import { IBusHandler } from 'src/libs/events';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { Message } from './message.base';
import { IMessageBus } from './message-bus.interface';

export abstract class SyncDomainEventMapper<
  UnitOfWork extends UnitOfWorkPort = UnitOfWorkPort,
> implements IBusHandler<DomainEventBase>
{
  static isSync = true as const;
  static isAsync = false as const;

  constructor(
    protected readonly unitOfWork: UnitOfWork,
    private readonly domainEventsPublisher: DomainEventsPublisher,
    private readonly messageBus: IMessageBus,
  ) {
    const tokens = Reflect.getMetadata(
      listenToEventsDecoratorKey,
      this.constructor,
    );

    if (!tokens) {
      throw new Error(
        'Sync domain event mapper must have EventsHandler decorator',
      );
    }

    this.domainEventsPublisher.register(tokens, this);
  }

  abstract execute(
    event: DomainEventBase,
  ): Promise<Result<Message, ExceptionBase>> | Result<Message, ExceptionBase>;

  async handle(
    event: DomainEventBase,
  ): Promise<Result<unknown, ExceptionBase>> {
    try {
      const result = await this.execute(event);
      const message = result.unwrap();

      if (!message.payload.initiatorUserId) {
        message.payload.initiatorUserId = event.initiatorUserId;
      }

      return this.messageBus.syncPublish(message);
    } catch (e) {
      return Result.fail(e);
    }
  }
}
