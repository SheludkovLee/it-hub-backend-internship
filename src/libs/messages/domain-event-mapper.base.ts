import { UnitOfWorkPort } from 'src/libs/database';
import {
  DomainEventBase,
  DomainEventsPublisher,
  listenToEventsDecoratorKey,
} from 'src/libs/domain';
import { IBusHandler } from 'src/libs/events';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { Message } from './message.base';

export abstract class DomainEventMapper<
  UnitOfWork extends UnitOfWorkPort = UnitOfWorkPort,
> implements IBusHandler<DomainEventBase>
{
  static isSync = false as const;
  static isAsync = true as const;

  constructor(
    protected readonly unitOfWork: UnitOfWork,
    private readonly domainEventsPublisher: DomainEventsPublisher,
  ) {
    const tokens = Reflect.getMetadata(
      listenToEventsDecoratorKey,
      this.constructor,
    );

    if (!tokens) {
      throw new Error('Domain event mapper must have EventsHandler decorator');
    }

    this.domainEventsPublisher.register(tokens, this);
  }

  abstract execute(
    event: DomainEventBase,
  ):
    | Promise<Result<Message, ExceptionBase>>
    | Result<Message, ExceptionBase>
    | Promise<Result<Message[], ExceptionBase>>
    | Result<Message[], ExceptionBase>;

  async handle(
    event: DomainEventBase,
  ): Promise<Result<unknown, ExceptionBase>> {
    try {
      const result = await this.execute(event);
      const message = result.unwrap();

      if (!Array.isArray(message)) {
        if (!message.payload.initiatorUserId) {
          message.payload.initiatorUserId = event.initiatorUserId;
        }

        const insertMessageResult = await this.unitOfWork
          .getMessagesOutboxRepository(event.trxId)
          .insert(message);

        insertMessageResult.unwrap();

        return Result.ok();
      }

      for await (const m of message) {
        if (!m.payload.initiatorUserId) {
          m.payload.initiatorUserId = event.initiatorUserId;
        }

        const insertMessageResult = await this.unitOfWork
          .getMessagesOutboxRepository(event.trxId)
          .insert(m);

        insertMessageResult.unwrap();
      }

      return Result.ok();
    } catch (e) {
      return Result.fail(e);
    }
  }
}
