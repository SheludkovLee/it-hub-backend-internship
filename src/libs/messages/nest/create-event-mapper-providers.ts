import { MessageBus } from 'src/infra/communication/message-bus';
import { UnitOfWorkPort } from 'src/libs/database';
import { DomainEventsPublisher } from 'src/libs/domain';
import { Constructor } from 'src/libs/types';

import { DomainEventMapper } from '../domain-event-mapper.base';
import { IMessageBus } from '../message-bus.interface';
import { SyncDomainEventMapper } from '../sync-domain-event-mapper.base';

export interface AsyncEventMapperConstructorInterface {
  new (
    unitOfWork: UnitOfWorkPort,
    domainEventsPublisher: DomainEventsPublisher,
  ): DomainEventMapper;
  isSync: false;
  isAsync: true;
}

export interface SyncEventMapperConstructorInterface {
  new (
    unitOfWork: UnitOfWorkPort,
    domainEventsPublisher: DomainEventsPublisher,
    messageBus: MessageBus,
  ): SyncDomainEventMapper;
  isSync: true;
  isAsync: false;
}

export const createEventMapperProviders =
  (
    eventMappers: Array<
      AsyncEventMapperConstructorInterface | SyncEventMapperConstructorInterface
    >,
  ) =>
  (deps: {
    unitOfWork: Constructor<UnitOfWorkPort>;
    domainEventsPublisher: Constructor<DomainEventsPublisher>;
    messageBus: Constructor<IMessageBus>;
  }) => {
    return eventMappers.map((eventMapperClass) => {
      if (eventMapperClass.isSync) {
        return {
          provide: eventMapperClass,
          useFactory(
            unitOfWork: UnitOfWorkPort,
            domainEventsPublisher: DomainEventsPublisher,
            messageBus: MessageBus,
          ) {
            return new eventMapperClass(
              unitOfWork,
              domainEventsPublisher,
              messageBus,
            );
          },
          inject: [
            deps.unitOfWork,
            deps.domainEventsPublisher,
            deps.messageBus,
          ],
        };
      } else if (eventMapperClass.isAsync) {
        return {
          provide: eventMapperClass,
          useFactory(
            unitOfWork: UnitOfWorkPort,
            domainEventsPublisher: DomainEventsPublisher,
          ) {
            return new eventMapperClass(unitOfWork, domainEventsPublisher);
          },
          inject: [deps.unitOfWork, deps.domainEventsPublisher],
        };
      } else {
        throw new Error('Unknown event mapper type');
      }
    });
  };
