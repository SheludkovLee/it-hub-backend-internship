import { Global, Module } from '@nestjs/common';

import { MailingConfigProvider, MailingServiceProvider } from './base';
import { UnisenderConfigProvider } from './unisender/unisender-config.provider';

@Global()
@Module({
  providers: [
    MailingConfigProvider,
    UnisenderConfigProvider,
    MailingServiceProvider,
  ],
  exports: [MailingServiceProvider],
})
export class MailingModule {}
