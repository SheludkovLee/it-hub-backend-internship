import { SendDailyDigestEmailInput } from './send-daily-digest-email.type';
import { SendRegistrationConfirmationEmailInput } from './send-registration-confirmation-email.type';
import { SendResetPasswordEmailInput } from './send-reset-password-email.type';

export abstract class IMailingService {
  abstract sendResetPasswordEmail(
    input: SendResetPasswordEmailInput,
  ): Promise<void>;
  abstract sendRegistrationConfirmationEmail(
    inputs: SendRegistrationConfirmationEmailInput[],
  ): Promise<void>;
  abstract sendDailyDigestEmail(
    inputs: SendDailyDigestEmailInput[],
    sendAt: string,
  ): Promise<void>;
}
