export interface SendResetPasswordEmailInput {
  name?: string;
  email: string;
  token: string;
}
