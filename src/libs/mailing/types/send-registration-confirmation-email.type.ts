export interface SendRegistrationConfirmationEmailInput {
  email: string;
  token: string;
}
