export interface SendDailyDigestEmailInput {
  email: string;
  body: string;
}
