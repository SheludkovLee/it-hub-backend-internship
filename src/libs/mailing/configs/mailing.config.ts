export class MailingConfig {
  readonly registrationConfirmationUrl: string;
  readonly resetPasswordUrl: string;
  readonly frontendDomain: string;

  constructor(input: MailingConfig) {
    Object.assign(this, input);
  }
}
