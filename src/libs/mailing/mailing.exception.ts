export class MailingException extends Error {
  constructor(public readonly details?: unknown) {
    super('Что-то пошло не так с почтовым сервисом');
  }
}
