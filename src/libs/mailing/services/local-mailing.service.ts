import { Logger } from '@nestjs/common';

import {
  IMailingService,
  SendDailyDigestEmailInput,
  SendRegistrationConfirmationEmailInput,
  SendResetPasswordEmailInput,
} from '../types';

export class LocalMailingService implements IMailingService {
  private readonly logger = new Logger(this.constructor.name);

  async sendRegistrationConfirmationEmail(
    inputs: SendRegistrationConfirmationEmailInput[],
  ): Promise<void> {
    this.logger.debug(
      `sendRegistrationConfirmationEmail executed ${JSON.stringify(inputs)}`,
    );
  }

  async sendResetPasswordEmail(
    input: SendResetPasswordEmailInput,
  ): Promise<void> {
    this.logger.debug(
      `sendResetPasswordEmail executed ${JSON.stringify(input)}`,
    );
  }

  async sendDailyDigestEmail(
    inputs: SendDailyDigestEmailInput[],
  ): Promise<void> {
    this.logger.debug(
      `sendDailyDigestEmail executed ${JSON.stringify(inputs)}`,
    );
  }
}
