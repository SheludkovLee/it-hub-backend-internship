export const generateLink = (baseLink: string, token: string) =>
  `${baseLink}${token}`;
