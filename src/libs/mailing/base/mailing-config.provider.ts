import { Provider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { MailingConfig } from '../configs/mailing.config';

export const MailingConfigProvider: Provider<MailingConfig> = {
  provide: MailingConfig,
  useFactory: (configService: ConfigService) => {
    const registrationConfirmationUrl = configService.get(
      'registrationConfirmationUrl',
    );
    const resetPasswordUrl = configService.get('resetPasswordUrl');
    const frontendDomain = configService.get<string>('app.frontendDomain');

    return new MailingConfig({
      registrationConfirmationUrl,
      resetPasswordUrl,
      frontendDomain,
    });
  },
  inject: [ConfigService],
};
