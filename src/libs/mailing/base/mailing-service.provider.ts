import { Provider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { MailingConfig } from '../configs/mailing.config';
import { LocalMailingService } from '../services/local-mailing.service';
import { IMailingService } from '../types/mailing.type';
import { UnisenderConfig } from '../unisender/unisender.config';
import { UnisenderMailingService } from '../unisender/unisender-mailing.service';

export const MailingServiceProvider: Provider<IMailingService> = {
  provide: IMailingService,
  inject: [ConfigService, MailingConfig, UnisenderConfig],
  useFactory: (
    configService: ConfigService,
    mailingConfig: MailingConfig,
    unisenderConfig: UnisenderConfig,
  ) => {
    const isEmailMailingEnabled =
      configService.get('emailMailingEnable') === 'true';

    return isEmailMailingEnabled
      ? new UnisenderMailingService(mailingConfig, unisenderConfig)
      : new LocalMailingService();
  },
};
