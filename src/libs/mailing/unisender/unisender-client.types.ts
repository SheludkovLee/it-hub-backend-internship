export interface UnisenderEmailSendInput {
  message: UnisenderMessage;
}

export interface UnisenderMessageOptions {
  send_at: string;
}

export interface UnisenderMessage {
  recipients: UnisenderMessageRecipient[];
  template_id?: string;
  skip_unsubscribe?: number;
  global_language?: string;
  template_engine?: string;
  global_substitutions?: Record<string, string>;
  body?: UnisenderMessageBody;
  subject?: string;
  from_email?: string;
  from_name?: string;
  reply_to?: string;
  bypass_global?: number;
  bypass_unavailable?: number;
  bypass_unsubscribed?: number;
  bypass_complained?: number;
  attachments?: UnisenderAttachmentsEntityOrInlineAttachment[];
  inline_attachments?: UnisenderAttachmentsEntityOrInlineAttachment[];
  options?: UnisenderMessageOptions;
}

export interface UnisenderMessageRecipient {
  email: string;
  substitutions?: Record<string, string>;
}

export interface UnisenderMessageBody {
  html: string;
  plaintext: string;
  amp: string;
}

export interface UnisenderAttachmentsEntityOrInlineAttachment {
  type: string;
  name: string;
  content: string;
}

export interface UnisenderEmailSendOutput {
  status: 'success' | 'error';
  job_id: string;
  emails: string[];
  failed_emails: Record<string, FailedEmailStatus>;
}

export enum FailedEmailStatus {
  UNSUBSCRIBED = 'unsubscribed',
  INVALID = 'invalid',
  DUPLICATE = 'duplicate',
  TEMPORARY_UNAVAILABLE = 'temporary_unavailable',
  PERMANENT_UNAVAILABLE = 'permanent_unavailable',
  COMPLAINED = 'complained',
  BLOCKED = 'blocked',
}
