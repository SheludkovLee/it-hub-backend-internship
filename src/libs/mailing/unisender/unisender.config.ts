export class UnisenderConfig implements RequestInit {
  readonly headers: HeadersInit;
  readonly baseUrl: string;

  readonly registrationConfirmationTemplateId: string;
  readonly resetPasswordTemplateId: string;
  readonly dailyDigestTemplateId: string;

  constructor(
    baseUrl: string,
    apiKey: string,
    templates: Pick<
      UnisenderConfig,
      | 'registrationConfirmationTemplateId'
      | 'resetPasswordTemplateId'
      | 'dailyDigestTemplateId'
    >,
  ) {
    this.baseUrl = baseUrl;
    this.headers = { 'X-API-KEY': apiKey };

    Object.assign(this, templates);
  }
}
