import { Provider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { UnisenderConfig } from './unisender.config';

export const UnisenderConfigProvider: Provider<UnisenderConfig> = {
  provide: UnisenderConfig,
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    const baseUrl = configService.get('unisender.baseUrl');
    const apiKey = configService.get('unisender.apiKey');

    const registrationConfirmationTemplateId = configService.get(
      'unisender.registrationConfirmationTemplateId',
    );
    const resetPasswordTemplateId = configService.get(
      'unisender.resetPasswordTemplateId',
    );
    const dailyDigestTemplateId = configService.get(
      'unisender.dailyDigestTemplateId',
    );

    const templates = {
      registrationConfirmationTemplateId,
      resetPasswordTemplateId,
      dailyDigestTemplateId,
    };

    return new UnisenderConfig(baseUrl, apiKey, templates);
  },
};
