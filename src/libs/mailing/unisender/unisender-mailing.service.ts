import { Injectable, Logger } from '@nestjs/common';
import * as Sentry from '@sentry/node';

import { MailingConfig } from '../configs';
import { MailingException } from '../mailing.exception';
import { generateLink } from '../mailing.utils';
import {
  IMailingService,
  SendDailyDigestEmailInput,
  SendRegistrationConfirmationEmailInput,
  SendResetPasswordEmailInput,
} from '../types';
import { UnisenderConfig } from './unisender.config';
import {
  UnisenderEmailSendInput,
  UnisenderEmailSendOutput,
  UnisenderMessageRecipient,
} from './unisender-client.types';

@Injectable()
export class UnisenderMailingService implements IMailingService {
  private readonly logger = new Logger('Unisender');

  constructor(
    private readonly config: MailingConfig,
    private readonly unisenderConfig: UnisenderConfig,
  ) {}

  async sendResetPasswordEmail(
    input: SendResetPasswordEmailInput,
  ): Promise<void> {
    const recipient: UnisenderMessageRecipient = {
      email: input.email,
      substitutions: {
        link: generateLink(this.config.resetPasswordUrl, input.token),
      },
    };

    if (input.name) {
      recipient.substitutions.name = input.name;
    }

    const body: UnisenderEmailSendInput = {
      message: {
        recipients: [recipient],
        template_id: this.unisenderConfig.resetPasswordTemplateId,
      },
    };

    await this.sendEmailRequest(body);
  }

  async sendRegistrationConfirmationEmail(
    inputs: SendRegistrationConfirmationEmailInput[],
  ): Promise<void> {
    const recipients: UnisenderMessageRecipient[] = inputs.map(
      ({ email, token }) => {
        const link = generateLink(
          this.config.registrationConfirmationUrl,
          token,
        );

        const recipient = { email, substitutions: { email, link } };

        return recipient;
      },
    );

    const body: UnisenderEmailSendInput = {
      message: {
        recipients,
        template_id: this.unisenderConfig.registrationConfirmationTemplateId,
      },
    };

    await this.sendEmailRequest(body);
  }

  private async sendEmailRequest(
    input: UnisenderEmailSendInput,
  ): Promise<UnisenderEmailSendOutput> {
    try {
      const config = this.unisenderConfig;

      const serializedBody = JSON.stringify(input);
      const url = `${config.baseUrl}/email/send.json`;

      this.logger.log(input);

      const response = await fetch(url, {
        headers: config.headers,
        method: 'POST',
        body: serializedBody,
      });

      const result = await response.json();

      if (result.status === 'error') {
        Sentry.captureException(new Error(JSON.stringify(result)), (scope) => {
          scope.setTag('kind', 'mail');
          return scope;
        });
        this.logger.error(JSON.stringify(result));
      }

      this.logger.log(result);

      return result;
    } catch (e) {
      Sentry.captureException(e, (scope) => {
        scope.setTag('kind', 'mail');
        return scope;
      });
      this.logger.error(JSON.stringify(e));

      throw new MailingException(e);
    }
  }

  async sendDailyDigestEmail(
    inputs: SendDailyDigestEmailInput[],
    sendAt: string,
  ): Promise<void> {
    const date = new Intl.DateTimeFormat('ru').format(new Date());
    const linkToWebsite = this.config.frontendDomain;
    const recipients: UnisenderMessageRecipient[] = inputs.map(
      ({ email, body }) => ({
        email,
        substitutions: { email, body, date, linkToWebsite },
      }),
    );
    const formattedDateTime = sendAt.replace('T', ' ').split('.')[0];
    const body: UnisenderEmailSendInput = {
      message: {
        recipients,
        template_id: this.unisenderConfig.dailyDigestTemplateId,
        options: {
          send_at: formattedDateTime,
        },
      },
    };

    await this.sendEmailRequest(body);
  }
}
