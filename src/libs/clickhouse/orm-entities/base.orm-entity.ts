export abstract class BaseClickhouseOrmEntity<P> {
  constructor(private _props: P) {}

  public get props(): P {
    return this._props;
  }
}
