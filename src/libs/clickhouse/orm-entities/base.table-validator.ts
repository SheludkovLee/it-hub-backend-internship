import { Schema, Validator, ValidatorResult } from 'jsonschema';

import { BaseClickhouseOrmEntity } from './base.orm-entity';

export interface IClickhouseTableValidator<
  Entity extends BaseClickhouseOrmEntity<unknown>,
> {
  validateDataShape<T>(data: T[], mapper: (entry: T) => Entity): Entity[];
}

export abstract class BaseClickhouseTableValidator<
  Entity extends BaseClickhouseOrmEntity<unknown>,
> implements IClickhouseTableValidator<Entity>
{
  protected validator: Validator = new Validator();

  constructor(protected readonly schema: Schema) {}

  validateDataShape<T>(raw: T[], mapper: (entry: T) => Entity): Entity[] {
    const mappedData = this.map(raw, mapper);

    const results = mappedData.map((data) => this.validate(data.props));

    const hasError = results.find((result) => !result.valid);

    if (hasError) {
      throw hasError.errors;
    }

    return mappedData;
  }

  protected map<T>(data: T[], mapper: (entry: T) => Entity): Entity[] {
    return data.map(mapper);
  }

  protected validate<T>(data: T): ValidatorResult {
    const result = this.validator.validate(data, this.schema);

    return result;
  }
}
