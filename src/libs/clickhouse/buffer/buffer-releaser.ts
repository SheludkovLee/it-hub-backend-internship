import { ClickHouseClient } from '@clickhouse/client';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { featureFlags } from 'src/infra/feature-flags/flags';
import { Stream } from 'stream';

import { ClickhouseClientProvider } from '../clickhouse.client';
import { ClickhouseBufferEntryOrmEntity } from './buffer-entry.orm-entity';

export interface IClickhouseBufferReleaser {
  release(): Promise<boolean>;
}

@Injectable()
export class ClickhouseBufferReleaser implements IClickhouseBufferReleaser {
  constructor(
    @Inject(ClickhouseClientProvider.provide)
    private readonly clickhouseClient: ClickHouseClient,
  ) {}

  async release(): Promise<boolean> {
    if (!featureFlags.clickhouse) {
      return true;
    }

    await ClickhouseBufferEntryOrmEntity.transaction(async (trx) => {
      const existedTables = await ClickhouseBufferEntryOrmEntity.query(trx)
        .select('table')
        .distinctOn('table')
        .castTo<{ table: string }[]>();

      for await (const table of existedTables) {
        const knexStream = trx
          .select('payload')
          .where('table', table.table)
          .from(ClickhouseBufferEntryOrmEntity.tableName)
          .stream({ objectMode: true });

        /**
         * Тут все на стримах т.к. в буфере может быть очень много событий и грузить их все в жаваскрипт было бы самоубийством
         * В итоге получаем из ПГ данные с помощью стрима, трансформируем через Transform стрим
         *  и отправляем уже трансформированные данные в кликхаус (который благо тоже принимает стримы)
         */
        const transformStream = new Stream.Transform({
          transform: (chunk, _, callback) => {
            const typedChunk = chunk as ClickhouseBufferEntryOrmEntity;

            callback(null, {
              ...typedChunk.payload,
            });
          },
          objectMode: true,
        });

        knexStream.pipe(transformStream);

        await this.clickhouseClient.insert({
          table: table.table,
          values: transformStream,
          format: 'JSONEachRow',
        });
      }

      await ClickhouseBufferEntryOrmEntity.query(trx).delete();
    });

    return true;
  }

  @Cron('0 0 * * 0')
  private async _triggerRelease() {
    try {
      await this.release();

      Logger.log(
        'Succeed release buffer of clickhouse table',
        'Clickhouse buffer releaser',
      );
    } catch (error) {
      Logger.error(error, `Clickhouse buffer releaser`);
    }
  }
}
