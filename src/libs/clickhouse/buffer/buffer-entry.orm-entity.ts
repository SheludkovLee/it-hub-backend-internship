import { Model } from 'objection';

export class ClickhouseBufferEntryOrmEntity extends Model {
  static tableName = 'clickhouse_buffer';

  id: string;

  table: string;

  payload: Record<string, unknown>;
}
