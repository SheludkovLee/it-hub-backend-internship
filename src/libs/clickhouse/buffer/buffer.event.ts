export class ClickhouseBufferEvent<P extends object = object> {
  constructor(private _table: string, private _payload: P) {}

  public get table(): string {
    return this._table;
  }

  public get payload(): P {
    return this._payload;
  }
}
