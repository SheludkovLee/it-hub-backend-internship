import { Injectable } from '@nestjs/common';

import { ModelObject } from '../../database/utility-types';
import { ClickhouseBufferEvent } from './buffer.event';
import { ClickhouseBufferEntryOrmEntity } from './buffer-entry.orm-entity';

export interface IClickhouseBufferRegister {
  registerEvents(event: ClickhouseBufferEvent[]): Promise<boolean>;
}

@Injectable()
export class ClickhouseBufferRegister implements IClickhouseBufferRegister {
  async registerEvents(event: ClickhouseBufferEvent[]): Promise<boolean> {
    await ClickhouseBufferEntryOrmEntity.query().insert(
      event.map(
        (event): ModelObject<ClickhouseBufferEntryOrmEntity> => ({
          table: event.table,
          payload: event.payload,
        }),
      ),
    );

    return true;
  }
}
