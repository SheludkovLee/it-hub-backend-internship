import knex from 'knex';

export class ClickhouseQueryBuilder {
  private static knex = knex({ client: 'pg' });

  static raw = this.knex.raw.bind(this.knex);
  static select = this.knex.select.bind(this.knex);

  static getQb() {
    return this.knex.queryBuilder();
  }
}
