import { ClickHouseClient } from '@clickhouse/client';
import { Global, Inject, Module, OnModuleDestroy } from '@nestjs/common';

import { ClickhouseBufferRegister } from './buffer/buffer-register';
import { ClickhouseBufferReleaser } from './buffer/buffer-releaser';
import { ClickhouseClientProvider } from './clickhouse.client';
import { ClickhouseMigrationsSource } from './migrations/migrations-source';
import { ClickhouseMigrator } from './migrations/migrator';

/**
 * На микросервисах сделаем так, что шлюз для работы с кликхаусом будет хоститься отдельно как отдельный микросервис
 * другие микросервисы будут стучаться к нему для регистрации эвентов, например
 */
@Global()
@Module({
  providers: [
    ClickhouseClientProvider,
    ClickhouseBufferRegister,
    ClickhouseBufferReleaser,
    ClickhouseMigrationsSource,
    ClickhouseMigrator,
  ],
  exports: [ClickhouseBufferRegister, ClickhouseClientProvider],
})
export class ClickhouseModule implements OnModuleDestroy {
  constructor(
    @Inject(ClickhouseClientProvider.provide)
    private readonly client: ClickHouseClient,
  ) {}

  async onModuleDestroy() {
    await this.client.close();
  }
}
