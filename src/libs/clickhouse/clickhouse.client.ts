import { createClient } from '@clickhouse/client';
import { FactoryProvider, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { readFile } from 'fs/promises';
import { featureFlags } from 'src/infra/feature-flags/flags';

export const ClickhouseClientProvider: FactoryProvider = {
  provide: 'ClickhouseClientProvider',
  useFactory: async (config: ConfigService) => {
    const client = createClient({
      host: config.get('clickhouse.host'),
      database: config.get('clickhouse.database'),
      username: config.get('clickhouse.user'),
      password: config.get('clickhouse.password'),
      tls: {
        ca_cert: await readFile('yandex-cloud/certs/clickhouse-ca.crt'),
      },
    });

    if (!featureFlags.clickhouse) {
      return client;
    }

    const pingResult = await client.ping();

    if (!pingResult.success) {
      throw new Error(`Clickhouse connect establish error`);
    }

    Logger.log(`Clickhouse connection established`, 'Clickhouse');

    return client;
  },
  inject: [ConfigService],
};
