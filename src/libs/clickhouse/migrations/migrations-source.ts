import { Injectable } from '@nestjs/common';
import { CreatePassedClassesTableClickhouseMigration } from 'src/libs/clickhouse/migrations/migrations/passed-classes-table.migration';
import { UpdateAcceptedStudentsTableClickhouseMigration } from 'src/libs/clickhouse/migrations/migrations/update-accepted-students-table.migration';
import { UpdateLearningStudentsTableClickhouseMigration } from 'src/libs/clickhouse/migrations/migrations/update-learning-students-table.migration';

import { CreateAcceptedStudentsTableClickhouseMigration } from './migrations/accepted-students-table.migration';
import { IMigration } from './migrations/base-migration';
import { CreateLearningStudentsTableClickhouseMigration } from './migrations/learning-students-table.migration';
import { CreateScoresTableClickhouseMigration } from './migrations/scores-table.migration';

interface IMigrationsSource {
  getMigrations(): Promise<IMigration[]>;
}

@Injectable()
export class ClickhouseMigrationsSource implements IMigrationsSource {
  async getMigrations(): Promise<IMigration[]> {
    return [
      new CreateScoresTableClickhouseMigration(),
      new CreateAcceptedStudentsTableClickhouseMigration(),
      new CreateLearningStudentsTableClickhouseMigration(),
      new UpdateAcceptedStudentsTableClickhouseMigration(),
      new UpdateLearningStudentsTableClickhouseMigration(),
      new CreatePassedClassesTableClickhouseMigration(),
    ];
  }
}
