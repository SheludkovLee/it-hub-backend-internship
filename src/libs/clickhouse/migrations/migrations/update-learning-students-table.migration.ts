// alter table accepted_students
//     add column qualificationId Nullable(UUID);

import { BaseClickhouseMigration } from './base-migration';

export class UpdateLearningStudentsTableClickhouseMigration extends BaseClickhouseMigration {
  getDescription(): string {
    return `Update learning students table (qualificationId)`;
  }

  getQuery(): string {
    return `
      ALTER TABLE ithub.learning_students
      ADD COLUMN IF NOT EXISTS qualificationId UUID;
`;
  }
}
