import { BaseClickhouseMigration } from './base-migration';

export class CreateScoresTableClickhouseMigration extends BaseClickhouseMigration {
  getDescription(): string {
    return `Creates scores table https://pw-tech.atlassian.net/browse/IT-5191`;
  }

  getQuery(): string {
    return `
      CREATE TABLE IF NOT EXISTS ithub.task_scores
      (
          "score" UInt8,
          "scoreInPercent" UInt8,
          "maxScore" UInt16,
          "studentId" UUID,
          "disciplineId" UUID,
          "learningGroupId" UUID,
          "qualificationId" UUID,
          "specialityId" UUID,
          "suborganizationId" UUID,
          "organizationId" UUID,
          "studyPeriodId" UUID,
          "timestamp" DateTime
      )
      ENGINE = MergeTree()
      PRIMARY KEY (
        "disciplineId", 
        "learningGroupId", 
        "qualificationId",
        "specialityId",
        "suborganizationId",
        "organizationId",
        "studyPeriodId",
        timestamp
        )
      `;
  }
}
