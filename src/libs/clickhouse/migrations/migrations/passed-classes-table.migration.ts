import { BaseClickhouseMigration } from './base-migration';

export class CreatePassedClassesTableClickhouseMigration extends BaseClickhouseMigration {
  getDescription(): string {
    return `Creates passed classes table https://pw-tech.atlassian.net/browse/IT-5678`;
  }

  getQuery(): string {
    return `
      CREATE TABLE IF NOT EXISTS ithub.passed_classes
      (
          "classId" UUID,
          "status" String,
          "reason" String,
          "studentId" UUID,
          "disciplineId" UUID,
          "learningGroupId" UUID,
          "specialityId" UUID,
          "suborganizationId" UUID,
          "organizationId" UUID,
          "studyPeriodId" UUID,
          "qualificationId" UUID,
          "timestamp" DateTime
      )
      ENGINE = MergeTree()
      PRIMARY KEY (
        "classId",
        "status",
        "reason",
        "studentId",
        "disciplineId",
        "learningGroupId",
        "specialityId",
        "suborganizationId",
        "organizationId",
        "studyPeriodId",
        "qualificationId",
        timestamp
        )
      `;
  }
}
