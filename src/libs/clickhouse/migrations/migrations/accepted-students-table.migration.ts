import { BaseClickhouseMigration } from './base-migration';

export class CreateAcceptedStudentsTableClickhouseMigration extends BaseClickhouseMigration {
  getDescription(): string {
    return `Creates accepted students table https://pw-tech.atlassian.net/browse/IT-4821`;
  }

  getQuery(): string {
    return `
      CREATE TABLE IF NOT EXISTS ithub.accepted_students
      (
          "studentId" UUID,
          "specialityId" UUID,
          "suborganizationId" UUID,
          "organizationId" UUID,
          "studyPeriodId" UUID,
          "timestamp" DateTime
      )
      ENGINE = MergeTree()
      PRIMARY KEY (
        "studentId",
        "specialityId",
        "suborganizationId",
        "organizationId",
        "studyPeriodId",
        timestamp
        )
      `;
  }
}
