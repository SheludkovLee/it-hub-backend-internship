export interface IMigration {
  getDescription(): string;

  getQuery(): string;
}

export abstract class BaseClickhouseMigration implements IMigration {
  abstract getDescription(): string;

  abstract getQuery(): string;
}
