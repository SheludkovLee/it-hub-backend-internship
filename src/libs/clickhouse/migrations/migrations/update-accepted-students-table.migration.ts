// alter table accepted_students
//     add column qualificationId Nullable(UUID);

import { BaseClickhouseMigration } from './base-migration';

export class UpdateAcceptedStudentsTableClickhouseMigration extends BaseClickhouseMigration {
  getDescription(): string {
    return `Update accepted students table (qualificationId)`;
  }

  getQuery(): string {
    return `
      ALTER TABLE ithub.accepted_students
      ADD COLUMN IF NOT EXISTS qualificationId UUID;
`;
  }
}
