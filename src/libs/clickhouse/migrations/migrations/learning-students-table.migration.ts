import { BaseClickhouseMigration } from './base-migration';

export class CreateLearningStudentsTableClickhouseMigration extends BaseClickhouseMigration {
  getDescription(): string {
    return `Creates learning students table https://pw-tech.atlassian.net/browse/IT-4821`;
  }

  getQuery(): string {
    return `
      CREATE TABLE IF NOT EXISTS ithub.learning_students
      (
          "studentId" UUID,
          "specialityId" UUID,
          "suborganizationId" UUID,
          "organizationId" UUID,
          "studyPeriodId" UUID,
          "timestamp" DateTime
      )
      ENGINE = MergeTree()
      PRIMARY KEY (
        "studentId",
        "specialityId",
        "suborganizationId",
        "organizationId",
        "studyPeriodId",
        timestamp
        )
      `;
  }
}
