import { ClickHouseClient } from '@clickhouse/client';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { featureFlags } from 'src/infra/feature-flags/flags';

import { ClickhouseClientProvider } from '../clickhouse.client';
import { ClickhouseMigrationsSource } from './migrations-source';

interface IMigrator {
  runMigrations(): Promise<boolean>;
}

@Injectable()
export class ClickhouseMigrator implements IMigrator {
  constructor(
    @Inject(ClickhouseClientProvider.provide)
    private readonly clickhouseClient: ClickHouseClient,
    private readonly migrationsSource: ClickhouseMigrationsSource,
  ) {
    this.runMigrations();
  }

  async runMigrations(): Promise<boolean> {
    if (!featureFlags.clickhouse) {
      return false;
    }

    try {
      const migrations = await this.migrationsSource.getMigrations();

      for await (const migration of migrations) {
        await this.clickhouseClient.command({
          query: migration.getQuery(),
        });
      }

      return true;
    } catch (error) {
      Logger.error(error);

      throw error;
    }
  }
}
