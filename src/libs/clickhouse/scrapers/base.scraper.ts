import { IClickhouseBufferRegister } from '../buffer/buffer-register';

export interface IClickhouseEventsScraper {
  scrape(): Promise<boolean>;
}

export abstract class BaseClickhouseEventsScraper
  implements IClickhouseEventsScraper
{
  constructor(protected buffer: IClickhouseBufferRegister) {}

  abstract scrape(): Promise<boolean>;
}
