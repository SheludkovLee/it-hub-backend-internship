import { FluentClient } from '@fluent-org/logger';
import * as winston from 'winston';
import * as Transport from 'winston-transport';

export class FluentdWinstonTransport extends Transport {
  constructor(
    baseOptions: winston.transport.TransportStreamOptions,
    private readonly fluentClient: FluentClient,
  ) {
    super(baseOptions);
  }

  public log(info: any, next: () => void) {
    setImmediate(async () => {
      try {
        await this.fluentClient.emit(info);
      } catch (error) {}
    });

    next();
  }
}
