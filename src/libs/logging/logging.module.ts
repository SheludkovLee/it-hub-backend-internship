import { FluentClient } from '@fluent-org/logger';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { utilities, WinstonModule } from 'nest-winston';
import { ClsService } from 'nestjs-cls';
import { featureFlags } from 'src/infra/feature-flags/flags';
import * as winston from 'winston';

import { FluentdWinstonTransport } from './transports/fluentd.transport';

@Module({
  imports: [
    WinstonModule.forRootAsync({
      useFactory: async (config: ConfigService, cls: ClsService) => {
        const transports: winston.transport[] = [
          new winston.transports.Console({
            format: winston.format.combine(
              winston.format.timestamp(),
              utilities.format.nestLike('IThub', {
                colors: true,
                prettyPrint: true,
              }),
            ),
          }),
        ];

        if (featureFlags.fluentdLogging) {
          const fluentClient = new FluentClient('fluentd.test', {
            socket: {
              host: config.get('fluentd.host'),
              port: parseInt(config.get('fluentd.port')),
              timeout: 3000,
            },
          });

          await fluentClient.connect();

          transports.push(new FluentdWinstonTransport({}, fluentClient));
        }

        return {
          transports: transports,
          format: winston.format((info) => {
            if (cls.isActive()) {
              info.reqId = cls.getId();
              info.userId = cls.get('userId');
            }

            return info;
          })(),
        };
      },
      inject: [ConfigService, ClsService],
    }),
  ],
  exports: [WinstonModule],
})
export class LoggingModule {}
