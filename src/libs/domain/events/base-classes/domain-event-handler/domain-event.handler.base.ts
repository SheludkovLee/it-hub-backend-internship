import { UnitOfWorkPort } from 'src/libs/database';
import { IBusHandler } from 'src/libs/events';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { listenToEventsDecoratorKey } from '../../constants';
import { DomainEventBase } from '../domain-event';
import { DomainEventsPublisher } from '../domain-event-publisher';

export abstract class DomainEventHandlerBase<
  UnitOfWork extends UnitOfWorkPort = UnitOfWorkPort,
  Event extends DomainEventBase = DomainEventBase,
> implements IBusHandler<Event>
{
  constructor(
    protected readonly unitOfWork: UnitOfWork,
    private readonly domainEventsPublisher: DomainEventsPublisher,
  ) {
    const tokens = Reflect.getMetadata(
      listenToEventsDecoratorKey,
      this.constructor,
    );

    if (!tokens) {
      throw new Error(`${this.constructor} must have EventsHandler decorator`);
    }

    this.domainEventsPublisher.register(tokens, this);
  }

  abstract handle(event: Event): Promise<Result<unknown, ExceptionBase>>;
}
