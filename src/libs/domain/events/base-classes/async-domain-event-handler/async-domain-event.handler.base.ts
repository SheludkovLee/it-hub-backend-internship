import { UnitOfWorkPort } from 'src/libs/database';
import { IBusHandler } from 'src/libs/events';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { listenToEventsDecoratorKey } from '../../constants';
import { AsyncDomainEventsPublisher } from '../async-domain-event-publisher';
import { DomainEventBase } from '../domain-event';

export abstract class AsyncDomainEventHandlerBase<
  UnitOfWork extends UnitOfWorkPort = UnitOfWorkPort,
  Event extends DomainEventBase = DomainEventBase,
> implements IBusHandler<Event>
{
  constructor(
    protected readonly unitOfWork: UnitOfWork,
    private readonly domainEventsPublisher: AsyncDomainEventsPublisher,
  ) {
    const tokens = Reflect.getMetadata(
      listenToEventsDecoratorKey,
      this.constructor,
    );

    if (!tokens) {
      throw new Error(`${this.constructor} must have EventsHandler decorator`);
    }

    this.domainEventsPublisher.register(tokens, this);
  }

  abstract handle(event: Event): Promise<Result<unknown, ExceptionBase>>;
}
