import { domainEventDecoratorKey } from 'src/libs/domain';
import { EventBase, EventProps } from 'src/libs/events';

export type DomainEventProps<Payload extends Record<string, any>> =
  EventProps<Payload>;

export abstract class DomainEventBase<
  Payload extends Record<string, any> = Record<string, any>,
> extends EventBase<Payload> {
  constructor(props: DomainEventProps<Payload>) {
    super(props);
    const token = Reflect.getMetadata(
      domainEventDecoratorKey,
      this.constructor,
    );
    if (!token) {
      throw new Error(
        `${this.constructor.name} must have DomainEvent decorator`,
      );
    }
    this.token = token;
  }
}
