import { Bus, IBusHandler } from 'src/libs/events';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { UUID } from '../../../value-objects';
import { DomainEventBase } from '../domain-event';

export class AsyncDomainEventsPublisher {
  constructor(
    private readonly bus: Bus<DomainEventBase, IBusHandler<DomainEventBase>>,
  ) {}

  async emit(
    event: DomainEventBase,
    initiatorUserId?: string,
  ): Promise<Result<unknown, ExceptionBase>> {
    event.trxId = UUID.generate().value;
    event.initiatorUserId = initiatorUserId;

    return this.bus.emit(event);
  }

  emitBulk(
    events: DomainEventBase[],
    initiatorUserId?: string,
  ): Promise<Result<unknown, ExceptionBase>> {
    return this.bus.emitBulk(
      events.map((event) => {
        event.trxId = UUID.generate().value;
        event.initiatorUserId = initiatorUserId;
        return event;
      }),
    );
  }

  register(eventTokens: string, handler: IBusHandler<DomainEventBase>): void;
  register(eventTokens: string[], handler: IBusHandler<DomainEventBase>): void;
  register(
    eventTokens: string[] | string,
    handler: IBusHandler<DomainEventBase>,
  ): void {
    if (Array.isArray(eventTokens)) {
      eventTokens.map((eventToken) => this.bus.register(eventToken, handler));
    } else {
      this.bus.register(eventTokens, handler);
    }
  }
}
