import { domainEventDecoratorKey } from '../../constants';

export function DomainEvent(token: string) {
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  return (target) => {
    if (!token) {
      throw new Error(`${target.name} event token is not provided`);
    }

    Reflect.defineMetadata(domainEventDecoratorKey, token, target);

    return target;
  };
}
