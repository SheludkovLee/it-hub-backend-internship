import { DomainEventBase } from 'src/libs/domain/events';
import { Constructor } from 'src/libs/types';

import {
  domainEventDecoratorKey,
  listenToEventsDecoratorKey,
} from '../../constants';

export function EventsHandler(...events: Constructor<DomainEventBase>[]) {
  return (target: any) => {
    const eventTokens = events.map((event) => {
      const token = Reflect.getMetadata(domainEventDecoratorKey, event);

      if (!token) {
        throw new Error(`${event.name} is not have DomainEvent decorator`);
      }

      return token;
    });

    Reflect.defineMetadata(listenToEventsDecoratorKey, eventTokens, target);

    return target;
  };
}
