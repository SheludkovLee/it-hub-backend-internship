import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { Rule } from '../rule';

export class RulesChecker<
  Context extends Record<string, any> = Record<string, any>,
> {
  constructor(private readonly context: Context) {}

  public checkRules(rules: Rule<Context>[]): Result<void, ExceptionBase> {
    rules.forEach((rule) => (rule.context = this.context));
    const ruleCheckResults = rules.map((rule) => rule.execute());
    const violatedRule = ruleCheckResults.find((result) => result.isErr);
    return violatedRule || Result.ok();
  }
}
