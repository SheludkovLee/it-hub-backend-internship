import { DomainEventBase } from '../../events';
import { Entity } from '../entity';

export abstract class AggregateRoot<EntityProps> extends Entity<EntityProps> {
  private _domainEvents: DomainEventBase[] = [];

  get domainEvents(): DomainEventBase[] {
    return this._domainEvents;
  }

  protected addEvent(domainEvent: DomainEventBase): void {
    if (this._domainEvents.some((event) => event.id === domainEvent.id)) {
      return;
    }

    this._domainEvents.push(domainEvent);
  }

  public clearEvents(): void {
    this._domainEvents = [];
  }
}
