import {
  ArgumentInvalidException,
  ArgumentOutOfRangeException,
} from 'src/libs/exceptions';

import { DateVO, ID } from '../../value-objects';
import { RulesChecker } from '../rules-checker';

export interface BaseEntityProps {
  id: ID;
  createdAt: DateVO;
  updatedAt: DateVO;
  deletedAt: DateVO | null;
}

export interface CreateEntityProps<T> {
  id: ID;
  props: T;
  createdAt?: DateVO;
  updatedAt?: DateVO;
  deletedAt?: DateVO | null;
}

export abstract class Entity<EntityProps> {
  protected constructor({
    id,
    createdAt,
    updatedAt,
    props,
  }: CreateEntityProps<EntityProps>) {
    this.setId(id);
    this.validateProps(props);

    const now = DateVO.now();
    this._createdAt = createdAt || now;
    this._updatedAt = updatedAt || now;
    this._deletedAt = null;
    this.props = props;
    this.rulesChecker = new RulesChecker<EntityProps>(this.props);
    this.validate();
  }

  protected readonly props: EntityProps;

  protected abstract _id: ID;

  private readonly _createdAt: DateVO;
  private _updatedAt: DateVO;
  private _deletedAt: DateVO | null;

  protected readonly rulesChecker: RulesChecker<EntityProps>;

  get id(): ID {
    return this._id;
  }

  private setId(id: ID): void {
    this._id = id;
  }

  get createdAt(): DateVO {
    return this._createdAt;
  }

  get updatedAt(): DateVO {
    return this._updatedAt;
  }

  get deletedAt(): DateVO | null {
    return this._deletedAt;
  }

  protected updatedAtNow(): void {
    this._updatedAt = DateVO.now();
  }

  protected deletedAtNow(): void {
    this._deletedAt = DateVO.now();
  }

  static isEntity(entity: unknown): entity is Entity<unknown> {
    return entity instanceof Entity;
  }

  public equals(object?: Entity<EntityProps>): boolean {
    if (object === null || object === undefined) {
      return false;
    }

    if (this === object) {
      return true;
    }

    if (!Entity.isEntity(object)) {
      return false;
    }

    return this.id ? this.id.equals(object.id) : false;
  }

  public getPropsCopy(): EntityProps & BaseEntityProps {
    const propsCopy = {
      ...this.props,
      id: this._id,
      createdAt: this._createdAt,
      updatedAt: this._updatedAt,
      deletedAt: this._deletedAt,
    };
    return Object.freeze(propsCopy);
  }

  public toObject(): unknown {
    const result = {
      id: this._id.value,
      createdAt: this._createdAt.value,
      updatedAt: this._updatedAt.value,
      deletedAt: this._deletedAt?.value || null,
      ...this.props,
    };

    return Object.freeze(result);
  }

  public abstract validate(): void;

  private validateProps(props: EntityProps): void {
    const maxProps = 50;

    // if (ValidationGuard.isEmpty(props)) { // Проверка говна! Убрал ее, пропсы могут быть пустыми
    //   throw new ArgumentNotProvidedException(
    //     'Entity props should not be empty',
    //   );
    // }
    if (typeof props !== 'object') {
      throw new ArgumentInvalidException('Entity props should be an object');
    }
    if (Object.keys(props).length > maxProps) {
      throw new ArgumentOutOfRangeException(
        `Entity props should not have more than ${maxProps} properties`,
      );
    }
  }
}
