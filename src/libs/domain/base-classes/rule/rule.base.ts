import { ExceptionBase, InvalidOperationException } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

export abstract class Rule<
  Context extends Record<string, any> = Record<string, any>,
> {
  protected errorMessage: string;
  public context: Context;

  constructor(errorMessage?: string) {
    this.errorMessage = errorMessage || 'Rule violated';
  }

  protected abstract check(): boolean;

  public execute(): Result<void, ExceptionBase> {
    if (!this.check()) {
      return Result.fail(new InvalidOperationException(this.errorMessage));
    }
    return Result.ok();
  }
}
