import { DomainPrimitive, ValueObject } from '../value-object.base';

export class FileVO extends ValueObject<string> {
  constructor(url: string) {
    super({ value: url });
  }

  public get url(): string {
    return this.props.value;
  }

  protected validate({ value }: DomainPrimitive<string>): void {
    return;
  }
}
