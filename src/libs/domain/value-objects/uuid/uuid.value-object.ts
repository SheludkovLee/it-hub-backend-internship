import { ArgumentInvalidException } from 'src/libs/exceptions';
import { v4 as uuidV4, validate } from 'uuid';

import { ID } from '../id';
import { DomainPrimitive } from '../value-object.base';

export class UUID extends ID {
  static generate(): UUID {
    return new UUID(uuidV4());
  }

  static create(uuid: string): UUID {
    return new UUID(uuid);
  }

  protected validate({ value }: DomainPrimitive<string>): void {
    if (!validate(value)) {
      throw new ArgumentInvalidException('Incorrect UUID format');
    }
  }
}
