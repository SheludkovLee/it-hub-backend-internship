import { ArgumentNotProvidedException } from 'src/libs/exceptions';
import { ValidationGuard } from 'src/libs/utils';

export type Primitives = string | number | boolean;

export interface DomainPrimitive<T extends Primitives | Date> {
  value: T;
}

export type ValueObjectProps<T> = T extends Primitives | Date
  ? DomainPrimitive<T>
  : T;

export abstract class ValueObject<T> {
  protected readonly props: ValueObjectProps<T>;

  constructor(props: ValueObjectProps<T>) {
    this.checkIfEmpty(props);
    this.validate(props);
    this.props = props;
  }

  protected abstract validate(props: ValueObjectProps<T>): void;

  static isValueObject(obj: unknown): obj is ValueObject<unknown> {
    return obj instanceof ValueObject;
  }

  public equals(vo?: ValueObject<T>): boolean {
    if (vo === null || vo === undefined) {
      return false;
    }
    return JSON.stringify(this) === JSON.stringify(vo);
  }

  private checkIfEmpty(props: ValueObjectProps<T>): void {
    if (ValidationGuard.isEmpty(props)) {
      throw new ArgumentNotProvidedException(
        `Property of Value Object "${this.constructor.name}" cannot be empty, current value is ${props}`,
      );
    }

    if (this.isDomainPrimitive(props) && ValidationGuard.isEmpty(props.value)) {
      throw new ArgumentNotProvidedException(
        `Property value of Value Object "${this.constructor.name}" cannot be empty, cause it is domain primitive. Current value is ${props.value}`,
      );
    }
  }

  private isDomainPrimitive(
    obj: unknown,
  ): obj is DomainPrimitive<T & (Primitives | Date)> {
    return !!Object.prototype.hasOwnProperty.call(obj, 'value');
  }
}
