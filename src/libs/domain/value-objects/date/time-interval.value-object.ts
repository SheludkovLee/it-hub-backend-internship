import { ArgumentInvalidException } from 'src/libs/exceptions';

import { ValueObject } from '../value-object.base';
import { TimeVO } from './time.value-object';

export interface TimeIntervalProps {
  from: string;
  to: string;
}

export interface TimeIntervalVOProps {
  from: TimeVO;
  to: TimeVO;
}

export class TimeIntervalVO extends ValueObject<TimeIntervalVOProps> {
  constructor(props: TimeIntervalProps) {
    super({
      from: TimeVO.fromString(props.from),
      to: TimeVO.fromString(props.to),
    });
  }

  protected validate(props: TimeIntervalVOProps): void {
    if (props.from.isAfter(props.to)) {
      throw new ArgumentInvalidException(
        'Конец интервала должен быть позже чем начало',
      );
    }
    if (props.from.equals(props.to)) {
      throw new ArgumentInvalidException(
        'Начало и конец интервала должны быть различны',
      );
    }
  }

  get from() {
    return this.props.from;
  }

  get to() {
    return this.props.to;
  }

  toJSON(): TimeIntervalProps {
    return {
      from: this.props.from.toString(),
      to: this.props.to.toString(),
    };
  }

  intersection(other: TimeIntervalVO): boolean {
    return (
      (this.from.isAfterOrEqual(other.from) && this.from.isBefore(other.to)) ||
      (this.to.isAfter(other.from) && this.to.isBeforeOrEqual(other.to)) ||
      (other.from.isAfterOrEqual(this.from) && other.from.isBefore(this.to)) ||
      (other.to.isAfter(this.from) && other.to.isBeforeOrEqual(this.to)) ||
      this.equals(other)
    );
  }

  isInside(other: TimeIntervalVO): boolean {
    return (
      other.from.isBeforeOrEqual(this.from) && other.to.isAfterOrEqual(this.to)
    );
  }
}
