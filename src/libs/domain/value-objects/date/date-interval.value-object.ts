import { OpUnitType } from 'dayjs';
import { ArgumentInvalidException } from 'src/libs/exceptions';

import { ValueObject } from '../value-object.base';
import { DateVO } from './date.value-object';

export interface DateIntervalProps {
  start: string;
  end: string;
  canBeEquals?: boolean;
}

export interface DateIntervalVOProps {
  start: DateVO;
  end: DateVO;
  canBeEquals?: boolean;
}

export class DateIntervalVO extends ValueObject<DateIntervalVOProps> {
  constructor(props: DateIntervalProps) {
    super({
      start: new DateVO(props.start),
      end: new DateVO(props.end),
      canBeEquals: props.canBeEquals,
    });
  }
  protected validate(props: DateIntervalVOProps): void {
    if (props.start.isAfter(props.end)) {
      throw new ArgumentInvalidException(
        'Конец интервала должен быть позже чем начало',
      );
    }
    if (!props.canBeEquals && props.start.equals(props.end)) {
      throw new ArgumentInvalidException(
        'Начало и конец интервала должны быть различны',
      );
    }
  }

  toJSON(): DateIntervalProps {
    return {
      start: this.props.start.value.toISOString(),
      end: this.props.end.value.toISOString(),
      canBeEquals: this.props.canBeEquals,
    };
  }

  get start() {
    return this.props.start;
  }

  get end() {
    return this.props.end;
  }

  get canBeEquals() {
    return this.props.canBeEquals;
  }

  intersection(other: DateIntervalVO): boolean {
    return (
      (this.start.isAfterOrEqual(other.start) &&
        this.start.isBefore(other.end)) ||
      (this.end.isAfter(other.start) && this.end.isBeforeOrEqual(other.end)) ||
      (other.start.isAfterOrEqual(this.start) &&
        other.start.isBefore(this.end)) ||
      (other.end.isAfter(this.start) && other.end.isBeforeOrEqual(this.end)) ||
      this.equals(other)
    );
  }

  isInside(other: DateIntervalVO): boolean {
    return (
      other.start.isBeforeOrEqual(this.start) &&
      other.end.isAfterOrEqual(this.end)
    );
  }

  isActual(): boolean {
    return (
      this.start.isBeforeOrEqual(DateVO.now()) &&
      this.end.isAfterOrEqual(DateVO.now())
    );
  }

  getLength(unit: OpUnitType): number {
    return this.end.diff(this.start, unit);
  }

  format(template: string): string {
    return `${this.start.format(template)}-${this.end.format(template)}`;
  }
}
