import 'dayjs/locale/ru';

import * as dayjs from 'dayjs';
import { Weekday } from 'src/libs/domain';
import { ArgumentInvalidException } from 'src/libs/exceptions';

import { DomainPrimitive, ValueObject } from '../value-object.base';

type UnitTypeShort = 'd' | 'D' | 'M' | 'y' | 'h' | 'm' | 's' | 'ms';

type UnitTypeLong =
  | 'millisecond'
  | 'second'
  | 'minute'
  | 'hour'
  | 'day'
  | 'month'
  | 'year'
  | 'date';

type UnitTypeLongPlural =
  | 'milliseconds'
  | 'seconds'
  | 'minutes'
  | 'hours'
  | 'days'
  | 'months'
  | 'years'
  | 'dates';

type UnitType = UnitTypeLong | UnitTypeLongPlural | UnitTypeShort;

type OpUnitType = UnitType | 'week' | 'weeks' | 'w';

dayjs.locale('ru');

export class DateVO extends ValueObject<Date> {
  dayjsValue: dayjs.Dayjs;

  static create(value: Date | string | number): DateVO {
    return new DateVO(value);
  }

  constructor(value: Date | string | number) {
    const date = new Date(value);

    super({ value: date });

    this.dayjsValue = dayjs(value);
  }

  get value(): Date {
    return this.props.value;
  }

  toDateWithoutTime(): DateVO {
    return new DateVO(this.value.toDateString());
  }

  static now(): DateVO {
    return new DateVO(Date.now());
  }

  toISOString(): string {
    return this.value.toISOString();
  }

  toUnix(): number {
    return this.dayjsValue.unix();
  }

  get timestamp() {
    return this.toUnix();
  }

  day(startingPoint = Weekday.monday): 0 | 1 | 2 | 3 | 4 | 5 | 6 {
    const MODULE = 7;
    const mapping = {
      [Weekday.monday]: 1,
      [Weekday.tuesday]: 2,
      [Weekday.wednesday]: 3,
      [Weekday.thursday]: 4,
      [Weekday.friday]: 5,
      [Weekday.saturday]: 6,
      [Weekday.sunday]: 0,
    };
    const day = this.value.getDay();
    let normalized = day - mapping[startingPoint];
    if (normalized < 0) {
      normalized = MODULE + normalized;
    }
    return (normalized % MODULE) as 0 | 1 | 2 | 3 | 4 | 5 | 6;
  }

  setHours(hours: number, minutes?: number): DateVO {
    const date = this.dayjsValue
      .set('hours', hours)
      .set('minutes', minutes)
      .toDate();

    return new DateVO(date);
  }

  addYears(years: number): DateVO {
    const date = this.dayjsValue.add(years, 'years').toDate();

    return new DateVO(date);
  }

  addMonths(months: number): DateVO {
    const date = this.dayjsValue.add(months, 'months').toDate();

    return new DateVO(date);
  }

  addWeeks(weeks: number): DateVO {
    const date = this.dayjsValue.add(weeks, 'weeks').toDate();

    return new DateVO(date);
  }

  addDays(days: number): DateVO {
    const date = this.dayjsValue.add(days, 'day').toDate();

    return new DateVO(date);
  }

  addHours(hours: number): DateVO {
    const date = this.dayjsValue.add(hours, 'hours').toDate();

    return new DateVO(date);
  }

  addMinute(minute: number): DateVO {
    const date = this.dayjsValue.add(minute, 'minute').toDate();

    return new DateVO(date);
  }

  addSeconds(seconds: number): DateVO {
    const date = this.dayjsValue.add(seconds, 'second').toDate();

    return new DateVO(date);
  }

  diff(date: DateVO, unit: OpUnitType, absolute = false): number {
    const value = this.dayjsValue.diff(date.dayjsValue, unit);

    return absolute ? Math.abs(value) : value;
  }

  diffInDays(date: DateVO, absolute = false): number {
    const value = this.dayjsValue.diff(date.dayjsValue, 'days');

    return absolute ? Math.abs(value) : value;
  }

  diffInMinutes(date: DateVO): number {
    return this.dayjsValue.diff(date.dayjsValue, 'm');
  }

  diffInMs(date: DateVO): number {
    return this.dayjsValue.diff(date.dayjsValue, 'ms');
  }

  isBefore(date: DateVO, unit: OpUnitType = 'milliseconds'): boolean {
    return this.dayjsValue.isBefore(date.dayjsValue, unit);
  }

  isAfter(date: DateVO, unit: OpUnitType = 'milliseconds'): boolean {
    return this.dayjsValue.isAfter(date.dayjsValue, unit);
  }

  isInPast(): boolean {
    return this.isBefore(DateVO.now());
  }

  isInFuture(): boolean {
    return this.isAfter(DateVO.now());
  }

  isSame(date: DateVO, unit: OpUnitType = 'milliseconds'): boolean {
    return this.dayjsValue.isSame(date.dayjsValue, unit);
  }

  startOf(unit: OpUnitType): DateVO {
    return new DateVO(this.dayjsValue.startOf(unit).toDate());
  }

  endOf(unit: OpUnitType): DateVO {
    return new DateVO(this.dayjsValue.endOf(unit).toDate());
  }

  format(template?: string) {
    return this.dayjsValue.format(template);
  }

  isAfterOrEqual(date: DateVO): boolean {
    return this.isAfter(date) || this.isSame(date);
  }

  isBeforeOrEqual(date: DateVO): boolean {
    return this.isBefore(date) || this.isSame(date);
  }

  protected validate({ value }: DomainPrimitive<Date>): void {
    if (!(value instanceof Date) || Number.isNaN(value.getTime())) {
      throw new ArgumentInvalidException(
        `Передана некорректная дата ${value} `,
      );
    }
  }

  equals(vo?: DateVO): boolean {
    return this.dayjsValue.isSame(vo.dayjsValue);
  }
}
