import { ArgumentInvalidException } from 'src/libs/exceptions';

import { DomainPrimitive, ValueObject } from '../value-object.base';

export enum Weekday {
  monday = 'monday',
  tuesday = 'tuesday',
  wednesday = 'wednesday',
  thursday = 'thursday',
  friday = 'friday',
  saturday = 'saturday',
  sunday = 'sunday',
}

export class WeekdayVO extends ValueObject<Weekday> {
  constructor(value: Weekday) {
    super({ value });

    this.validate({ value });
  }

  protected validate({ value }: DomainPrimitive<string>): void {
    if (!Object.values(Weekday).includes(value as Weekday)) {
      throw new ArgumentInvalidException(`Некорректный день недели: ${value}`);
    }
  }

  public get value(): Weekday {
    return this.props.value;
  }
}
