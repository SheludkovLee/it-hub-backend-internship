import { ArgumentInvalidException } from 'src/libs/exceptions';
import { ValidationGuard } from 'src/libs/utils';

import { ValueObject } from '../value-object.base';

export interface TimeVOProps {
  hours: number;
  minutes: number;
}

export class TimeVO extends ValueObject<TimeVOProps> {
  constructor(hours: number, minutes: number) {
    super({ hours, minutes });
  }

  protected validate(props: TimeVOProps): void {
    if (props.hours < 0 || props.hours > 23) {
      throw new ArgumentInvalidException('Hours must be between 0 and 23');
    }
    if (props.minutes < 0 || props.minutes > 59) {
      throw new ArgumentInvalidException('Minutes must be between 0 and 59');
    }
  }

  public get hours() {
    return this.props.hours;
  }

  public get minutes() {
    return this.props.minutes;
  }

  toString(): string {
    const hours = this.props.hours.toString().padStart(2, '0');
    const minutes = this.props.minutes.toString().padStart(2, '0');
    return `${hours}:${minutes}`;
  }

  toJSON(): string {
    return this.toString();
  }

  isAfter(time: TimeVO): boolean {
    if (this.props.hours > time.props.hours) {
      return true;
    }
    if (this.props.hours < time.props.hours) {
      return false;
    }
    return this.props.minutes > time.props.minutes;
  }

  isAfterOrEqual(time: TimeVO): boolean {
    return this.isAfter(time) || this.equals(time);
  }

  isBefore(time: TimeVO): boolean {
    return !this.isAfterOrEqual(time);
  }

  isBeforeOrEqual(time: TimeVO): boolean {
    return !this.isAfter(time);
  }

  static fromString(timeString: string): TimeVO {
    const match = ValidationGuard.isTimeString(timeString);

    if (!match) {
      throw new ArgumentInvalidException('Невалидный формат времени');
    }

    const [hours, minutes] = timeString
      .split(':')
      .map((el) => parseInt(el, 10));

    return new TimeVO(hours, minutes);
  }
}
