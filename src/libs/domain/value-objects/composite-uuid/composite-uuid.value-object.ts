import { ArgumentInvalidException } from 'src/libs/exceptions';
import { validate } from 'uuid';

import { ID } from '../id';
import { DomainPrimitive } from '../value-object.base';

export class CompositeUUID extends ID {
  public static readonly separator = '|';
  protected constructor(...props: string[]) {
    super(props.join(CompositeUUID.separator));
  }

  get UUIDs() {
    return this.value.split(CompositeUUID.separator);
  }

  private splitUUIDs(uuid: string): string[] {
    return uuid.split(CompositeUUID.separator);
  }

  static create(...uuids: string[]): CompositeUUID {
    return new CompositeUUID(...uuids);
  }

  protected validate({ value }: DomainPrimitive<string>): void {
    for (const uuid of this.splitUUIDs(value)) {
      if (!validate(uuid)) {
        throw new ArgumentInvalidException('Incorrect UUID format');
      }
    }
  }

  equals(vo?: CompositeUUID): boolean {
    const uuids1 = this.splitUUIDs(this.props.value);
    const uuids2 = this.splitUUIDs(vo.props.value);

    return uuids1.sort().join() === uuids2.sort().join();
  }
}
