export const calculatePercentage = (
  partialValue: number,
  totalValue: number,
): number => {
  return (100 * partialValue) / totalValue;
};
