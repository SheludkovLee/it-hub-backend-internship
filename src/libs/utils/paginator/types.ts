export interface PagePagination {
  page: number;
  pageSize: number;
}
