import { QueryBuilder } from 'objection';
import { ObjectionOrmEntityBase } from 'src/libs/database';
import { PaginatedResponse } from 'src/libs/types';

import { PagePagination } from './types';

export class Paginator {
  static async paginate<TOrmEntity extends ObjectionOrmEntityBase>(
    qb: QueryBuilder<TOrmEntity>,
    options: PagePagination,
  ): Promise<PaginatedResponse<TOrmEntity>> {
    const pageSize = options.pageSize ?? 0;
    const page = options.page ?? 1;
    const pageIndex = page - 1;

    const { results, total } = await qb.page(pageIndex, pageSize);

    const totalPages = Math.ceil(total / pageSize);
    const hasMore = total > page * pageSize;

    return {
      items: results,
      hasMore,
      page,
      perPage: results.length,
      totalPages,
      total,
    };
  }

  static async paginateLocal<Item>(
    items: Item[],
    options: PagePagination,
  ): Promise<PaginatedResponse<Item>> {
    const pageSize = options.pageSize ?? 0;
    const page = options.page ?? 1;

    const results = items.slice(pageSize * (page - 1), pageSize * page);
    const total = items.length;

    const totalPages = Math.ceil(total / pageSize);
    const hasMore = total > page * pageSize;

    return {
      items: results,
      hasMore,
      page,
      perPage: results.length,
      totalPages,
      total,
    };
  }
}
