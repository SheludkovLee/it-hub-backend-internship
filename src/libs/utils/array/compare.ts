type Comparator<T> = (a: T, b: T) => boolean;

const defaultComparator: Comparator<unknown> = (a, b) => a === b;

export function compare<T>(
  oldVersion: T[],
  newVersion: T[],
  comparator: Comparator<T> = defaultComparator,
): { deletedValues: T[]; addedValues: T[] } {
  const deletedValues = oldVersion.filter(
    (oldValue) =>
      !newVersion.find((newValue) => comparator(oldValue, newValue)),
  );

  const addedValues = newVersion.filter(
    (newValue) =>
      !oldVersion.find((oldValue) => comparator(newValue, oldValue)),
  );

  return { deletedValues: deletedValues, addedValues: addedValues };
}
