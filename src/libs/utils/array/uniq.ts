export const uniq = <T>(array: T[]) => {
  return [...new Set(array)];
};
