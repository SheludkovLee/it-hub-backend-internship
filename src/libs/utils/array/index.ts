import { differenceBy } from './difference';
import { groupBy } from './group-by';
import { uniq } from './uniq';

export const arrayUtils = {
  uniq,
  groupBy,
  difference: differenceBy,
};
