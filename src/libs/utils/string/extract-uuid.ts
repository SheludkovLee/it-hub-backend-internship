import { uuidRegex } from '../regexp/uuid.regexp';

export function extractOneUuidFromString(inputString: string): string | null {
  const match = inputString.match(uuidRegex);
  return match ? match[0] : null;
}
