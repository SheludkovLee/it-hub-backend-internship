import { extractOneUuidFromString } from './extract-uuid';

describe('extractOneUuidFromString function', () => {
  it('should extract one UUID from a string', () => {
    const inputString =
      'Some text with a UUID: 123e4567-e89b-12d3-a456-426655440000';
    const extractedUuid = extractOneUuidFromString(inputString);

    // Assert that the extractedUuid matches the expected UUID
    expect(extractedUuid).toEqual('123e4567-e89b-12d3-a456-426655440000');
  });

  it('should return null if no UUID is found', () => {
    const inputString = 'This string does not contain any UUIDs.';
    const extractedUuid = extractOneUuidFromString(inputString);

    // Assert that extractedUuid is null
    expect(extractedUuid).toBeNull();
  });
});
