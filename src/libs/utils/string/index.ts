import { toCamelCase } from './to-camel-case';
import { wrap } from './wrap';

export const stringUtils = {
  toCamelCase,
  wrap,
};
