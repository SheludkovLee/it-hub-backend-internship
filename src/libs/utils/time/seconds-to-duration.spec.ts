import { secondsToDuration } from './seconds-to-duration';

describe('secondsToDuration', () => {
  it('should convert 30 seconds to "00:00:30"', () => {
    expect(secondsToDuration(30)).toBe('00:00:30');
  });

  it('should convert 3600 seconds to "01:00:00"', () => {
    expect(secondsToDuration(3600)).toBe('01:00:00');
  });

  it('should convert 3661 seconds to "01:01:01"', () => {
    expect(secondsToDuration(3661)).toBe('01:01:01');
  });

  // Add more test cases as needed
});
