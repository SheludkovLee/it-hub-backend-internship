import { digitRegexp, emailRegexp, timeRegexp, urlRegexp } from './regexp';

export class ValidationGuard {
  static isEmpty(value: unknown): boolean {
    if (typeof value === 'number' || typeof value === 'boolean') {
      return false;
    }
    if (typeof value === 'undefined' || value === null) {
      return true;
    }
    if (value instanceof Date) {
      return false;
    }
    if (value instanceof Object && !Object.keys(value).length) {
      return true;
    }
    if (Array.isArray(value)) {
      if (value.length === 0) {
        return true;
      }
      if (value.every((item) => ValidationGuard.isEmpty(item))) {
        return true;
      }
    }

    return value === '';
  }

  static lengthIsBetween(
    value: number | string | Array<unknown>,
    min: number,
    max: number,
  ): boolean {
    if (ValidationGuard.isEmpty(value)) {
      throw new Error(
        'Cannot check length of a value. Provided value is empty',
      );
    }
    const valueLength =
      typeof value === 'number'
        ? Number(value).toString().length
        : value.length;

    return valueLength >= min && valueLength <= max;
  }

  static isNumeric(value: string): boolean {
    if (typeof value !== 'string') {
      throw new Error('Value of numeric string must be a string');
    }

    return digitRegexp.test(value);
  }

  static isEmail(value: string): boolean {
    if (typeof value !== 'string') {
      throw new Error('Value of email must be a string');
    }

    return emailRegexp.test(value);
  }

  static isUrl(value: string): boolean {
    if (typeof value !== 'string') {
      throw new Error('Value of url must be a string');
    }

    return urlRegexp.test(value);
  }

  static isEnum(value: any, enumObj: object): boolean {
    return Object.keys(enumObj)
      .map((key) => enumObj[key])
      .includes(value);
  }

  static isTimeString(value: string): boolean {
    return timeRegexp.test(value);
  }
}
