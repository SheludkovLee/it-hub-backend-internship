import { isEmpty } from './isEmpty';
import { isEqual } from './isEqual';
import { mapKeys } from './map-keys';
import { merge } from './merge';
import { omit } from './omit';

export const objectUtils = {
  isEmpty,
  isEqual,
  mapKeys,
  merge,
  omit,
};
