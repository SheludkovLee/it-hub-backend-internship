import { ApolloServerErrorCode } from '@apollo/server/errors';
import { ApolloServerPluginLandingPageLocalDefault } from '@apollo/server/plugin/landingPage/default';
import { ApolloDriverConfig, ApolloDriverConfigFactory } from '@nestjs/apollo';
import { Logger } from '@nestjs/common';
import * as Sentry from '@sentry/node';
import { join } from 'path';

import { UUIDScalar } from '../scalars';

export class GraphqlConfigService implements ApolloDriverConfigFactory {
  createGqlOptions(): ApolloDriverConfig {
    const plugins: ApolloDriverConfig['plugins'] = [
      ApolloServerPluginLandingPageLocalDefault(),
    ];

    return {
      plugins,
      autoSchemaFile: join(process.cwd(), 'src/generated-schema.gql'),
      playground: false,
      introspection: true,
      sortSchema: true,
      resolvers: { UUID: UUIDScalar },
      formatError(formattedError, error) {
        if (
          formattedError.extensions.code ===
          ApolloServerErrorCode.INTERNAL_SERVER_ERROR
        ) {
          Logger.error(error);
          Sentry.captureException(error, (scope) =>
            scope.setTag('kind', 'Field Resolver'),
          );
        }

        return formattedError;
      },
    };
  }
}
