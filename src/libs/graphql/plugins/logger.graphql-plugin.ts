import {
  ApolloServerPlugin,
  GraphQLRequestContext,
  GraphQLRequestListener,
} from '@apollo/server';
import { Plugin } from '@nestjs/apollo';
import { Logger } from '@nestjs/common';

@Plugin()
export class LoggingPlugin implements ApolloServerPlugin {
  async requestDidStart(
    requestContext: GraphQLRequestContext<unknown>,
  ): Promise<GraphQLRequestListener<unknown>> {
    Logger.log(
      {
        message: 'Incoming GraphQL request',
        operationName: requestContext.request.operationName,
        variables: requestContext.request.variables,
        organizationId:
          requestContext.request.http.headers.get('X-Organization-Id'),
        subOrganizationId: requestContext.request.http.headers.get(
          'X-Suborganization-Id',
        ),
      },
      'GraphQL',
    );

    return;
  }
}
