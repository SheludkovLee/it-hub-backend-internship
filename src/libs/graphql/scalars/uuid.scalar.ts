import { ASTNode, GraphQLError, GraphQLScalarType, Kind } from 'graphql';
import { validate as validateUuid } from 'uuid';

const validate = (value: unknown, ast?: ASTNode) => {
  if (typeof value !== 'string') {
    throw new GraphQLError(
      `Value is not string: ${value}`,
      ast ? { nodes: ast } : undefined,
    );
  }

  if (!validateUuid(value as string)) {
    throw new GraphQLError(
      `Value is not a valid UUID: ${value}`,
      ast ? { nodes: ast } : undefined,
    );
  }

  return value;
};

export const UUIDScalar = new GraphQLScalarType({
  name: 'UUID',
  description: `A field whose value is a generic Universally Unique Identifier`,

  serialize(value) {
    return validate(value);
  },

  parseValue(value) {
    return validate(value);
  },

  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(
        `Can only validate strings as UUIDs but got a: ${ast.kind}`,
        { nodes: ast },
      );
    }

    return validate(ast.value, ast);
  },
  extensions: {
    codegenScalarType: 'string',
    jsonSchema: {
      type: 'string',
      format: 'uuid',
    },
  },
});
