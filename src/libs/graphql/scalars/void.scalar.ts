import { CustomScalar, Scalar } from '@nestjs/graphql';

@Scalar('Void', () => null)
export class VoidScalar implements CustomScalar<void, void> {
  description = 'Represents NULL values';

  parseValue(): null {
    return null;
  }

  serialize(): string {
    return '';
  }

  parseLiteral(): null {
    return null;
  }
}
