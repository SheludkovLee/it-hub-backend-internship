import { ASTNode, GraphQLError, GraphQLScalarType, Kind } from 'graphql';
import { ValidationGuard } from 'src/libs/utils';

const validate = (value: unknown, ast?: ASTNode) => {
  if (typeof value !== 'string') {
    throw new GraphQLError(
      `Value is not string: ${value}`,
      ast ? { nodes: ast } : undefined,
    );
  }

  if (!ValidationGuard.isTimeString(value)) {
    throw new GraphQLError(
      `Value is not a valid time: ${value}`,
      ast ? { nodes: ast } : undefined,
    );
  }

  const [hours, minutes] = value.split(':');

  return `${hours}:${minutes}`;
};

export const TimeScalar = new GraphQLScalarType({
  name: 'Time',
  description: 'A field whose value is a time without time zone',

  serialize(value) {
    return validate(value);
  },

  parseValue(value) {
    return validate(value);
  },

  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(
        `Can only validate strings as times but got a: ${ast.kind}`,
        { nodes: ast },
      );
    }

    return validate(ast.value, ast);
  },
  extensions: {
    codegenScalarType: 'string',
    jsonSchema: {
      type: 'string',
      format: 'time',
    },
  },
});
