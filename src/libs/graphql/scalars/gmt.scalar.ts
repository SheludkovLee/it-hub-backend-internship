import { ASTNode, GraphQLError, GraphQLScalarType, Kind } from 'graphql';

export const GMTTimeZonesList = [
  'GMT+0',
  'GMT+1',
  'GMT+2',
  'GMT+3',
  'GMT+4',
  'GMT+5',
  'GMT+6',
  'GMT+7',
  'GMT+8',
  'GMT+9',
  'GMT+10',
  'GMT+11',
  'GMT+12',
  'GMT+13',
  'GMT-1',
  'GMT-2',
  'GMT-3',
  'GMT-4',
  'GMT-5',
  'GMT-6',
  'GMT-7',
  'GMT-8',
  'GMT-9',
  'GMT-10',
  'GMT-11',
  'GMT-12',
];

export const MSC_TIMEZONE = 'GMT+3';

const validate = (value: unknown, ast?: ASTNode) => {
  if (typeof value !== 'string') {
    throw new GraphQLError(
      `Value is not string: ${value}`,
      ast ? { nodes: ast } : undefined,
    );
  }

  if (!GMTTimeZonesList.includes(value)) {
    throw new GraphQLError(
      `Value is not a valid GMT time zone: ${value}`,
      ast ? { nodes: ast } : undefined,
    );
  }

  return value;
};

export const GMTScalar = new GraphQLScalarType({
  name: 'GMT',
  description: 'A field whose value is a GMT time zone',

  serialize(value) {
    return validate(value);
  },

  parseValue(value) {
    return validate(value);
  },

  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(
        `Can only validate strings as times but got a: ${ast.kind}`,
        { nodes: ast },
      );
    }

    return validate(ast.value, ast);
  },
  extensions: {
    codegenScalarType: 'string',
    jsonSchema: {
      type: 'string',
    },
  },
});
