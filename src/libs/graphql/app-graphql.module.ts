import { ApolloDriver } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

import { GraphqlConfigService } from './config/graphql-config.service';
import { LoggingPlugin } from './plugins/logger.graphql-plugin';
import { VoidScalar } from './scalars';

@Module({
  imports: [
    GraphQLModule.forRootAsync({
      driver: ApolloDriver,
      useClass: GraphqlConfigService,
    }),
  ],
  providers: [VoidScalar, LoggingPlugin],
})
export class AppGraphqlModule {}
