import * as bullmq from 'bullmq';

export type JobToAdd<TData = any> = {
  name: bullmq.Job['name'];
  data: TData;
  opts?: bullmq.Job['opts'];
};

export type ConnectionOptions = bullmq.ConnectionOptions;

const defaultJobOptions: JobToAdd['opts'] = {
  removeOnComplete: true,
  removeOnFail: 100,
};

export class Queue<TData> {
  private readonly queue: bullmq.Queue;
  private readonly worker: bullmq.Worker;

  constructor(
    name: string,
    jobsProcessor: bullmq.Processor<TData>,
    connection?: ConnectionOptions,
  ) {
    this.queue = new bullmq.Queue(name, { connection });
    this.worker = new bullmq.Worker(name, jobsProcessor, {
      connection,
      autorun: false,
    });
  }

  run() {
    return this.worker.run();
  }

  addJob(job: JobToAdd<TData>) {
    const { name, data, opts = {} } = job;

    return this.queue.add(name, data, { ...defaultJobOptions, ...opts });
  }

  addJobs(jobs: JobToAdd<TData>[]) {
    const jobsWithOpts = jobs.map((job) => {
      const opts = job.opts || {};
      return { ...job, opts: { ...defaultJobOptions, ...opts } };
    });
    return this.queue.addBulk(jobsWithOpts);
  }

  getActiveOrWaitingOrDelayedJobs() {
    return this.queue.getJobs(['waiting', 'delayed', 'active']);
  }
}
