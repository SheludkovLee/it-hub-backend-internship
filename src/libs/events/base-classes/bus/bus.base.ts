import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { IBus, IBusHandler, IEvent } from '../../interfaces';

export abstract class Bus<
  Event extends IEvent,
  Handler extends IBusHandler<Event>,
> implements IBus<Event, Handler>
{
  protected subscribers = new Map<string, Handler[]>();

  public register(token: string, handler: Handler): void;
  public register(token: string[], handler: Handler): void;
  public register(token: string | string[], handler: Handler): void {
    if (Array.isArray(token)) {
      token.forEach((t) => this.register(t, handler));
    } else {
      const subscribers = this.subscribers.get(token);

      if (!subscribers) {
        this.subscribers.set(token, [handler]);
      } else {
        subscribers.push(handler);
      }
    }
  }

  public async emit(event: Event): Promise<Result<any, ExceptionBase>> {
    const subscribers = this.subscribers.get(event.token) || [];
    if (!subscribers || subscribers.length === 0) {
      return Result.ok();
    }

    const results = await Promise.all(
      subscribers.map((subscriber) => subscriber.handle(event)),
    );

    if (results.length === 0) {
      return Result.ok();
    }

    const erroredResult = results.find((result) => result.isErr);

    if (erroredResult) {
      return erroredResult;
    }
    return Result.ok();
  }

  public async emitBulk(events: Event[]): Promise<Result<void, ExceptionBase>> {
    const results = await Promise.all(events.map((event) => this.emit(event)));

    const erroredResult = results.find((result) => result.isErr);

    if (erroredResult) {
      return erroredResult;
    }

    return Result.ok();
  }
}
