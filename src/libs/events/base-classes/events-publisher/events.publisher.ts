import { UUID } from 'src/libs/domain';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { IBusHandler, IEvent, IEventsPublisher } from '../../interfaces';
import { Bus } from '../bus';

export abstract class EventsPublisher<
  Event extends IEvent,
  Handler extends IBusHandler<Event>,
> implements IEventsPublisher<Event, Handler>
{
  constructor(private readonly bus: Bus<Event, Handler>) {}

  async emit(
    event: Event,
    trxId: string,
  ): Promise<Result<unknown, ExceptionBase>> {
    event.trxId = trxId;

    return this.bus.emit(event);
  }

  emitBulk(
    events: Event[],
    trxId: string,
  ): Promise<Result<unknown, ExceptionBase>> {
    return this.bus.emitBulk(
      events.map((event) => {
        event.trxId = trxId || UUID.generate().value;
        return event;
      }),
    );
  }
  register(eventTokens: string, handler: Handler): void;
  register(eventTokens: string[], handler: Handler): void;
  register(eventTokens: string[] | string, handler: Handler): void {
    if (Array.isArray(eventTokens)) {
      eventTokens.map((eventToken) => this.bus.register(eventToken, handler));
    } else {
      this.bus.register(eventTokens, handler);
    }
  }
}
