import { DateVO, UUID } from 'src/libs/domain';

import { IEvent } from '../../interfaces';

export interface EventProps<Payload extends Record<string, any>> {
  trxId?: string;
  initiatorUserId?: string;
  payload: Payload;
}

export abstract class EventBase<
  Payload extends Record<string, any> = Record<string, any>,
> implements IEvent<Payload>
{
  public readonly id: string;
  public token: string;
  public trxId?: string;
  public initiatorUserId?: string;
  public payload: Payload;
  public dateOccurred: string;

  constructor(props: EventProps<Payload>) {
    this.id = UUID.generate().value;
    this.trxId = props.trxId;
    this.initiatorUserId = props.initiatorUserId;
    this.payload = props.payload;
    this.dateOccurred = DateVO.now().toISOString();
  }
}
