import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { IBusHandler } from './bus-handler.interface';
import { IEvent } from './event.interface';

export interface IEventsPublisher<
  Event extends IEvent,
  Handler extends IBusHandler<Event>,
> {
  emit(event: Event, trxId: string): Promise<Result<unknown, ExceptionBase>>;
  emitBulk(
    events: Event[],
    trxId: string,
  ): Promise<Result<unknown, ExceptionBase>>;
  register(eventTokens: string[], handler: Handler): void;
}
