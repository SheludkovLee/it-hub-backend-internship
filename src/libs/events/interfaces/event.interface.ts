export interface IEvent<
  Payload extends Record<string, any> = Record<string, any>,
> {
  id: string;
  token: string;
  trxId?: string;
  payload: Payload;
  dateOccurred: string;
}
