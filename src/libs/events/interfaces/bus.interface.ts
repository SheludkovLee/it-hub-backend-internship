import { IBusHandler, IEvent } from 'src/libs/events';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

export interface IBus<
  Event extends IEvent,
  Handler extends IBusHandler<Event>,
> {
  register(token: string | string[], handler: Handler): void;
  emit(event: Event): Promise<Result<any, ExceptionBase>>;
  emitBulk(events: Event[]): Promise<Result<void, ExceptionBase>>;
}
