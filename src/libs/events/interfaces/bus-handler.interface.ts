import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { IEvent } from './event.interface';

export interface IBusHandler<Event extends IEvent> {
  handle(event: Event): Promise<Result<unknown, ExceptionBase>>;
}
