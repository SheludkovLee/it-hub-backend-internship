import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';

@Injectable()
export class ZammadHttpService {
  private readonly apiKey: string;
  private readonly logger = new Logger(`Zammad HTTP Service`);

  constructor(private readonly configService: ConfigService) {
    this.apiKey = this.configService.getOrThrow<string>('zammad.apiKey');
  }

  async createTicket(data: JSON): Promise<boolean> {
    try {
      const result = await axios.post(
        `https://zammad.infra.ithub.ru/api/v1/tickets`,
        data,
        {
          headers: {
            Authorization: `Bearer ${this.apiKey}`,
          },
        },
      );

      return true;
    } catch (error) {
      this.logger.error(error);

      return false;
    }
  }
}
