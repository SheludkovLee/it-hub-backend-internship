import { HeadObjectCommandOutput } from '@aws-sdk/client-s3';

import { IFileInfo } from '../types/file-info.type';

export class S3FileInfoDto implements IFileInfo {
  readonly size: number;
  readonly contentType: string;
  readonly lastModified: Date;

  constructor(data: HeadObjectCommandOutput) {
    const bytes = data.ContentLength;
    const megabytes = Number((bytes / 1000 / 1000).toFixed(3));

    this.size = megabytes;
    this.contentType = data.ContentType;
    this.lastModified = data.LastModified;
  }
}
