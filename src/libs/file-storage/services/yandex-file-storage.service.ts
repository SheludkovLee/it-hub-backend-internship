import {
  DeleteObjectCommand,
  HeadObjectCommand,
  NotFound,
  PutObjectCommand,
  S3Client,
} from '@aws-sdk/client-s3';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { UUID } from '../../domain';
import { S3FileInfoDto } from '../dtos/s3-file-info.dto';
import { FileStorageException } from '../file-storage.exception';
import { IFileInfo } from '../types/file-info.type';
import { IFileStorageService } from '../types/file-storage-service.type';

@Injectable()
export class YandexFileStorageService implements IFileStorageService {
  private readonly logger = new Logger(this.constructor.name);
  private readonly bucketName: string;
  private readonly temporaryBucketName: string;
  private readonly endpoint: string;

  constructor(
    private readonly client: S3Client,
    readonly configService: ConfigService,
  ) {
    this.bucketName = configService.get('S3.bucketName');
    this.temporaryBucketName = configService.get('S3.temporaryBucketName');
    this.endpoint = configService.get('S3.endpoint');
  }

  constructFileUrl(fileKey: string, isTemporaryBucket?: boolean): string {
    const bucketName =
      isTemporaryBucket && this.temporaryBucketName
        ? this.temporaryBucketName
        : this.bucketName;
    return this.endpoint + bucketName + `/${fileKey}`;
  }

  async createUniqueSignedUrl(
    userId: string,
    fileExtension: string,
    fileName?: string,
  ): Promise<string> {
    const uniqId = UUID.generate().value;

    const fileKey = `user_${userId}/${fileName}_${uniqId}.${fileExtension}`;

    const command = new PutObjectCommand({
      Bucket: this.bucketName,
      Key: fileKey,
    });

    const signedUrl = await getSignedUrl(this.client, command);

    return signedUrl;
  }

  async checkFileExists(fileKey: string): Promise<boolean> {
    return Boolean(await this.getFileInfo(fileKey));
  }

  async getFileInfo(fileKey: string): Promise<IFileInfo> {
    try {
      const command = new HeadObjectCommand({
        Bucket: this.bucketName,
        Key: fileKey,
      });

      const info = await this.client.send(command);

      return new S3FileInfoDto(info);
    } catch (e: unknown) {
      if (e instanceof NotFound) {
        return null;
      }

      this.logger.error(JSON.stringify(e));
    }
  }

  async uploadFile(
    fileKey: string,
    buffer: any,
    isTemporary?: boolean,
  ): Promise<void> {
    try {
      const bucketName =
        isTemporary && this.temporaryBucketName
          ? this.temporaryBucketName
          : this.bucketName;

      const command = new PutObjectCommand({
        Bucket: bucketName,
        Key: fileKey,
        Body: buffer,
      });

      const res = await this.client.send(command);
    } catch (e: unknown) {
      this.logger.error(JSON.stringify(e));

      throw new FileStorageException();
    }
  }

  async deleteFile(fileKey: string): Promise<void> {
    try {
      const command = new DeleteObjectCommand({
        Bucket: this.bucketName,
        Key: fileKey,
      });

      await this.client.send(command);
    } catch (e: unknown) {
      this.logger.error(JSON.stringify(e));

      throw new FileStorageException();
    }
  }
}
