export interface IFileInfo {
  /** In megabytes */
  readonly size: number;
  readonly contentType: string;
  readonly lastModified: Date;
}
