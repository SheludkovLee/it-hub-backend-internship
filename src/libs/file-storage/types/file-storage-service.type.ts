import { IFileInfo } from './file-info.type';

export abstract class IFileStorageService {
  abstract constructFileUrl(
    fileKey: string,
    isTemporaryBucket?: boolean,
  ): string;
  abstract createUniqueSignedUrl(
    userId: string,
    fileExtension: string,
    fileName?: string,
  ): Promise<string>;
  abstract checkFileExists(fileKey: string): Promise<boolean>;
  abstract getFileInfo(fileKey: string): Promise<IFileInfo>;
  abstract deleteFile(fileKey: string): Promise<void>;
  abstract uploadFile(
    fileKey: string,
    buffer: any,
    isTemporary?: boolean,
  ): Promise<void>;
}
