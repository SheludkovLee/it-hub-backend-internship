import { S3Client } from '@aws-sdk/client-s3';
import { Provider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

export const S3ClientProvider: Provider<S3Client> = {
  provide: S3Client,
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    return new S3Client({
      region: configService.get('S3.region'),
      credentials: {
        accessKeyId: configService.get('S3.accessKey'),
        secretAccessKey: configService.get('S3.secretAccessKey'),
      },
      endpoint: configService.get('S3.endpoint'),
    });
  },
};
