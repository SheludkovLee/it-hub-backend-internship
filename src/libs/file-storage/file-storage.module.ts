import { Global, Module } from '@nestjs/common';

import { S3ClientProvider } from './providers/s3-client.provider';
import { YandexFileStorageService } from './services/yandex-file-storage.service';
import { IFileStorageService } from './types/file-storage-service.type';

@Global()
@Module({
  providers: [
    { provide: IFileStorageService, useClass: YandexFileStorageService },
    S3ClientProvider,
  ],
  exports: [IFileStorageService],
})
export class FileStorageModule {}
