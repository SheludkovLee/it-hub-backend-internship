export class FileStorageException extends Error {
  constructor() {
    super('Something went wrong with file storage');
  }
}
