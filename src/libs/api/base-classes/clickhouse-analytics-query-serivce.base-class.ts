import { ClickHouseClient } from '@clickhouse/client';
import { BaseQueryService } from 'src/libs/api/base-classes/query-service.base-class';

export abstract class ClickhouseAnalyticsQueryService extends BaseQueryService {
  constructor(protected clickhouseClient: ClickHouseClient) {
    super();
  }
}

export type ClickhouseQueryBaseResponse<Data> = {
  data: Data;
};
