// eslint-disable-next-line simple-import-sort/imports
import {UserObjectionOrmEntity} from 'src/domains/identity/database';
import {TemplateDisciplineObjectionOrmEntity} from 'src/domains/learning-design/database';

import {IQueryService} from '../interfaces/query-service.interface';

export const objectionQueryModels = {
    identity: {
        user: UserObjectionOrmEntity,
    },
    learningDesign: {
        templateDiscipline: TemplateDisciplineObjectionOrmEntity,
    },
    learning: {},
};

export abstract class BaseQueryService<Model = unknown>
    implements IQueryService<Model> {
    protected queryModels = objectionQueryModels;
}
