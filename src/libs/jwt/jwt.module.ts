import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule as NestJwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { JwtService } from './jwt.service';
import { JwtStrategy } from './jwt.strategy';
import { JwtConfigService } from './jwt-config.service';

@Module({
  imports: [
    ConfigModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    NestJwtModule.registerAsync({
      useClass: JwtConfigService,
    }),
  ],
  providers: [JwtStrategy, JwtService],
  exports: [PassportModule, JwtStrategy, JwtService],
})
export class AppJwtModule {}
