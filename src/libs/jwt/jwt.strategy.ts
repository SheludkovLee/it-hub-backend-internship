import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { PatchedModelObject } from 'objection';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserObjectionOrmEntity } from 'src/domains/identity/database';

import { JwtPayload } from './jwt.types';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(readonly configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get<string>('jwt.secret'),
    });
  }

  async validate(
    payload: JwtPayload,
  ): Promise<PatchedModelObject<UserObjectionOrmEntity>> {
    return UserObjectionOrmEntity.query().findById(payload.sub);
  }
}
