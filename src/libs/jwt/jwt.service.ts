import { Injectable } from '@nestjs/common';
import { JwtService as NestJwtService } from '@nestjs/jwt';

import { JwtPayload, JwtToken } from './jwt.types';

@Injectable()
export class JwtService {
  constructor(private readonly jwtService: NestJwtService) {}

  async createUserJwtToken(id: string): Promise<JwtToken> {
    const payload = {
      sub: id,
    } as JwtPayload;

    return this.jwtService.sign(payload);
  }

  async verifyTokenAsync(token: string): Promise<JwtPayload> {
    return this.jwtService.verifyAsync(token);
  }
}
