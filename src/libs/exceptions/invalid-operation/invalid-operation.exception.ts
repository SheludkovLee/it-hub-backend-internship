import { ExceptionCodes } from 'src/libs/exceptions';

import { ExceptionBase } from '../exception.base';

export class InvalidOperationException extends ExceptionBase {
  code = ExceptionCodes.INVALID;
}
