import { ExceptionBase } from '../exception.base';
import { ExceptionCodes } from '../exception.codes';

export class ForbiddenException extends ExceptionBase {
  readonly code = ExceptionCodes.FORBIDDEN;
}
