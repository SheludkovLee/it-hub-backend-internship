export interface SerializedException {
  message: string;
  code: string;
  stack?: string;
}

export abstract class ExceptionBase extends Error {
  abstract code: string;

  constructor(message: string) {
    super(message);
  }

  public toJSON(): SerializedException {
    return {
      message: this.message,
      code: this.code,
      stack: this.stack,
    };
  }
}
