// TODO: подумать над расположением интерфейсов
// import { IMessage } from 'src/infra/messages/message.interface';
import { ExceptionBase } from 'src/libs/exceptions';
import { IMessage } from 'src/libs/messages/message.interface';
import { Result } from 'src/libs/utils';

export interface MessageInboxRepositoryPort {
  insertAsProcessed(message: IMessage): Promise<Result<void, ExceptionBase>>;

  insertAsFailed(
    message: IMessage,
    error: any,
  ): Promise<Result<void, ExceptionBase>>;

  getTableName(): string;
}
