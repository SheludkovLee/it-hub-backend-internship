import { Message } from 'src/libs/messages/message.base';

import { MessageInboxStatus } from './types';

export interface MessageInboxOrmEntityProps {
  id: string;
  token: string;
  sourceContext: string;
  payload: Record<string, any>;
  trxId: string;
  dateOccurred: string;
  status: MessageInboxStatus;
  error?: string;
}

export class MessageInboxOrmEntity {
  id: string;
  token: string;
  sourceContext: string;
  payload: Record<string, any>;
  trxId: string;
  dateOccurred: string;
  status: MessageInboxStatus;
  error?: string;

  constructor(props: MessageInboxOrmEntityProps) {
    this.id = props.id;
    this.token = props.token;
    this.sourceContext = props.sourceContext;
    this.payload = props.payload;
    this.trxId = props.trxId;
    this.dateOccurred = props.dateOccurred;
    this.status = props.status;
    this.error = props.error;
  }

  static create(props: MessageInboxOrmEntityProps): MessageInboxOrmEntity {
    return new MessageInboxOrmEntity(props);
  }

  static fromMessage(
    message: Message,
    status: MessageInboxStatus,
    error?: any,
  ): MessageInboxOrmEntity {
    return new MessageInboxOrmEntity({
      ...message,
      sourceContext: message.context,
      status,
      error: JSON.stringify(error),
    });
  }
}
