import { Knex } from 'knex';
import { Model } from 'objection';
import { ExceptionBase } from 'src/libs/exceptions';
import { Message } from 'src/libs/messages/message.base';
import { Result } from 'src/libs/utils';

import { UnitOfWorkObjection } from '../unit-of-work';
import { MessageInboxOrmEntity } from './message-inbox.orm-entity';
import { MessageInboxRepositoryBase } from './message-inbox.repository.base';
import { MessageInboxStatus } from './types';

export abstract class MessageInboxObjectionRepository extends MessageInboxRepositoryBase<UnitOfWorkObjection> {
  async insertAsProcessed(
    message: Message,
  ): Promise<Result<void, ExceptionBase>> {
    return this.insert(message, MessageInboxStatus.PROCESSED);
  }

  async insertAsFailed(
    message: Message,
    error: string,
  ): Promise<Result<void, ExceptionBase>> {
    return this.insert(message, MessageInboxStatus.FAILED, error);
  }

  private async insert(
    message: Message,
    status: MessageInboxStatus,
    error?: any,
  ): Promise<Result<void, ExceptionBase>> {
    try {
      const existedMessage = await this.getQb().where('id', message.id);

      if (existedMessage) {
        await this.getQb()
          .update(MessageInboxOrmEntity.fromMessage(message, status, error))
          .where('id', message.id);
      } else {
        await this.getQb().insert(
          MessageInboxOrmEntity.fromMessage(message, status, error),
        );
      }

      return Result.ok();
    } catch (e) {
      return Result.fail(e);
    }
  }

  private getQb(): Knex.QueryBuilder {
    const knex = Model.knex();
    const qb = knex(this.tableName);

    if (this.trxId) {
      const transaction = this.unitOfWork.getTrx(this.trxId);

      qb.transacting(transaction);
    }

    return qb;
  }
}
