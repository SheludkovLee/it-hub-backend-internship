import { ExceptionBase } from 'src/libs/exceptions';
import { Message } from 'src/libs/messages/message.base';
import { Result } from 'src/libs/utils';

import { UnitOfWorkPort } from '../unit-of-work';
import { MessageInboxRepositoryPort } from './message-inbox.repository.port';

export abstract class MessageInboxRepositoryBase<
  UnitOfWork extends UnitOfWorkPort = UnitOfWorkPort,
> implements MessageInboxRepositoryPort
{
  protected abstract tableName: string;

  constructor(
    protected readonly unitOfWork: UnitOfWork,
    protected readonly trxId?: string,
  ) {}

  abstract insertAsProcessed(
    message: Message,
  ): Promise<Result<void, ExceptionBase>>;

  abstract insertAsFailed(
    message: Message,
    error: any,
  ): Promise<Result<void, ExceptionBase>>;

  public getTableName(): string {
    return this.tableName;
  }
}
