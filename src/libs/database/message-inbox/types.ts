export enum MessageInboxStatus {
  PROCESSED = 'PROCESSED',
  FAILED = 'FAILED',
}
