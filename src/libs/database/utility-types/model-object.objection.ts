import { DataPropertyNames } from 'objection';

export type ModelObject<T> = {
  [K in DataPropertyNames<T>]?: T[K] extends object ? ModelObject<T[K]> : T[K];
};
