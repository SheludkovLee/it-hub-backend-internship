import { AggregateRoot, ID } from 'src/libs/domain';
import { ExceptionBase, NotFoundException } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { ObjectionOrmEntityBase } from '../entity';
import { ObjectionRepositoryBase } from './objection.repository.base';
import { InsertParams } from './repository.interface';

export abstract class ObjectionAggregateRepositoryBase<
  Entity extends AggregateRoot<any>,
  OrmEntity extends ObjectionOrmEntityBase,
> extends ObjectionRepositoryBase<Entity, OrmEntity> {
  async save(entity: Entity): Promise<Result<Entity, ExceptionBase>> {
    try {
      if (this.trxId) {
        this.unitOfWork.addAggregate(this.trxId, entity);
      }

      const ormEntity = this.mapper.toOrm(entity);

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      await this.getQb().upsertGraph(ormEntity, { insertMissing: true });

      return Result.ok(entity);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async saveMany(entities: Entity[]): Promise<Result<Entity[], ExceptionBase>> {
    try {
      if (this.trxId) {
        entities.forEach((entity) => {
          this.unitOfWork.addAggregate(this.trxId, entity);
        });
      }

      const ormEntities = entities.map((orm) => this.mapper.toOrm(orm));

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      await this.getQb().upsertGraph(ormEntities, { insertMissing: true });

      return Result.ok(entities);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async insert(
    entity: Entity,
    params?: InsertParams,
  ): Promise<Result<Entity, ExceptionBase>> {
    if (this.trxId) {
      this.unitOfWork.addAggregate(this.trxId, entity);
    }

    return super.insert(entity, params);
  }

  async insertMany(
    entities: Entity[],
  ): Promise<Result<Entity[], ExceptionBase>> {
    if (this.trxId) {
      entities.forEach((entity) => {
        this.unitOfWork.addAggregate(this.trxId, entity);
      });
    }

    return super.insertMany(entities);
  }

  async loadByIds(ids: ID[]): Promise<Result<Entity[], ExceptionBase>> {
    try {
      const qb = this.getQb().findByIds(ids.map((id) => id.value));

      if (this.relations) {
        qb.withGraphFetched(this.relations);
      }

      const results = await qb;

      const entities = results.map((entity) => this.mapper.toDomain(entity));

      if (this.trxId) {
        entities.forEach((entity) =>
          this.unitOfWork.addAggregate(this.trxId, entity),
        );
      }

      return Result.ok(entities);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async loadById(id: ID): Promise<Result<Entity, ExceptionBase>> {
    try {
      const qb = this.getQb().findById(id.value);

      if (this.relations) {
        qb.withGraphFetched(this.relations);
      }

      const result = await qb;

      if (!result) {
        return Result.fail(new NotFoundException(this.notFoundMessage));
      }

      const entity = this.mapper.toDomain(result);

      if (this.trxId) {
        this.unitOfWork.addAggregate(this.trxId, entity);
      }

      return Result.ok(entity);
    } catch (e) {
      return Result.fail(e);
    }
  }

  protected addAggregate(entities: Entity | Entity[]): void {
    if (this.trxId) {
      if (Array.isArray(entities)) {
        return entities.forEach((entity) => this.addAggregate(entity));
      }

      return this.unitOfWork.addAggregate(this.trxId, entities);
    }
  }
}
