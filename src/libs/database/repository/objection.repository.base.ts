import { uniq } from 'lodash';
import { ModelClass, QueryBuilder } from 'objection';
import { DateVO, Entity, ID } from 'src/libs/domain';
import { ExceptionBase, NotFoundException } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { ObjectionOrmEntityBase } from '../entity';
import { Mapper } from '../interfaces';
import { UnitOfWorkObjection } from '../unit-of-work';
import { OrmEntityRelations, RepositoryBase } from './repository.base';
import { InsertParams } from './repository.interface';

export abstract class ObjectionRepositoryBase<
  DomainEntity extends Entity<any>,
  OrmEntity extends ObjectionOrmEntityBase,
> extends RepositoryBase<DomainEntity, OrmEntity, UnitOfWorkObjection> {
  protected abstract mapper: Mapper<DomainEntity, OrmEntity>;
  protected abstract model: ModelClass<OrmEntity>;
  protected abstract get notFoundMessage(): string;

  protected get useSoftDelete(): boolean {
    return false;
  }

  protected get relations(): OrmEntityRelations<OrmEntity> | undefined {
    return;
  }

  protected getQb(): QueryBuilder<OrmEntity> {
    const qb = this.model.query();

    if (this.trxId) {
      const transaction = this.unitOfWork.getTrx(this.trxId);

      qb.transacting(transaction);
    }

    return qb as QueryBuilder<OrmEntity>;
  }

  protected getRelatedQb(relation: keyof OrmEntity) {
    const qb = this.model.relatedQuery(relation);

    if (this.trxId) {
      const transaction = this.unitOfWork.getTrx(this.trxId);

      qb.transacting(transaction);
    }

    return qb;
  }

  async save(
    entity: DomainEntity,
  ): Promise<Result<DomainEntity, ExceptionBase>> {
    try {
      const ormEntity = this.mapper.toOrm(entity);

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      await this.getQb().upsertGraph(ormEntity, { insertMissing: true });

      return Result.ok(entity);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async saveMany(
    entities: DomainEntity[],
  ): Promise<Result<DomainEntity[], ExceptionBase>> {
    try {
      const ormEntities = entities.map(this.mapper.toOrm);

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      await this.getQb().upsertGraph(ormEntities, { insertMissing: true });

      return Result.ok(entities);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async insert(
    entity: DomainEntity,
    params?: InsertParams,
  ): Promise<Result<DomainEntity, ExceptionBase>> {
    try {
      const ormEntity = this.mapper.toOrm(entity);

      if (params) {
        if (params.onConflict && params.onConflict.ignore) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          await this.getQb()
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            .insert(ormEntity)
            .onConflict(params.onConflict.columns)
            .ignore();
        }
      } else {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        await this.getQb().insert(ormEntity);
      }

      return Result.ok(entity);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async insertMany(
    entities: DomainEntity[],
  ): Promise<Result<DomainEntity[], ExceptionBase>> {
    try {
      if (entities.length === 0) {
        return Result.ok(entities);
      }

      const ormEntities = entities.map((entity) => this.mapper.toOrm(entity));

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      await this.getQb().insert(ormEntities);

      return Result.ok(entities);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async update(
    entity: DomainEntity,
  ): Promise<Result<DomainEntity, ExceptionBase>> {
    try {
      const ormEntity = this.mapper.toOrm(entity);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      await this.getQb().update(ormEntity).where('id', ormEntity.id);

      return Result.ok(entity);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async delete(
    entity: DomainEntity,
  ): Promise<Result<DomainEntity, ExceptionBase>> {
    try {
      if (this.useSoftDelete) {
        await this.getQb()
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          .update({ deletedAt: DateVO.now().toISOString() })
          .where('id', '=', entity.id.value);
      } else {
        await this.getQb().delete().where('id', '=', entity.id.value);
      }
      return Result.ok(entity);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async deleteMany(
    entities: DomainEntity[],
  ): Promise<Result<DomainEntity[], ExceptionBase>> {
    try {
      if (entities.length === 0) {
        return Result.ok(entities);
      }

      if (this.useSoftDelete) {
        await this.getQb()
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          .update({ deletedAt: DateVO.now().toISOString() })
          .whereIn(
            'id',
            entities.map((entity) => entity.id.value),
          );
      } else {
        await this.getQb()
          .delete()
          .whereIn(
            'id',
            entities.map((entity) => entity.id.value),
          );
      }

      return Result.ok(entities);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async loadByIds(ids: ID[]): Promise<Result<DomainEntity[], ExceptionBase>> {
    try {
      const qb = this.getQb().findByIds(uniq(ids.map((id) => id.value)));

      if (this.relations) {
        qb.withGraphFetched(this.relations);
      }

      const results = await qb;

      const entities = results.map((entity) => this.mapper.toDomain(entity));

      return Result.ok(entities);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async loadById(id: ID): Promise<Result<DomainEntity, ExceptionBase>> {
    try {
      const qb = this.getQb().findById(id.value);

      if (this.relations) {
        qb.withGraphFetched(this.relations);
      }

      const result = await qb;

      if (!result) {
        return Result.fail(new NotFoundException(this.notFoundMessage));
      }

      const entity = this.mapper.toDomain(result);

      return Result.ok(entity);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async doesExistBy(params: Partial<OrmEntity>): Promise<boolean> {
    const entity = await this.getQb().findOne(params).select('id');

    return Boolean(entity);
  }

  async loadOneByParams(
    params: Partial<OrmEntity>,
  ): Promise<Result<DomainEntity, ExceptionBase>> {
    try {
      const qb = this.getQb().findOne(params);

      if (this.relations) {
        qb.withGraphFetched(this.relations);
      }

      const result = await qb;

      if (!result) {
        return Result.fail(new NotFoundException(this.notFoundMessage));
      }

      const entity = this.mapper.toDomain(result);

      return Result.ok(entity);
    } catch (error) {
      return Result.fail(error);
    }
  }

  async loadManyByParams(
    params: Partial<OrmEntity>,
  ): Promise<Result<DomainEntity[], ExceptionBase>> {
    try {
      const qb = this.getQb().where(params);

      if (this.relations) {
        qb.withGraphFetched(this.relations);
      }

      const result = await qb;

      if (!result) {
        return Result.fail(new NotFoundException(this.notFoundMessage));
      }

      const entities = result.map((entity) => this.mapper.toDomain(entity));

      return Result.ok(entities);
    } catch (error) {
      return Result.fail(error);
    }
  }
}
