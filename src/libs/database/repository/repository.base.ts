import { Model } from 'objection';
import { Entity, ID } from 'src/libs/domain';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { ObjectionOrmEntityBase } from '../entity';
import { IOrmEntity, Mapper } from '../interfaces';
import { UnitOfWorkPort } from '../unit-of-work';
import { InsertParams, IRepository } from './repository.interface';

export type OrmEntityRelations<T extends ObjectionOrmEntityBase> =
  | string
  | Partial<
      Record<keyof Omit<T, keyof Model>, boolean | Record<string, unknown>>
    >;

export abstract class RepositoryBase<
  DomainEntity extends Entity<any> = Entity<any>,
  OrmEntity extends IOrmEntity = IOrmEntity,
  UnitOfWork extends UnitOfWorkPort = UnitOfWorkPort,
> implements IRepository<DomainEntity, OrmEntity>
{
  protected abstract mapper: Mapper<DomainEntity, OrmEntity>;

  constructor(
    protected readonly unitOfWork: UnitOfWork,
    protected readonly trxId?: string,
  ) {}

  abstract save(
    entity: DomainEntity,
  ): Promise<Result<DomainEntity, ExceptionBase>>;

  abstract saveMany(
    entities: DomainEntity[],
  ): Promise<Result<DomainEntity[], ExceptionBase>>;

  abstract delete(
    entity: DomainEntity,
  ): Promise<Result<DomainEntity, ExceptionBase>>;

  abstract deleteMany(
    entities: DomainEntity[],
  ): Promise<Result<DomainEntity[], ExceptionBase>>;

  abstract insert(
    entity: DomainEntity,
    params?: InsertParams,
  ): Promise<Result<DomainEntity, ExceptionBase>>;

  abstract insertMany(
    entities: DomainEntity[],
  ): Promise<Result<DomainEntity[], ExceptionBase>>;

  abstract loadById(id: ID): Promise<Result<DomainEntity, ExceptionBase>>;

  abstract loadByIds(ids: ID[]): Promise<Result<DomainEntity[], ExceptionBase>>;

  abstract update(
    entity: DomainEntity,
  ): Promise<Result<DomainEntity, ExceptionBase>>;

  abstract doesExistBy(params: Partial<OrmEntity>): Promise<boolean>;

  abstract loadOneByParams(
    params: Partial<OrmEntity>,
  ): Promise<Result<DomainEntity, ExceptionBase>>;

  abstract loadManyByParams(
    params: Partial<OrmEntity>,
  ): Promise<Result<DomainEntity[], ExceptionBase>>;
}
