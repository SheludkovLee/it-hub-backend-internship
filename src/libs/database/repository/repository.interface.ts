import { Entity, ID } from 'src/libs/domain';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { IOrmEntity } from '../interfaces';

export interface IRepository<
  DomainEntity extends Entity<any>,
  OrmEntity extends IOrmEntity,
> {
  save(entity: DomainEntity): Promise<Result<DomainEntity, ExceptionBase>>;
  saveMany(
    entities: DomainEntity[],
  ): Promise<Result<DomainEntity[], ExceptionBase>>;
  insert(
    entity: DomainEntity,
    params?: InsertParams,
  ): Promise<Result<DomainEntity, ExceptionBase>>;
  insertMany(
    entities: DomainEntity[],
  ): Promise<Result<DomainEntity[], ExceptionBase>>;
  update(entity: DomainEntity): Promise<Result<DomainEntity, ExceptionBase>>;
  loadById(id: ID): Promise<Result<DomainEntity, ExceptionBase>>;
  loadByIds(ids: ID[]): Promise<Result<DomainEntity[], ExceptionBase>>;
  delete(entity: DomainEntity): Promise<Result<DomainEntity, ExceptionBase>>;
  deleteMany(
    entities: DomainEntity[],
  ): Promise<Result<DomainEntity[], ExceptionBase>>;
  doesExistBy(params: Partial<OrmEntity>): Promise<boolean>;
  loadOneByParams(
    params: Partial<OrmEntity>,
  ): Promise<Result<DomainEntity, ExceptionBase>>;
  loadManyByParams(
    params: Partial<OrmEntity>,
  ): Promise<Result<DomainEntity[], ExceptionBase>>;
}

export interface InsertParams {
  onConflict?: { columns: string[]; ignore: boolean };
}
