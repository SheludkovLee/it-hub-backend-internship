import { types } from 'pg';

types.setTypeParser(types.builtins.NUMERIC, parseFloat);
