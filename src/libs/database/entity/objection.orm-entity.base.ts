import { Model } from 'objection';

import { IOrmEntity, IOrmIdEntity } from '../interfaces';

export class ObjectionOrmEntityBase
  extends Model
  implements IOrmIdEntity, IOrmEntity
{
  id: string;
  createdAt: string;
  updatedAt: string;
}
