import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

export abstract class ReadDaoBase<DaoModel, Params> {
  abstract query(params: Params): Promise<Result<DaoModel, ExceptionBase>>;
}
