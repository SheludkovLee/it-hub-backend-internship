import { IOrmEntity } from 'src/libs/database';
import { Entity } from 'src/libs/domain';

export interface Mapper<
  DomainEntity extends Entity<any> = Entity<any>,
  OrmEntity extends IOrmEntity = IOrmEntity,
> {
  toDomain(entity: OrmEntity, additional?: any): DomainEntity;
  toOrm(entity: DomainEntity): OrmEntity;
}
