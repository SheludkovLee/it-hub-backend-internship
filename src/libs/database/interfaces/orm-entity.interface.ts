export interface IOrmIdEntity {
  id: string;
}

export interface IOrmEntity extends IOrmIdEntity {
  createdAt: string;
  updatedAt: string;
}
