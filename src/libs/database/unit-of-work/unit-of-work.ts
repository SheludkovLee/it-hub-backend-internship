import { MessageInboxRepositoryPort } from 'src/libs/database/message-inbox';
import { AggregateRoot, ID } from 'src/libs/domain';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { MessageOutboxRepositoryPort } from '../message-outbox';
import { StartTransactionConfig, UnitOfWorkPort } from './unit-of-work.port';

export abstract class UnitOfWork implements UnitOfWorkPort {
  protected aggregates: Map<string, AggregateRoot<unknown>[]> = new Map();

  abstract start(config?: StartTransactionConfig): Promise<string>;

  abstract hasTrx(id: string): boolean;

  async execute<ResultType>(
    trxId: string,
    callback: () => Promise<Result<ResultType, ExceptionBase>>,
  ): Promise<Result<ResultType, ExceptionBase>> {
    try {
      await this.startFromId(trxId);

      const result = await callback();

      if (result.isErr) {
        await this.rollback(trxId);
      } else {
        await this.commit(trxId);
      }

      return result;
    } catch (e) {
      await this.rollback(trxId);
      throw e;
    }
  }

  abstract startFromId(
    id: string,
    config?: StartTransactionConfig,
  ): Promise<void>;

  abstract commit(id: string): Promise<void>;

  public addAggregate(trxId: string, aggregate: AggregateRoot<unknown>): void {
    const aggregates = this.aggregates.get(trxId);

    if (!aggregates) {
      this.aggregates.set(trxId, [aggregate]);
    } else {
      aggregates.push(aggregate);
    }
  }

  public getAggregates(trxId: string): AggregateRoot<unknown>[] {
    return this.aggregates.get(trxId) || [];
  }

  public getAggregate(
    trxId: string,
    aggregateId: ID,
  ): AggregateRoot<any> | void {
    const aggregates = this.aggregates.get(trxId) || [];

    return aggregates.find((agg) => agg.id.equals(aggregateId));
  }

  abstract rollback(id: string): Promise<void>;

  abstract getMessagesOutboxRepository(
    trxId?: string,
  ): MessageOutboxRepositoryPort;

  abstract getMessageInboxRepository(
    trxId?: string,
  ): MessageInboxRepositoryPort;
}
