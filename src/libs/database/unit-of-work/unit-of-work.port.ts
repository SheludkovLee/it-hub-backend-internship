import { MessageInboxRepositoryPort } from 'src/libs/database/message-inbox';
import { AggregateRoot, ID } from 'src/libs/domain';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { MessageOutboxRepositoryPort } from '../message-outbox';

export type IsolationLevels =
  | 'read uncommitted'
  | 'read committed'
  | 'snapshot'
  | 'repeatable read'
  | 'serializable';

export interface StartTransactionConfig {
  isolationLevel?: IsolationLevels;
}

export interface UnitOfWorkPort {
  start(config?: StartTransactionConfig): Promise<string>;

  execute<ResultType>(
    trxId: string,
    callback: () => Promise<Result<ResultType, ExceptionBase>>,
  ): Promise<Result<ResultType, ExceptionBase>>;

  startFromId(id: string, config?: StartTransactionConfig): Promise<void>;

  getAggregates(trxId: string): AggregateRoot<unknown>[];

  getAggregate(trxId: string, aggregateId: ID): AggregateRoot<any> | void;

  addAggregate(trxId: string, aggregate: AggregateRoot<unknown>): void;

  commit(id: string): Promise<void>;

  rollback(id: string): Promise<void>;

  getMessagesOutboxRepository(trxId?: string): MessageOutboxRepositoryPort;

  getMessageInboxRepository(trxId?: string): MessageInboxRepositoryPort;

  hasTrx(id: string): boolean;
}
