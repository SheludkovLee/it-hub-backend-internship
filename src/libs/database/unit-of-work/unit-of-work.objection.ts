import { Model, Transaction } from 'objection';
import { UUID } from 'src/libs/domain';

import { UnitOfWork } from './unit-of-work';
import { StartTransactionConfig } from './unit-of-work.port';

const transactions = new Map<string, Transaction>();

export abstract class UnitOfWorkObjection extends UnitOfWork {
  protected transactions = transactions;

  async start(config?: StartTransactionConfig): Promise<string> {
    const id: string = UUID.generate().value;
    await this.startFromId(id, config);
    return id;
  }

  async startFromId(
    id: string,
    config?: StartTransactionConfig,
  ): Promise<void> {
    if (!this.transactions.has(id)) {
      const trx = await Model.knex().transaction({
        isolationLevel: config?.isolationLevel,
      });

      this.transactions.set(id, trx);
    }
  }

  getTrx(id: string): Transaction {
    return this.transactions.get(id);
  }

  hasTrx(id: string): boolean {
    return this.transactions.has(id);
  }

  protected finish(id: string): void {
    this.aggregates.delete(id);
    this.transactions.delete(id);
  }

  async commit(id: string): Promise<void> {
    const trx = this.getTrx(id);

    if (!trx) {
      return;
    }

    await trx.commit();
    this.finish(id);
  }

  async rollback(id: string): Promise<void> {
    const trx = this.transactions.get(id);

    if (trx) {
      await trx.rollback();

      this.finish(id);
    }
  }
}
