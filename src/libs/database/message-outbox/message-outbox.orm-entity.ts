import { Message } from 'src/libs/messages/message.base';

import { MessageOutboxStatus } from './types';

export interface MessageOutboxOrmEntityProps {
  id: string;
  token: string;
  context: string;
  payload: Record<string, any>;
  trxId: string;
  dateOccurred: string;
  status: MessageOutboxStatus;
}

export class MessageOutboxOrmEntity {
  id: string;
  token: string;
  context: string;
  payload: Record<string, any>;
  trxId: string;
  dateOccurred: string;
  status: MessageOutboxStatus;

  constructor(props: MessageOutboxOrmEntityProps) {
    this.id = props.id;
    this.token = props.token;
    this.context = props.context;
    this.payload = props.payload;
    this.trxId = props.trxId;
    this.dateOccurred = props.dateOccurred;
    this.status = props.status;
  }

  static create(props: MessageOutboxOrmEntityProps): MessageOutboxOrmEntity {
    return new MessageOutboxOrmEntity(props);
  }

  static fromMessage(message: Message): MessageOutboxOrmEntity {
    return new MessageOutboxOrmEntity({
      ...message,
      status: MessageOutboxStatus.PENDING,
    });
  }
}
