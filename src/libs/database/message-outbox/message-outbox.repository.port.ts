// TODO: подумать над расположением интерфейсов
// import { IMessage } from 'src/infra/messages/message.interface';
import { ExceptionBase } from 'src/libs/exceptions';
import { IMessage } from 'src/libs/messages/message.interface';
import { Result } from 'src/libs/utils';

export interface MessageOutboxRepositoryPort {
  insert(message: IMessage): Promise<Result<void, ExceptionBase>>;

  insertBulk(messages: IMessage[]): Promise<Result<void, ExceptionBase>>;

  getById(id: string): Promise<Result<IMessage, ExceptionBase>>;

  getPending(): Promise<Result<IMessage[], ExceptionBase>>;

  markAsProcessed(message: IMessage): Promise<Result<void, ExceptionBase>>;

  markAsRejected(message: IMessage): Promise<Result<void, ExceptionBase>>;

  removeProcessed(): Promise<Result<void, ExceptionBase>>;

  getTableName(): string;
}
