import { Knex } from 'knex';
import { Model } from 'objection';
import { ExceptionBase } from 'src/libs/exceptions';
import { Message } from 'src/libs/messages/message.base';
import { Result } from 'src/libs/utils';
import { objectUtils } from 'src/libs/utils/object';
import { stringUtils } from 'src/libs/utils/string';

import { UnitOfWorkObjection } from '../unit-of-work';
import { MessageOutboxOrmEntity } from './message-outbox.orm-entity';
import { MessageOutboxRepositoryBase } from './message-outbox.repository.base';
import { MessageOutboxStatus } from './types';

export abstract class MessageOutboxObjectionRepository extends MessageOutboxRepositoryBase<UnitOfWorkObjection> {
  private getQb(): Knex.QueryBuilder {
    const knex = Model.knex();
    const qb = knex(this.tableName);

    if (this.trxId) {
      const transaction = this.unitOfWork.getTrx(this.trxId);

      qb.transacting(transaction);
    }

    return qb;
  }

  // TODO: сделать этот метод нормальным
  async getById(id: string): Promise<Result<Message, ExceptionBase>> {
    try {
      const rawMessage = await this.getQb().where('id', id).first();
      const serializeMessage = (rawMessage: any) => {
        const camelCased = objectUtils.mapKeys(rawMessage, (_, key) => {
          return stringUtils.toCamelCase(key);
        });

        const isPayloadObject = typeof camelCased.payload === 'object';
        const payload = isPayloadObject
          ? camelCased.payload
          : JSON.parse(camelCased.payload);

        return { ...camelCased, payload } as Message;
      };

      const message = serializeMessage(rawMessage);
      return Result.ok(message);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async insert(message: Message): Promise<Result<void, ExceptionBase>> {
    try {
      await this.getQb().insert(MessageOutboxOrmEntity.fromMessage(message));

      return Result.ok();
    } catch (e) {
      return Result.fail(e);
    }
  }

  async insertBulk(messages: Message[]): Promise<Result<void, ExceptionBase>> {
    try {
      if (messages.length === 0) {
        return Result.ok();
      }

      const entities = messages.map((message) =>
        MessageOutboxOrmEntity.fromMessage(message),
      );

      await this.getQb().insert(entities);

      return Result.ok();
    } catch (e) {
      return Result.fail(e);
    }
  }

  async getPending(): Promise<Result<Message[], ExceptionBase>> {
    try {
      const data = await this.getQb()
        .select()
        .where({ status: MessageOutboxStatus.PENDING });

      return Result.ok(data);
    } catch (e) {
      return Result.fail(e);
    }
  }

  async markAsProcessed(
    message: Message,
  ): Promise<Result<void, ExceptionBase>> {
    try {
      await this.getQb()
        .update({ status: MessageOutboxStatus.PROCESSED })
        .where({ id: message.id, status: MessageOutboxStatus.PENDING });

      return Result.ok();
    } catch (e) {
      return Result.fail(e);
    }
  }

  async markAsRejected(message: Message): Promise<Result<void, ExceptionBase>> {
    try {
      await this.getQb()
        .update({ status: MessageOutboxStatus.REJECTED })
        .where({ id: message.id, status: MessageOutboxStatus.PENDING });

      return Result.ok();
    } catch (e) {
      return Result.fail(e);
    }
  }

  async removeProcessed(): Promise<Result<void, ExceptionBase>> {
    try {
      await this.getQb().del().where({ status: MessageOutboxStatus.REJECTED });

      return Result.ok();
    } catch (e) {
      return Result.fail(e);
    }
  }
}
