import { MessageOutboxRepositoryPort } from 'src/libs/database/message-outbox/message-outbox.repository.port';
import { ExceptionBase } from 'src/libs/exceptions';
import { Message } from 'src/libs/messages/message.base';
import { Result } from 'src/libs/utils';

import { UnitOfWorkPort } from '../unit-of-work';

export abstract class MessageOutboxRepositoryBase<
  UnitOfWork extends UnitOfWorkPort = UnitOfWorkPort,
> implements MessageOutboxRepositoryPort
{
  protected abstract tableName: string;

  constructor(
    protected readonly unitOfWork: UnitOfWork,
    protected readonly trxId?: string,
  ) {}

  abstract getById(id: string): Promise<Result<Message, ExceptionBase>>;

  abstract insert(message: Message): Promise<Result<void, ExceptionBase>>;

  abstract insertBulk(
    messages: Message[],
  ): Promise<Result<void, ExceptionBase>>;

  abstract getPending(): Promise<Result<Message[], ExceptionBase>>;

  abstract markAsProcessed(
    message: Message,
  ): Promise<Result<void, ExceptionBase>>;

  abstract markAsRejected(
    message: Message,
  ): Promise<Result<void, ExceptionBase>>;

  abstract removeProcessed(): Promise<Result<void, ExceptionBase>>;

  public getTableName(): string {
    return this.tableName;
  }
}
