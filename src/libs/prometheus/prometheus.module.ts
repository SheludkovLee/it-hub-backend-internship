import { Module } from '@nestjs/common';
import { PrometheusModule as NestPrometheusModule } from '@willsoto/nestjs-prometheus';

import { PrometheusController } from './prometheus.controller';

@Module({
  imports: [
    NestPrometheusModule.register({
      controller: PrometheusController,
      path: '/prom/Hiv2YSmGyHu35rBx6w2e',
    }),
  ],
})
export class PrometheusModule {}
