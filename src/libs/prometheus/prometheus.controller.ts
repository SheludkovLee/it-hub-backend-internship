import { Controller, Get, Res } from '@nestjs/common';
import { PrometheusController as NestPrometheusController } from '@willsoto/nestjs-prometheus';
import { FastifyReply } from 'fastify';
import { Public } from 'src/common';

@Public()
@Controller()
export class PrometheusController extends NestPrometheusController {
  @Get()
  async index(@Res({ passthrough: true }) response: FastifyReply) {
    return super.index(response);
  }
}
