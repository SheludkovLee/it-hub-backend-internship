import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { ScheduleModule as NestSchedulerModule } from '@nestjs/schedule';
import { ClsModule } from 'nestjs-cls';
import { v4 } from 'uuid';

import { ApiModule } from './api/module';
import { AppController } from './app.controller';
import { AuthJwtGuard, CommonModule } from './common';
import { AttachPermissionsCheckerGuard } from './common/guards/permissions/attach-permissions-checker.guard';
import { IdentityModule } from './domains/identity/identity.module';
import { LearningModule } from './domains/learning/learning.module';
import { LearningDesignModule } from './domains/learning-design/learning-design.module';
import * as config from './infra/config';
import { FileStorageModule } from './libs/file-storage';
import { AppGraphqlModule } from './libs/graphql/app-graphql.module';
import { AppJwtModule } from './libs/jwt/jwt.module';
import { LoggingModule } from './libs/logging/logging.module';
import { MailingModule } from './libs/mailing/mailing.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config.configuration],
      validationSchema: config.validationSchema,
      validationOptions: config.validationOptions,
    }),
    ClsModule.forRoot({
      middleware: {
        mount: true,
        generateId: true,
        idGenerator: v4,
      },
      global: true,
    }),
    IdentityModule,
    CommonModule,
    MailingModule,
    AppJwtModule,
    AppGraphqlModule,
    LearningDesignModule,
    LearningModule,
    ApiModule,
    FileStorageModule,
    NestSchedulerModule.forRoot(),
    LoggingModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthJwtGuard,
    },
    {
      provide: APP_GUARD,
      useClass: AttachPermissionsCheckerGuard,
    },
  ],
  controllers: [AppController],
})
export class AppModule {}
