import {
  MessageOutboxObjectionRepository,
  MessageOutboxRepositoryPort,
  UnitOfWorkObjection,
  UnitOfWorkPort,
} from 'src/libs/database';
import { MessageInboxRepositoryPort } from 'src/libs/database/message-inbox';

export function createUnitOfWork(outboxTableName: string): UnitOfWorkPort {
  class UnitOfWork extends UnitOfWorkObjection {
    getMessagesOutboxRepository(trxId?: string): MessageOutboxRepositoryPort {
      class MessageOutboxRepository extends MessageOutboxObjectionRepository {
        protected tableName = outboxTableName;
      }

      return new MessageOutboxRepository(this, trxId);
    }

    getMessageInboxRepository(trxId?: string): MessageInboxRepositoryPort {
      throw new Error('Not implemented');
    }
  }

  return new UnitOfWork();
}
