import { DynamicModule, Module, Provider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Client as PgClient } from 'pg';

import { currentEnv } from '../config';
import { MessageBroker } from './message-broker';
import { MessageBus } from './message-bus';
import { MessageRelay } from './message-relay';

type BoundedContext =
  | 'identity'
  | 'learning'
  | 'learning_design'
  | 'organization_management'
  | 'role_system'
  | 'schedule'
  | 'notification';

type Options = {
  boundedContext: BoundedContext;
  outboxChanelName: string;
  outboxTableName: string;
};

const OPTIONS_PROVIDER_TOKEN = 'OPTIONS';

@Module({})
export class CommunicationModule {
  static init(options: Options): DynamicModule {
    /**
     * Create new user for pg_notify session mode
     */

    let pgClientProvider: Provider<PgClient>;

    if (currentEnv === 'production' || currentEnv === 'preproduction') {
      pgClientProvider = {
        provide: PgClient,
        useFactory(configService: ConfigService) {
          const host = configService.get<string>('postgres.host');
          const port = configService.get<number>('postgres.port');
          const user = configService.get<string>('postgres.sessionUser');
          const password = configService.get<string>(
            'postgres.sessionPassword',
          );
          const database = configService.get<string>('postgres.dbName');
          return new PgClient({ host, port, user, password, database });
        },
        inject: [ConfigService],
      };
    } else {
      pgClientProvider = {
        provide: PgClient,
        useFactory(configService: ConfigService) {
          const host = configService.get<string>('postgres.host');
          const port = configService.get<number>('postgres.port');
          const user = configService.get<string>('postgres.user');
          const password = configService.get<string>('postgres.password');
          const database = configService.get<string>('postgres.dbName');
          return new PgClient({ host, port, user, password, database });
        },
        inject: [ConfigService],
      };
    }

    const messageBusProvider: Provider<MessageBus> = {
      provide: MessageBus,
      useValue: MessageBus.getInstance(),
    };

    const messageBrokerProvider: Provider<MessageBroker> = {
      provide: MessageBroker,
      useFactory(messageBus: MessageBus, configService: ConfigService) {
        return MessageBroker.getInstance(messageBus, {
          host: configService.get<string>('redis.host'),
          port: configService.get<number>('redis.port'),
          password: configService.get<string>('redis.password'),
        });
      },
      inject: [MessageBus, ConfigService],
    };

    const messageRelayProvider: Provider<MessageRelay> = {
      provide: MessageRelay,
      useFactory(
        messageBroker: MessageBroker,
        pgClient: PgClient,
        options: Options,
      ) {
        return new MessageRelay(messageBroker, pgClient, options);
      },
      inject: [MessageBroker, PgClient, OPTIONS_PROVIDER_TOKEN],
    };

    return {
      module: CommunicationModule,
      providers: [
        pgClientProvider,
        messageBusProvider,
        messageBrokerProvider,
        messageRelayProvider,
        {
          provide: OPTIONS_PROVIDER_TOKEN,
          useValue: options,
        },
      ],
      exports: [MessageBus],
      global: false,
    };
  }
}
