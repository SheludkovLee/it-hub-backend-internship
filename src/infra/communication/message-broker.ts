import { IMessage } from 'src/libs/messages/message.interface';
import { ConnectionOptions, Queue } from 'src/libs/queue';
import { arrayUtils } from 'src/libs/utils/array';

import { MessageBus } from './message-bus';

export class MessageBroker {
  private static instance: MessageBroker = undefined;
  private queueByContextMap: Map<string, Queue<IMessage>> = new Map();

  private constructor(
    private readonly messageBus: MessageBus,
    private readonly connectionOptions?: ConnectionOptions,
  ) {}

  static getInstance(
    messageBus: MessageBus,
    connectionOptions?: ConnectionOptions,
  ): MessageBroker {
    if (!this.instance) {
      this.instance = new MessageBroker(messageBus, connectionOptions);
    }

    return this.instance;
  }

  private getQueueByContext(context: string) {
    if (this.queueByContextMap.has(context)) {
      return this.queueByContextMap.get(context);
    }
    const queueName = `message_broker_${context}`;
    const queue = new Queue<IMessage>(
      queueName,
      async (job) => {
        this.messageBus.publish(job.data);
      },
      this.connectionOptions,
    );
    // TODO: add handler for job 'failed'

    this.queueByContextMap.set(context, queue);
    queue.run();

    return queue;
  }

  public async addMessage(message: IMessage) {
    const queue = this.getQueueByContext(message.context);
    await queue.addJob({ data: message, name: message.token });
  }

  public async addMessages(messages: IMessage[]) {
    const messagesByContextMapping = arrayUtils.groupBy(
      messages,
      (message) => message.context,
    );

    const { entries } = Object;
    for (const [context, messages] of entries(messagesByContextMapping)) {
      const queue = await this.getQueueByContext(context);
      const jobs = messages.map((message) => ({
        data: message,
        name: message.token,
      }));
      await queue.addJobs(jobs);
    }
  }
}
