import { Logger } from '@nestjs/common';
import { Client as PgClient } from 'pg';
import { UnitOfWorkPort } from 'src/libs/database';
import { IMessage } from 'src/libs/messages/message.interface';
import { Result } from 'src/libs/utils';

import { MessageBroker } from './message-broker';
import { createUnitOfWork } from './unit-of-work';

type Options = {
  outboxChanelName: string;
  outboxTableName: string;
};

export class MessageRelay {
  private readonly logger = new Logger(MessageRelay.name);
  private readonly unitOfWork: UnitOfWorkPort;

  constructor(
    private readonly messageBroker: MessageBroker,
    private readonly pgClient: PgClient,
    private readonly options: Options,
  ) {
    this.unitOfWork = createUnitOfWork(options.outboxTableName);
    this.start();
  }

  private async start(): Promise<void> {
    const trxId = await this.unitOfWork.start();
    const addPendingMessagesToBroker = async () => {
      const outboxRepository =
        this.unitOfWork.getMessagesOutboxRepository(trxId);

      const getPendingMessagesResult = await outboxRepository.getPending();
      if (getPendingMessagesResult.isErr) {
        return getPendingMessagesResult;
      }

      const pendingMessagesList = getPendingMessagesResult.unwrap();
      for (const pendingMessage of pendingMessagesList) {
        await this.processMessage(pendingMessage, trxId);
      }

      return Result.ok();
    };
    await this.unitOfWork.execute(trxId, addPendingMessagesToBroker);
    await this.listenMessageOutboxTable();
  }

  private async listenMessageOutboxTable(): Promise<void> {
    const { outboxChanelName } = this.options;

    this.pgClient.on('notification', async (notification) => {
      this.logger.debug(
        notification,
        `Message relay context ${outboxChanelName}`,
      );

      if (!notification.payload) {
        return;
      }

      const isTargetChannel = notification.channel === outboxChanelName;
      if (isTargetChannel) {
        const messageId = notification.payload;

        const trxId = await this.unitOfWork.start();
        await this.unitOfWork.execute(trxId, async () => {
          const outboxRepository =
            this.unitOfWork.getMessagesOutboxRepository(trxId);

          const message = await outboxRepository
            .getById(messageId)
            .then((result) => result.unwrap());

          await this.processMessage(message, trxId);
          return Result.ok();
        });
      }
    });

    this.pgClient.on('error', (error) => {
      Logger.error(error);
    });

    try {
      await this.pgClient.connect();

      await this.pgClient.query(`LISTEN ${outboxChanelName}`);
    } catch (error) {
      this.logger.error(error);
    }
  }

  private processMessage = async (message: IMessage, trxId: string) => {
    const outboxRepository = this.unitOfWork.getMessagesOutboxRepository(trxId);

    try {
      this.logger.log(message, `Message Relay process message`);

      await this.messageBroker.addMessage(message);
      await outboxRepository.markAsProcessed(message);
    } catch (error) {
      this.logger.error(error);
      // await outboxRepository.markAsRejected(message);
      // TODO: add message to DLQ
    }
  };
}
