import { IMessage } from 'src/libs/messages/message.interface';
import { IMessageHandler } from 'src/libs/messages/message-handler.interface';
import { Result } from 'src/libs/utils';

import { BoundedContext } from '../../../database/types';

export class MessageBus {
  private static instance: MessageBus = undefined;
  private messageHandlersMap: Map<string, IMessageHandler[]> = new Map();
  private syncMessageHandlersMap: Map<string, IMessageHandler[]> = new Map();

  static getInstance() {
    if (!this.instance) {
      this.instance = new MessageBus();
    }
    return this.instance;
  }

  public register(messageToken: string, handler: IMessageHandler): void {
    if (!this.messageHandlersMap.has(messageToken)) {
      this.messageHandlersMap.set(messageToken, []);
    }
    this.messageHandlersMap.get(messageToken).push(handler);
  }

  public registerSyncHandler(
    messageToken: string,
    handler: IMessageHandler,
  ): void {
    if (!this.syncMessageHandlersMap.has(messageToken)) {
      this.syncMessageHandlersMap.set(messageToken, []);
    }
    this.syncMessageHandlersMap.get(messageToken).push(handler);
  }

  public publish(message: IMessage): void {
    this._publish(message);
  }

  public async publishAsSync(message: IMessage, context?: BoundedContext) {
    let messageHandlers = this.messageHandlersMap.get(message.token) || [];

    if (context) {
      messageHandlers = messageHandlers.filter(
        (handler) => handler.getContext() === context,
      );
    }

    for (const handler of messageHandlers) {
      const result = await handler.handle(message);
      if (result.isErr) {
        return result;
      }
    }

    return Result.ok();
  }

  public publishBulk(messages: IMessage[]): void {
    for (const message of messages) {
      this._publish(message);
    }
  }

  public async syncPublish(message: IMessage) {
    const messageHandlers =
      this.syncMessageHandlersMap.get(message.token) || [];

    for (const handler of messageHandlers) {
      const result = await handler.handle(message);
      if (result.isErr) {
        return result;
      }
    }

    return Result.ok();
  }

  private _publish(message: IMessage) {
    const messageHandlers = this.messageHandlersMap.get(message.token) || [];
    for (const handler of messageHandlers) {
      handler.handle(message);
    }
  }
}
