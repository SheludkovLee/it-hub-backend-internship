import {IMessage} from 'src/libs/messages/message.interface';

type Payload = {
    id: string;
    name: string;
    description: string;
    studyHoursCount: number;
    maxScore: number;
    code: string;
    materials: string;
};

export interface ITemplateDisciplineCreatedMessage extends IMessage<Payload> {
}
