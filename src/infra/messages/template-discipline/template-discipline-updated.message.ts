import {IMessage} from 'src/libs/messages/message.interface';

type Payload = {
    id: string;
    studyHoursCount: number;
    name: string;
    description: string;
    maxScore: number;
    code: string;
    materials: string;
};

export interface ITemplateDisciplineUpdatedMessage extends IMessage<Payload> {
}
