import {IMessage} from 'src/libs/messages/message.interface';

type Payload = {
    id: string;
};

export interface ITemplateDisciplineDeletedMessage extends IMessage<Payload> {
}
