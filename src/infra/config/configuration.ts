import * as Joi from 'joi';
import { BaseValidationOptions } from 'joi';

type AppEnvironment =
  | 'development'
  | 'staging'
  | 'production'
  | 'preproduction';

export const currentEnv = process.env.APP_ENVIRONMENT as AppEnvironment;
export const isProdOrPreprodEnv = (env: AppEnvironment): boolean =>
  env === 'production' || env === 'preproduction';

export const isProdEnv = (env: AppEnvironment): boolean => env === 'production';

export const isDevEnv = (env: AppEnvironment): boolean => env === 'development';

export const validationSchema = Joi.object({
  // APP
  PORT: Joi.number().required(),
  APP_URL: Joi.string().required(),
  APP_ENVIRONMENT: Joi.string().valid(
    'development',
    'staging',
    'production',
    'preproduction',
  ),
  NODE_ENV: Joi.string()
    .valid('development', 'production', 'staging', 'preproduction')
    .default('development'),
  FRONTEND_DOMAIN: Joi.string().optional(),

  // POSTGRES
  POSTGRES_HOST: Joi.string().required(),
  POSTGRES_PORT: Joi.number().required(),
  POSTGRES_DB_NAME: Joi.string().required(),
  POSTGRES_USER: Joi.string().required(),
  POSTGRES_PASSWORD: Joi.string().required(),

  POSTGRES_SESSION_USER: Joi.string().optional(),
  POSTGRES_SESSION_PASSWORD: Joi.string().optional(),

  // REDIS
  REDIS_HOST: Joi.string().hostname().required(),
  REDIS_PORT: Joi.number().port().required(),
  REDIS_PASSWORD: Joi.alternatives().conditional('APP_ENVIRONMENT', {
    is: 'production',
    then: Joi.string().required(),
    otherwise: Joi.string().optional(),
  }),

  // JWT
  JWT_SECRET: Joi.string().required(),
  JWT_EXPIRES_IN: Joi.string().required(),

  // MAILING MODULE
  EMAIL_MAILING_ENABLE: Joi.bool().default(true),

  // UNISENDER
  UNISENDER_BASE_URL: Joi.string().optional(),
  UNISENDER_API_KEY: Joi.string().optional(),
  UNISENDER_REGISTRATION_CONFIRMATION_TEMPLATE_ID: Joi.string().optional(),
  UNISENDER_RESET_PASSWORD_TEMPLATE_ID: Joi.string().optional(),
  UNISENDER_DAILY_DIGEST_TEMPLATE_ID: Joi.string().optional(),

  REGISTRATION_CONFIRMATION_URL: Joi.string().optional(),
  RESET_PASSWORD_URL: Joi.string().optional(),

  // S3
  S3_ACCESS_KEY: Joi.string().required(),
  S3_SECRET_ACCESS_KEY: Joi.string().required(),
  S3_ENDPOINT: Joi.string().required(),
  S3_BUCKET_NAME: Joi.string().required(),
  S3_TEMPORARY_BUCKET_NAME: Joi.string().optional(),
  S3_REGION: Joi.string().required(),

  // START_EXAM
  START_EXAM_SECRET_KEY: Joi.string().required(),
  START_EXAM_ACCOUNT_ID: Joi.string().required(),
  START_EXAM_CENTER: Joi.string().required(),

  //SENTRY
  SENTRY_DSN: Joi.string().optional(),

  //CLICKHOUSE
  CLICKHOUSE_HOST: Joi.string().optional(),
  CLICKHOUSE_DATABASE: Joi.string().optional(),
  CLICKHOUSE_USER: Joi.string().optional(),
  CLICKHOUSE_PASSWORD: Joi.string().optional(),

  FEATURE_FLAGS: Joi.string().optional(),

  //FLUENTD
  FLUENTD_HOST: Joi.string().optional(),
  FLUENTD_PORT: Joi.string().optional(),

  KINESCOPE_API_KEY: Joi.string().required(),

  //ZAMMAD
  ZAMMAD_API_KEY: Joi.string().required(),
});

export const configuration = () => ({
  app: {
    url: process.env.APP_URL,
    port: parseInt(process.env.PORT, 10),
    environment: process.env.NODE_ENV,
    frontendDomain: process.env.FRONTEND_DOMAIN,
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    expiresIn: process.env.JWT_EXPIRES_IN,
  },
  postgres: {
    host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_PORT,
    dbName: process.env.POSTGRES_DB_NAME,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    sessionUser: process.env.POSTGRES_SESSION_USER,
    sessionPassword: process.env.POSTGRES_SESSION_PASSWORD,
  },
  redis: {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT, 10),
    password: process.env.REDIS_PASSWORD,
  },
  unisender: {
    baseUrl: process.env.UNISENDER_BASE_URL,
    apiKey: process.env.UNISENDER_API_KEY,
    registrationConfirmationTemplateId:
      process.env.UNISENDER_REGISTRATION_CONFIRMATION_TEMPLATE_ID,
    resetPasswordTemplateId: process.env.UNISENDER_RESET_PASSWORD_TEMPLATE_ID,
    dailyDigestTemplateId: process.env.UNISENDER_DAILY_DIGEST_TEMPLATE_ID,
  },

  emailMailingEnable: process.env.EMAIL_MAILING_ENABLE,

  registrationConfirmationUrl: process.env.REGISTRATION_CONFIRMATION_URL,
  resetPasswordUrl: process.env.RESET_PASSWORD_URL,

  S3: {
    accessKey: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
    endpoint: process.env.S3_ENDPOINT,
    bucketName: process.env.S3_BUCKET_NAME,
    temporaryBucketName: process.env.S3_TEMPORARY_BUCKET_NAME,
    region: process.env.S3_REGION,
  },

  startExam: {
    secretKey: process.env.START_EXAM_SECRET_KEY,
    accountId: process.env.START_EXAM_ACCOUNT_ID,
    center: process.env.START_EXAM_CENTER,
  },

  sentry: {
    dsn: process.env.SENTRY_DSN,
  },

  clickhouse: {
    host: process.env.CLICKHOUSE_HOST,
    database: process.env.CLICKHOUSE_DATABASE,
    user: process.env.CLICKHOUSE_USER,
    password: process.env.CLICKHOUSE_PASSWORD,
  },

  fluentd: {
    host: process.env.FLUENTD_HOST,
    port: process.env.FLUENTD_PORT,
  },

  kinescope: {
    apiKey: process.env.KINESCOPE_API_KEY,
  },

  zammad: {
    apiKey: process.env.ZAMMAD_API_KEY,
  },
});

export const validationOptions: BaseValidationOptions = {
  abortEarly: false,
  convert: true,
};
