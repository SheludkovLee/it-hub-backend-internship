const envFlags: string[] = process.env.FEATURE_FLAGS
  ? JSON.parse(process.env.FEATURE_FLAGS)
  : [];

export const featureFlags = {
  digestMailing: envFlags.includes('digestMailing'),
  clickhouse: envFlags.includes('clickhouse'),
  fluentdLogging: envFlags.includes('fluentdLogging'),
  specialityCopyV2: envFlags.includes('specialityCopyV2'),
  studentAvailableTasks_V2: envFlags.includes('studentAvailableTasks_V2'),
  migrateOldOrganizations: envFlags.includes('migrateOldOrganizations'),
};
