import { INestApplication, Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import * as Sentry from '@sentry/node';
import Knex, { Knex as KnexType } from 'knex';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import * as objection from 'objection';

import knexConfig from '../knexfile';
import { AppModule } from './app.module';
import { ExceptionInterceptor } from './common';
import { currentEnv } from './infra/config';

const useNewRelic = () => {
  if (currentEnv !== 'development') {
    require('newrelic');
  }
};

const useSentry = (app: INestApplication) => {
  const configService = app.get<ConfigService>(ConfigService);

  const dsn = configService.get<string>('sentry.dsn');

  Sentry.init({
    dsn: dsn,
    tracesSampleRate: 1.0,
    environment: currentEnv,
    enabled: currentEnv !== 'development',
  });
};

const useKnexLogger = (knex: KnexType) => {
  knex.on('query', (...args) => {
    Logger.log(args[0], 'Knex');
  });
};

const initObjection = () => {
  const knex = Knex(knexConfig);

  useKnexLogger(knex);

  objection.Model.knex(knex);
};

const runServer = async () => {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({ bodyLimit: 54 * 1024 * 1024 }),
    {
      bufferLogs: true,
    },
  );

  app.enableCors();
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      forbidUnknownValues: false,
    }),
  );
  app.useGlobalInterceptors(new ExceptionInterceptor());
  app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));
  useNewRelic();
  useSentry(app);

  const config = app.get<ConfigService>(ConfigService);
  const port = config.get<number>('app.port');

  await app.listen(port, '0.0.0.0');
};

const main = () => {
  initObjection();
  runServer();
};

main();
