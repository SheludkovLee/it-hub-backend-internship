import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType({ isAbstract: true })
export abstract class BasePaginatedPayload<TData> {
  abstract items: TData[];

  @Field(() => Int)
  page: number;

  @Field(() => Int)
  perPage: number;

  @Field(() => Int)
  totalPages: number;

  @Field(() => Int)
  total: number;

  @Field(() => Boolean)
  hasMore: boolean;

  constructor(props: BasePaginatedPayload<TData>) {
    this.page = props.page;
    this.perPage = props.perPage;
    this.totalPages = props.totalPages;
    this.total = props.total;
    this.hasMore = props.hasMore;
  }
}
