import {Global, Module} from '@nestjs/common';

import {
  AsyncDomainEventsPublisherProvider,
  AsyncEventBus,
  CommandBusProvider,
  DomainEventsPublisherProvider,
  EventBus,
  QueryBusProvider,
} from './providers';

@Global()
@Module({
    providers: [
        CommandBusProvider,
        EventBus,
        AsyncEventBus,
        DomainEventsPublisherProvider,
        AsyncDomainEventsPublisherProvider,
        QueryBusProvider,
    ],
    exports: [
        CommandBusProvider,
        EventBus,
        AsyncEventBus,
        DomainEventsPublisherProvider,
        AsyncDomainEventsPublisherProvider,
        QueryBusProvider,
    ],
})
export class CommonModule {
}
