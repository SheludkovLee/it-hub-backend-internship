import { Field, InputType, Int } from '@nestjs/graphql';
import { IsOptional, IsPositive, Max } from 'class-validator';

@InputType({ isAbstract: true })
export class PaginationInputType {
  @IsOptional()
  @IsPositive()
  @Field(() => Int, { defaultValue: 1 })
  page: number;

  @IsOptional()
  @IsPositive()
  @Max(50)
  @Field(() => Int, {
    defaultValue: 20,
  })
  pageSize: number;
}
