import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class DateRangeInputType {
  @Field(() => Date)
  from: string;

  @Field(() => Date)
  to: string;
}
