export type PartialField<Type, Field extends keyof Type> = Omit<Type, Field> &
  Partial<Pick<Type, Field>>;
