import {
  BadRequestException,
  ConflictException,
  ExecutionContext,
  ForbiddenException,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { GqlContextType, GqlExecutionContext } from '@nestjs/graphql';
import * as Sentry from '@sentry/node';
import { currentEnv, isProdOrPreprodEnv } from 'src/infra/config';
import { ExceptionCodes } from 'src/libs/exceptions';

export const exceptionsMapper = (ex, context: ExecutionContext) => {
  Logger.error(ex);

  if (ex.toJSON) {
    const exception = ex.toJSON();
    switch (exception.code) {
      case ExceptionCodes.CONFLICT:
        throw new ConflictException(exception.message);

      case ExceptionCodes.FORBIDDEN:
        throw new ForbiddenException(exception.message);

      case ExceptionCodes.NOT_FOUND:
        throw new NotFoundException(exception.message);

      case ExceptionCodes.VALIDATION:
        throw new BadRequestException(exception.message);

      case ExceptionCodes.ARGUMENT_INVALID:
        throw new BadRequestException(exception.message);

      case ExceptionCodes.INVALID:
        throw new BadRequestException(exception.message);

      case ExceptionCodes.ARGUMENT_OUT_OF_RANGE:
        throw new BadRequestException(exception.message);

      case ExceptionCodes.ARGUMENT_NOT_PROVIDED:
        throw new BadRequestException(exception.message);

      default:
        throw new BadRequestException(exception.message);
    }
  }

  /**
   * Логаем ошибку, если она какая-то странная
   */
  if (context.getType<GqlContextType>() === 'graphql') {
    const { req } = GqlExecutionContext.create(context).getContext();
    Sentry.captureException(ex, (scope) => {
      scope.setTag('operationName', req.body.operationName);
      scope.setExtra('query', req.body.query);
      scope.setExtra('variables', req.body.variables);
      scope.setUser({ id: req.user?.id });
      return scope;
    });
  } else {
    Sentry.captureException(ex);
  }

  /**
   * Не отправляем странные ошибки на фронт, если это прод
   */
  if (isProdOrPreprodEnv(currentEnv)) {
    throw new InternalServerErrorException(
      `Что-то пошло не так... Попробуйте перезагрузить страницу.`,
    );
  }

  /**
   * Кинем ошибку на фронт как есть, если это стейдж или препрод
   */
  throw ex;
};
