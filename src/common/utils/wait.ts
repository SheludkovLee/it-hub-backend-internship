export async function wait(ms = 5000) {
  await new Promise((resolve) =>
    setTimeout(() => {
      resolve(true);
    }, ms),
  );
}
