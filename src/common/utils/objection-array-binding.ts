export const arrayBinding = (array: unknown[]) =>
  array.map((_) => '?').join(',');
