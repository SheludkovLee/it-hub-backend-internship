export type WithRequiredProperty<Type, Property extends keyof Type> = Type &
  Required<Pick<Type, Property>>;
