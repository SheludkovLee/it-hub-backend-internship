import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

import { AppRequest } from '../types/app-request.type';

export const GetPermissionsChecker = createParamDecorator(
  (_: unknown, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);
    const { req } = ctx.getContext();

    const appReq = req as AppRequest;

    return appReq.permissionsChecker;
  },
);
