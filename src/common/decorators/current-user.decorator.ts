import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { UserObjectionOrmEntity } from 'src/domains/identity/database';

import { AppRequest } from '../types/app-request.type';

export class CurrentRequestUser extends UserObjectionOrmEntity {}

export const CurrentUser = createParamDecorator(
  (field: keyof CurrentRequestUser, context: ExecutionContext) => {
    const ctx = GqlExecutionContext.create(context);
    const { req } = ctx.getContext();

    const appReq = req as AppRequest;

    const user = appReq.user;

    if (field) {
      return user[field];
    }

    return user;
  },
);
