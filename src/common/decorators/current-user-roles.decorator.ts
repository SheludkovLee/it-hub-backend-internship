import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

import {
  AppRequest,
  ICurrentUserRolesProvider,
} from '../types/app-request.type';

export const GetCurrentUserRolesProvider = createParamDecorator(
  (_: unknown, context: ExecutionContext): ICurrentUserRolesProvider => {
    const ctx = GqlExecutionContext.create(context);
    const { req } = ctx.getContext();

    const appReq = req as AppRequest;

    return appReq.rolesProvider;
  },
);
