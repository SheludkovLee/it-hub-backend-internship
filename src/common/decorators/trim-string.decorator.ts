import { Transform, TransformFnParams } from 'class-transformer';

export const TrimString = ({ isArray = false } = {}) => {
  if (isArray) {
    return trimArrayStringsTransformer;
  } else {
    return trimStringTransformer;
  }
};

const trimStringTransformer = Transform(({ value }: TransformFnParams) =>
  typeof value === 'string' ? value?.trim() : value,
);

const trimArrayStringsTransformer = Transform(
  ({ value: values }: TransformFnParams) =>
    Array.isArray(values) ? values.map((value) => value?.trim()) : values,
);
