import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AppRequest } from 'src/common/types/app-request.type';

import { PermissionsChecker } from './permissions-checker';

@Injectable()
export class AttachPermissionsCheckerGuard implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const gqlContext = GqlExecutionContext.create(context);
    const { req } = gqlContext.getContext();

    if (!req) {
      return true;
    }

    const appReq = req as AppRequest;

    if (!appReq.user) {
      return true;
    }

    const permissionsChecker = new PermissionsChecker(appReq.rolesProvider);

    appReq.permissionsChecker = permissionsChecker;

    return true;
  }
}
