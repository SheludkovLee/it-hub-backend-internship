import {
  AbilityBuilder,
  createMongoAbility,
  MongoAbility,
  subject,
} from '@casl/ability';
import { ForbiddenError } from '@nestjs/apollo';
import { ICurrentUserRolesProvider } from 'src/common/types/app-request.type';

import {
  OrganizationAdminAbilities,
  OrganizationAdminAbilitiesBuilder,
} from './ability-builders/organization-admin.ability-builder';
import {
  SuborganizationAdminAbilities,
  SuborganizationAdminAbilitiesBuilder,
} from './ability-builders/suborganization-admin.ability-builder';
import {
  SuperAdminAbilities,
  SuperAdminAbilitiesBuilder,
} from './ability-builders/super-admin.ability-builder';
import {
  TeacherAbilities,
  TeacherAbilitiesBuilder,
} from './ability-builders/teacher.ability-builder';
import { PermissionsQueryBuilder } from './permissions-query-builder';

export type AllRolesAbilities =
  | SuperAdminAbilities
  | OrganizationAdminAbilities
  | SuborganizationAdminAbilities
  | TeacherAbilities;

export type AppAbility = MongoAbility<AllRolesAbilities>;

export class PermissionsChecker {
  private userAbilities: AppAbility;

  constructor(private readonly rolesProvider: ICurrentUserRolesProvider) {}

  async prepareAbilities() {
    const mainAbilityBuilder = new AbilityBuilder<AppAbility>(
      createMongoAbility,
    );

    const abilityBuilders = [
      new SuperAdminAbilitiesBuilder(this.rolesProvider),
      new OrganizationAdminAbilitiesBuilder(this.rolesProvider),
      new SuborganizationAdminAbilitiesBuilder(this.rolesProvider),
      new TeacherAbilitiesBuilder(this.rolesProvider),
    ];

    for await (const abilityBuilder of abilityBuilders) {
      await abilityBuilder.enrich(mainAbilityBuilder);
    }

    const userAbilities = mainAbilityBuilder.build();

    this.userAbilities = userAbilities;
  }

  /**
   * @deprecated Используйте can для лучшей типизации
   */
  async __can<A extends AllRolesAbilities[0], S extends AllRolesAbilities[1]>(
    action: A,
    subjectName: S,
    subjectIdOrHook: string | (() => string | Promise<string>),
  ): Promise<boolean> {
    if (!this.userAbilities) {
      await this.prepareAbilities();
    }

    let subjectId: string;

    if (typeof subjectIdOrHook === 'function') {
      subjectId = await subjectIdOrHook();
    }

    if (typeof subjectIdOrHook === 'string') {
      subjectId = subjectIdOrHook;
    }

    const subjectInfo = { id: subjectId };

    const caslSubject = subject(subjectName as string, subjectInfo);

    if (this.userAbilities.can(action, caslSubject as unknown)) {
      return true;
    }

    throw new ForbiddenError(`Доступ запрещен.`);
  }

  async can(
    ability: AllRolesAbilities,
    subjectIdOrHook:
      | string
      | Record<string, unknown>
      | (() =>
          | string
          | Record<string, unknown>
          | Promise<string>
          | Promise<Record<string, unknown>>),
  ): Promise<boolean> {
    if (!this.userAbilities) {
      await this.prepareAbilities();
    }

    let subjectInfo: Record<string, unknown>;

    if (typeof subjectIdOrHook === 'function') {
      const subjectHookResult = await subjectIdOrHook();

      if (typeof subjectHookResult === 'string') {
        subjectInfo = { id: subjectHookResult };
      }

      if (typeof subjectHookResult === 'object') {
        subjectInfo = subjectHookResult;
      }
    }

    if (typeof subjectIdOrHook === 'string') {
      subjectInfo = { id: subjectIdOrHook };
    }

    if (typeof subjectIdOrHook === 'object') {
      subjectInfo = subjectIdOrHook;
    }

    const caslSubject = subject(ability[1] as string, subjectInfo);

    /**
     * gugabuga
     * хз как тут протипизировать
     */
    if (this.userAbilities.can(ability[0], caslSubject as unknown)) {
      return true;
    }

    throw new ForbiddenError(`Доступ запрещен.`);
  }

  async getBuilder() {
    if (!this.userAbilities) {
      await this.prepareAbilities();
    }

    return new PermissionsQueryBuilder(this.userAbilities);
  }
}
