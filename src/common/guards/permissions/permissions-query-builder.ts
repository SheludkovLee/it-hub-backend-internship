import { subject } from '@casl/ability';
import { ForbiddenError } from '@nestjs/apollo';

import { AllRolesAbilities, AppAbility } from './permissions-checker';

type PermissionBuilderArgs = {
  action: AllRolesAbilities[0];
  subjectName: AllRolesAbilities[1];
  subjectIdOrHook: string | (() => string | Promise<string>);
};

export class PermissionsQueryBuilder {
  constructor(private readonly userAbilities: AppAbility) {}

  private args: PermissionBuilderArgs[] = [];
  private operator: 'or' | 'and' = 'and';

  can<A extends AllRolesAbilities[0], S extends AllRolesAbilities[1]>(
    action: A,
    subjectName: S,
    subjectIdOrHook: string | (() => string | Promise<string>),
  ): PermissionsQueryBuilder {
    this.args.push({ action, subjectName, subjectIdOrHook });

    return this;
  }

  or() {
    this.operator = 'or';

    return this;
  }

  private async performSingleCheck(arg: PermissionBuilderArgs) {
    const { action, subjectIdOrHook, subjectName } = arg;

    let subjectId: string;

    if (typeof subjectIdOrHook === 'function') {
      subjectId = await subjectIdOrHook();
    }

    if (typeof subjectIdOrHook === 'string') {
      subjectId = subjectIdOrHook;
    }

    const subjectInfo = { id: subjectId };

    const caslSubject = subject(subjectName as string, subjectInfo);

    if (this.userAbilities.can(action, caslSubject as unknown)) {
      return true;
    }

    throw new ForbiddenError(`Доступ запрещен.`);
  }

  async check(): Promise<boolean> {
    if (this.args.length === 0) {
      throw new ForbiddenError(`Доступ запрещен.`);
    }

    if (this.args.length === 1) {
      return this.performSingleCheck(this.args[0]);
    }

    if (this.operator === 'and') {
      for await (const arg of this.args) {
        await this.performSingleCheck(arg);
      }
    }

    if (this.operator === 'or') {
      for await (const arg of this.args) {
        try {
          await this.performSingleCheck(arg);

          return true;
        } catch (error) {}
      }

      throw new ForbiddenError(`Доступ запрещен.`);
    }
  }
}
