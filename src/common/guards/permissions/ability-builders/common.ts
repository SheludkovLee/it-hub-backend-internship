export type AppAbilityHelper<
  Action,
  Subject,
  SubjectFields = { id?: string },
> = [Action, Subject | SubjectFields];
