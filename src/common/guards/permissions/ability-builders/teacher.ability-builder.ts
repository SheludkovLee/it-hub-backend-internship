import { AbilityBuilder } from '@casl/ability';
import { ICurrentUserRolesProvider } from 'src/common/types/app-request.type';

import { AppAbility } from '../permissions-checker';
import { AppAbilityHelper } from './common';

export type TeacherAbilities =
  | AppAbilityHelper<'read', 'Discipline'>
  | AppAbilityHelper<'modify', 'Discipline'>
  | AppAbilityHelper<'synchronize', 'Discipline'>
  | AppAbilityHelper<'read', 'Task', { disciplineId?: string }>
  | AppAbilityHelper<'read', 'Class', { disciplineId?: string }>
  | AppAbilityHelper<'read', 'ScoreJournal', { disciplineId?: string }>
  | AppAbilityHelper<'read', 'TemplateDiscipline'>
  | AppAbilityHelper<'modify', 'TemplateDiscipline'>;

export class TeacherAbilitiesBuilder {
  constructor(private readonly rolesProvider: ICurrentUserRolesProvider) {}

  async enrich(builder: AbilityBuilder<AppAbility>) {
    const [teachingDiscplines, retakingDisciplines] = await Promise.all([
      this.rolesProvider.getDisciplinesWhereUserIsTeacher(),
      this.rolesProvider.getDisciplinesWhereTeacherAssignedToRetake(),
    ]);

    const disciplinesWhereUserIsTeacher =
      teachingDiscplines.concat(retakingDisciplines);

    if (disciplinesWhereUserIsTeacher.length !== 0) {
      builder.can('read', 'Discipline', {
        id: { $in: disciplinesWhereUserIsTeacher },
      });

      builder.can('modify', 'Discipline', {
        id: { $in: disciplinesWhereUserIsTeacher },
      });

      builder.can('synchronize', 'Discipline', {
        id: { $in: disciplinesWhereUserIsTeacher },
      });

      builder.can('read', 'Task', {
        disciplineId: { $in: disciplinesWhereUserIsTeacher },
      });

      builder.can('read', 'Class', {
        disciplineId: { $in: disciplinesWhereUserIsTeacher },
      });

      builder.can('read', 'ScoreJournal', {
        disciplineId: { $in: disciplinesWhereUserIsTeacher },
      });
    }

    const [
      templateDisciplinesWhereUserIsTeacher,
      templateDisciplinesWhereUsersIsAttached,
    ] = await Promise.all([
      this.rolesProvider.getTemplateDisciplinesWhereUserIsDisciplineTeacher(),
      this.rolesProvider.getTemplateDisciplinesWhereTeacherIsAttachedTeacher(),
    ]);

    const availableTemplates = [
      ...templateDisciplinesWhereUserIsTeacher,
      ...templateDisciplinesWhereUsersIsAttached,
    ];

    if (availableTemplates.length !== 0) {
      builder.can('read', 'TemplateDiscipline', {
        id: { $in: availableTemplates },
      });

      builder.can('modify', 'TemplateDiscipline', {
        id: { $in: availableTemplates },
      });
    }
  }
}
