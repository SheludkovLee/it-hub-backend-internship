import { AbilityBuilder } from '@casl/ability';
import { ICurrentUserRolesProvider } from 'src/common/types/app-request.type';

import { AppAbility } from '../permissions-checker';
import { AppAbilityHelper } from './common';

export type SuborganizationAdminAbilities =
  | AppAbilityHelper<'modify_inner_entities', 'Suborganization'>
  | AppAbilityHelper<'read_inner_entities', 'Suborganization'>
  | AppAbilityHelper<'read', 'Suborganization'>
  | AppAbilityHelper<'read_users', 'Suborganization'>
  | AppAbilityHelper<'read_learning_paths', 'Suborganization'>
  | AppAbilityHelper<'modify_template_disciplines', 'Suborganization'>
  | AppAbilityHelper<'delete_template_disciplines', 'Suborganization'>
  | AppAbilityHelper<'delete_template_sections', 'Suborganization'>
  | AppAbilityHelper<'delete', 'Suborganization'>
  | AppAbilityHelper<'modify_flow', 'Organization'>;

export class SuborganizationAdminAbilitiesBuilder {
  constructor(private readonly rolesProvider: ICurrentUserRolesProvider) {}

  async enrich(builder: AbilityBuilder<AppAbility>) {
    const organizationsIdsWhereUserIsSuborganizationAdmin =
      await this.rolesProvider.getOrganizationsIdsWhereUserIsSuborganizationAdmin();

    if (organizationsIdsWhereUserIsSuborganizationAdmin.length > 0) {
      builder.can('read', 'Organization', {
        id: {
          $in: organizationsIdsWhereUserIsSuborganizationAdmin,
        },
      });

      builder.can('read_users', 'Organization', {
        id: {
          $in: organizationsIdsWhereUserIsSuborganizationAdmin,
        },
      });

      builder.can('read_learning_paths', 'Organization', {
        id: {
          $in: organizationsIdsWhereUserIsSuborganizationAdmin,
        },
      });

      builder.can('synchronize_inner_entities', 'Organization', {
        id: {
          $in: organizationsIdsWhereUserIsSuborganizationAdmin,
        },
      });

      builder.can('read_template_disciplines', 'Organization', {
        id: {
          $in: organizationsIdsWhereUserIsSuborganizationAdmin,
        },
      });
      builder.can('modify_discipline', 'Organization', {
        id: {
          $in: organizationsIdsWhereUserIsSuborganizationAdmin,
        },
      });

      builder.can('modify_learning_group', 'Organization', {
        id: {
          $in: organizationsIdsWhereUserIsSuborganizationAdmin,
        },
      });

      builder.can('modify_flow', 'Organization', {
        id: {
          $in: organizationsIdsWhereUserIsSuborganizationAdmin,
        },
      });
    }

    const suborganizationsWhereUserIsAdmin =
      await this.rolesProvider.getSuborganizationIdsWhereUserIsAdmin();

    if (suborganizationsWhereUserIsAdmin.length !== 0) {
      builder.can('delete', 'Suborganization', {
        id: {
          $in: suborganizationsWhereUserIsAdmin,
        },
      });

      builder.can('modify_inner_entities', 'Suborganization', {
        id: { $in: suborganizationsWhereUserIsAdmin },
      });

      builder.can('read_inner_entities', 'Suborganization', {
        id: { $in: suborganizationsWhereUserIsAdmin },
      });

      builder.can('read', 'Suborganization', {
        id: { $in: suborganizationsWhereUserIsAdmin },
      });

      builder.can('read_users', 'Suborganization', {
        id: {
          $in: suborganizationsWhereUserIsAdmin,
        },
      });

      builder.can('read_learning_paths', 'Suborganization', {
        id: {
          $in: suborganizationsWhereUserIsAdmin,
        },
      });

      builder.can('modify_template_disciplines', 'Suborganization', {
        id: {
          $in: suborganizationsWhereUserIsAdmin,
        },
      });

      builder.cannot('delete_template_disciplines', 'Suborganization', {
        id: {
          $in: suborganizationsWhereUserIsAdmin,
        },
      });

      builder.cannot('delete_template_sections', 'Suborganization', {
        id: {
          $in: suborganizationsWhereUserIsAdmin,
        },
      });
    }
  }
}
