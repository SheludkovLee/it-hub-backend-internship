import { AbilityBuilder } from '@casl/ability';
import { ICurrentUserRolesProvider } from 'src/common/types/app-request.type';

import { AppAbility } from '../permissions-checker';
import { AppAbilityHelper } from './common';

export type OrganizationAdminAbilities =
  | AppAbilityHelper<'modify_inner_entities', 'Organization'>
  | AppAbilityHelper<'modify_learning_group', 'Organization'>
  | AppAbilityHelper<'read_inner_entities', 'Organization'>
  | AppAbilityHelper<'read_template_disciplines', 'Organization'>
  | AppAbilityHelper<'read', 'Organization'>
  | AppAbilityHelper<'read_users', 'Organization'>
  | AppAbilityHelper<'read_learning_paths', 'Organization'>
  | AppAbilityHelper<'modify_discipline', 'Organization'>
  | AppAbilityHelper<'synchronize_inner_entities', 'Organization'>
  | AppAbilityHelper<'delete', 'Organization'>
  | AppAbilityHelper<'manage', 'Suborganization'>
  | AppAbilityHelper<'modify_flow', 'Organization'>;

export class OrganizationAdminAbilitiesBuilder {
  constructor(private readonly rolesProvider: ICurrentUserRolesProvider) {}

  async enrich(builder: AbilityBuilder<AppAbility>) {
    const organizationsWhereUserIsAdmin =
      await this.rolesProvider.getOrganizationIdsWhereUserIsAdmin();

    if (organizationsWhereUserIsAdmin.length > 0) {
      builder.can('modify_inner_entities', 'Organization', {
        id: {
          $in: organizationsWhereUserIsAdmin,
        },
      });

      builder.can('read_inner_entities', 'Organization', {
        id: {
          $in: organizationsWhereUserIsAdmin,
        },
      });

      builder.can('read', 'Organization', {
        id: {
          $in: organizationsWhereUserIsAdmin,
        },
      });

      builder.can('read_users', 'Organization', {
        id: {
          $in: organizationsWhereUserIsAdmin,
        },
      });

      builder.can('read_learning_paths', 'Organization', {
        id: {
          $in: organizationsWhereUserIsAdmin,
        },
      });

      builder.can('synchronize_inner_entities', 'Organization', {
        id: {
          $in: organizationsWhereUserIsAdmin,
        },
      });

      builder.can('read_template_disciplines', 'Organization', {
        id: {
          $in: organizationsWhereUserIsAdmin,
        },
      });

      builder.can('modify_discipline', 'Organization', {
        id: {
          $in: organizationsWhereUserIsAdmin,
        },
      });

      builder.can('modify_learning_group', 'Organization', {
        id: {
          $in: organizationsWhereUserIsAdmin,
        },
      });

      builder.can('modify_flow', 'Organization', {
        id: {
          $in: organizationsWhereUserIsAdmin,
        },
      });

      /**
       * Думаю не стоит давать админам удалять свои франшизы
       */
      // builder.can('delete', 'Organization', {
      //   id: {
      //     $in: organizationsWhereUserIsAdmin,
      //   },
      // });
    }
    const suborganizationsWhereUserIsOrganizationAdmin =
      await this.rolesProvider.getOrganizationsSuborganizationIdsWhereUserIsAdmin();

    if (suborganizationsWhereUserIsOrganizationAdmin.length !== 0) {
      builder.can('manage', 'Suborganization', {
        id: {
          $in: suborganizationsWhereUserIsOrganizationAdmin,
        },
      });

      builder.cannot('delete_template_disciplines', 'Suborganization', {
        id: {
          $in: suborganizationsWhereUserIsOrganizationAdmin,
        },
      });

      builder.cannot('delete_template_sections', 'Suborganization', {
        id: {
          $in: suborganizationsWhereUserIsOrganizationAdmin,
        },
      });
    }
  }
}
