import { AbilityBuilder } from '@casl/ability';
import { ICurrentUserRolesProvider } from 'src/common/types/app-request.type';

import { AppAbility } from '../permissions-checker';
import { AppAbilityHelper } from './common';

export type SuperAdminAbilities = AppAbilityHelper<'manage', 'all'>;

export class SuperAdminAbilitiesBuilder {
  constructor(private readonly rolesProvider: ICurrentUserRolesProvider) {}

  async enrich(builder: AbilityBuilder<AppAbility>) {
    if (await this.rolesProvider.isSuperAdmin()) {
      builder.can('manage', 'all');

      return;
    }
  }
}
