import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType('Interval')
export class IntervalObjectType {
  @Field(() => Date)
  from: string;

  @Field(() => Date)
  to: string;
}
