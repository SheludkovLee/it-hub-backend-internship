import { FastifyRequest } from 'fastify';

import { CurrentRequestUser } from '../decorators';
import { PermissionsChecker } from '../guards/permissions/permissions-checker';
import { AppRoles } from './app-roles.type';

export interface ICurrentUserRolesProvider {
  getRoles(): Promise<AppRoles[]>;

  isSuperAdmin(): Promise<boolean>;

  isAdminInAnyOrganization(): Promise<boolean>;

  isAdminInOrganization(organizationId: string): Promise<boolean>;

  getOrganizationIdsWhereUserIsAdmin(): Promise<string[]>;

  isAdminInAnySuborganization(): Promise<boolean>;

  getOrganizationsIdsWhereUserIsSuborganizationAdmin(): Promise<string[]>;

  getOrganizationsSuborganizationIdsWhereUserIsAdmin(): Promise<string[]>;

  getSuborganizationIdsWhereUserIsAdmin(): Promise<string[]>;

  getSuborganizationsOrganizationIdsWhereUserIsAdmin(): Promise<string[]>;

  getSuborganizationsIdsWhereUserSuborganizationAdminOrOrganizationAdmin(): Promise<
    string[]
  >;

  isOnlyStudent(): Promise<boolean>;

  isOnlyParent(): Promise<boolean>;

  isStudentInAnyOrganization(): Promise<boolean>;

  isTeacherInAnyOrganization(): Promise<boolean>;

  isParentInAnyOrganization(): Promise<boolean>;

  getDisciplinesWhereTeacherAssignedToRetake(): Promise<string[]>;

  getDisciplinesWhereUserIsTeacher(): Promise<string[]>;

  getTemplateDisciplinesWhereUserIsDisciplineTeacher(): Promise<string[]>;

  getTemplateDisciplinesWhereTeacherIsAttachedTeacher(): Promise<string[]>;

  getSuborganizationsWhereUserIsTeacher(): Promise<string[]>;
}

export interface AppRequest extends FastifyRequest {
  user: CurrentRequestUser;
  rolesProvider: ICurrentUserRolesProvider;
  permissionsChecker: PermissionsChecker;
}
