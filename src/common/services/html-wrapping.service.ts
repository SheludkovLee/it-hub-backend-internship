type HtmlTag = string;
type HtmlClass = string;

export class HtmlWrappingService {
  public static wrapInTag(
    content: string,
    tag: HtmlTag,
    htmlClassName?: HtmlClass,
  ): string {
    const htmlClass = htmlClassName ? ` class="${htmlClassName}"` : '';
    return `<${tag}${htmlClass}>` + content + `</${tag}>`;
  }
}
