import { Parser, ParserOptions } from '@json2csv/plainjs';
import { ArgumentInvalidException } from 'src/libs/exceptions';

import { CsvReportRow, ICsvBuilder, ReportOptions } from '../csv-builder.types';

export class CsvBuilderAdapter implements ICsvBuilder {
  protected reportContent: Array<CsvReportRow>;
  protected parserOptions: ParserOptions;

  build(options: ReportOptions): CsvBuilderAdapter {
    if (!options.rows.length) {
      throw new ArgumentInvalidException('Недостаточно данных для отчёта');
    }
    if (options.header.length > Object.keys(options.rows[0]).length) {
      throw new ArgumentInvalidException(
        'Кол-во полей в объекте не соответствует заголовку',
      );
    }
    this.reportContent = options.rows.map((row) => {
      const rowValues = Object.values(row);
      const csvRow = {};

      for (const [index, columnName] of options.header.entries()) {
        csvRow[columnName] = rowValues[index];
      }

      return csvRow;
    });

    return this;
  }

  useColumnModifiers(options: ParserOptions): CsvBuilderAdapter {
    this.parserOptions = options;

    return this;
  }

  execute(): string {
    const parser = new Parser(this.parserOptions);

    return parser.parse(this.reportContent);
  }
}
