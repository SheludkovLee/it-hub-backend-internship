export type CsvReportRow = Record<string, any> | string | number;
export type File = string;

export interface ReportOptions {
  header: Array<string>;
  rows: Array<CsvReportRow>;
}

export interface ICsvBuilder {
  build(options: ReportOptions): ICsvBuilder;
  execute(): File;
}
