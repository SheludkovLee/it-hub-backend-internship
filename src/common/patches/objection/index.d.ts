import 'objection/typings/objection/index';

declare module 'objection' {
  export type PatchedModelObject<T extends Model> = {
    [K in DataPropertyNames<T>]?: T[K];
  };
}
