import { Provider } from '@nestjs/common';
import { CommandBus, ICommandBus } from 'src/libs/cqrs';
import {
  MessageInboxRepositoryPort,
  MessageOutboxRepositoryPort,
  UnitOfWorkObjection,
} from 'src/libs/database';

/**
 * Нужен для метода executeMany в commandBus, чтобы стартануть транзакцию
 */
class UowForCommandBus extends UnitOfWorkObjection {
  getMessagesOutboxRepository(trxId?: string): MessageOutboxRepositoryPort {
    throw new Error('Not implemented');
  }

  getMessageInboxRepository(trxId?: string): MessageInboxRepositoryPort {
    throw new Error('Not implemented');
  }
}

export const CommandBusProvider: Provider<ICommandBus> = {
  provide: CommandBus,
  useFactory: () => {
    return new CommandBus(new UowForCommandBus());
  },
};
