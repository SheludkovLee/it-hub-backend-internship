import { Injectable } from '@nestjs/common';
import { AsyncDomainEventHandlerBase, DomainEventBase } from 'src/libs/domain';
import { Bus } from 'src/libs/events';

@Injectable()
export class AsyncEventBus extends Bus<
  DomainEventBase,
  AsyncDomainEventHandlerBase
> {}
