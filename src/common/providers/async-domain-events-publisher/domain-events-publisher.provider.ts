import { Provider } from '@nestjs/common';
import { AsyncDomainEventsPublisher } from 'src/libs/domain';

import { AsyncEventBus } from '../async-event-bus';

export const AsyncDomainEventsPublisherProvider: Provider<AsyncDomainEventsPublisher> =
  {
    provide: AsyncDomainEventsPublisher,
    useFactory: (eventBus: AsyncEventBus) => {
      return new AsyncDomainEventsPublisher(eventBus);
    },
    inject: [AsyncEventBus],
  };
