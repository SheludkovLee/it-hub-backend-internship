import { Provider } from '@nestjs/common';
import { DomainEventsPublisher } from 'src/libs/domain';

import { EventBus } from '../event-bus';

export const DomainEventsPublisherProvider: Provider<DomainEventsPublisher> = {
  provide: DomainEventsPublisher,
  useFactory: (eventBus: EventBus) => {
    return new DomainEventsPublisher(eventBus);
  },
  inject: [EventBus],
};
