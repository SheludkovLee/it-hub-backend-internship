import { Injectable } from '@nestjs/common';
import { DomainEventBase, DomainEventHandlerBase } from 'src/libs/domain';
import { Bus } from 'src/libs/events';

@Injectable()
export class EventBus extends Bus<DomainEventBase, DomainEventHandlerBase> {}
