import { Provider } from '@nestjs/common';
import { IQueryBus, QueryBus } from 'src/libs/cqrs';

export const QueryBusProvider: Provider<IQueryBus> = {
  provide: QueryBus,
  useClass: QueryBus,
};
