import { Injectable } from '@nestjs/common';
import {
  MessageInboxRepositoryPort,
  MessageOutboxRepositoryPort,
  UnitOfWorkObjection,
} from 'src/libs/database';

import {
  MessageInboxRepository,
  MessageOutboxRepository,
  UserObjectionRepository,
} from '../repository';

@Injectable()
export class UnitOfWork extends UnitOfWorkObjection {
  getMessagesOutboxRepository(trxId?: string): MessageOutboxRepositoryPort {
    return new MessageOutboxRepository(this, trxId);
  }

  getMessageInboxRepository(trxId?: string): MessageInboxRepositoryPort {
    return new MessageInboxRepository(this, trxId);
  }

  getUserRepository(trxId?: string): UserObjectionRepository {
    return new UserObjectionRepository(this, trxId);
  }
}
