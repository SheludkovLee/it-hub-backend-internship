import { buildTableName } from 'database/utils';
import { PatchedModelObject } from 'objection';
import { ObjectionOrmEntityBase } from 'src/libs/database';

export class UserObjectionOrmEntity extends ObjectionOrmEntityBase {
  static tableName = buildTableName('identity', 'users');

  isSuperAdmin: boolean;
  email: string;
  firstName?: string;
  middleName?: string;
  lastName?: string;
  phoneNumber?: string;
  avatar?: string;
  description?: string;
  encryptedPassword?: string;

  static create(
    props: PatchedModelObject<UserObjectionOrmEntity>,
  ): UserObjectionOrmEntity {
    return this.fromJson(props);
  }
}
