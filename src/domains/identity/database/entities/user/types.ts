export interface ResetPasswordJson {
  createdAt: string;
  token: string;
}
