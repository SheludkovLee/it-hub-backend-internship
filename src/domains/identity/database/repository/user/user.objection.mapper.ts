import { PasswordVO } from 'src/domains/identity/domain/value-objects/password/password.value-object';
import { Mapper } from 'src/libs/database';
import { DateVO, UUID } from 'src/libs/domain';

import { UserEntity } from '../../../domain';
import { UserObjectionOrmEntity } from '../../entities';

export class UserMapper implements Mapper<UserEntity, UserObjectionOrmEntity> {
  toDomain(entity: UserObjectionOrmEntity): UserEntity {
    return new UserEntity({
      id: new UUID(entity.id),
      props: {
        isSuperAdmin: entity.isSuperAdmin,
        email: entity.email,
        firstName: entity.firstName,
        middleName: entity.middleName,
        lastName: entity.lastName,
        phoneNumber: entity.phoneNumber,
        avatar: entity.avatar,
        description: entity.description,
        password:
          entity.encryptedPassword &&
          new PasswordVO({ encryptedPassword: entity.encryptedPassword }),
      },
      updatedAt: new DateVO(entity.updatedAt),
      createdAt: new DateVO(entity.createdAt),
    });
  }

  toOrm(entity: UserEntity): UserObjectionOrmEntity {
    const props = entity.getPropsCopy();

    return UserObjectionOrmEntity.create({
      id: props.id.value,
      updatedAt: props.updatedAt.toISOString(),
      createdAt: props.createdAt.toISOString(),
      isSuperAdmin: props.isSuperAdmin,
      email: props.email,
      firstName: props.firstName,
      middleName: props.middleName,
      lastName: props.lastName,
      phoneNumber: props.phoneNumber,
      avatar: props.avatar,
      description: props.description,
      encryptedPassword: props.password?.encryptedPassword ?? null,
    });
  }
}
