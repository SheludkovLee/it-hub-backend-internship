import { raw } from 'objection';
import { ObjectionAggregateRepositoryBase } from 'src/libs/database';
import { ExceptionBase, NotFoundException } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { UserEntity } from '../../../domain';
import { UserObjectionOrmEntity } from '../../entities';
import { UserMapper } from './user.objection.mapper';

export class UserObjectionRepository extends ObjectionAggregateRepositoryBase<
  UserEntity,
  UserObjectionOrmEntity
> {
  protected model = UserObjectionOrmEntity;
  protected mapper = new UserMapper();

  protected get notFoundMessage(): string {
    return 'Пользователь не найден';
  }

  protected get useSoftDelete(): boolean {
    return true;
  }

  async loadByEmail(email: string): Promise<Result<UserEntity, ExceptionBase>> {
    try {
      const result = await this.getQb()
        .findOne(raw(`lower(email)`), email.toLowerCase())
        .whereNull('deletedAt');

      if (!result) {
        return Result.fail(new NotFoundException(this.notFoundMessage));
      }

      const entity = this.mapper.toDomain(result);

      return Result.ok(entity);
    } catch (e) {
      return Result.fail(e);
    }
  }
}
