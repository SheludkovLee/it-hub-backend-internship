import { Module } from '@nestjs/common';
import { CommunicationModule } from 'src/infra/communication/communication.module';
import { AppJwtModule } from 'src/libs/jwt/jwt.module';

import { CommandHandlersProviders } from './commands';
import { UnitOfWork } from './database';
import { EventHandlersProviders } from './event-handlers/user';
import { eventMapperProviders } from './event-mappers';

@Module({
  imports: [
    CommunicationModule.init({
      boundedContext: 'identity',
      outboxTableName: 'identity_message_outbox',
      outboxChanelName: 'identity_message_outbox',
    }),
    AppJwtModule,
  ],
  providers: [
    ...CommandHandlersProviders,
    ...EventHandlersProviders,
    ...eventMapperProviders,
    UnitOfWork,
  ],
})
export class IdentityModule {}
