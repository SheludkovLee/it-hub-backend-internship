import { AggregateRoot, CreateEntityProps, ID } from 'src/libs/domain';
import {
  ConflictException,
  InvalidOperationException,
} from 'src/libs/exceptions';

import { ResetPasswordVO } from '../..';
import { PasswordVO } from '../../value-objects';
import { CreateUserProps, UpdateUserProps } from './types';

export interface UserProps {
  isSuperAdmin: boolean;
  email: string;
  firstName?: string;
  lastName?: string;
  middleName?: string;
  phoneNumber?: string;
  avatar?: string;
  description?: string;
  resetPassword?: ResetPasswordVO;
  registrationToken?: string;
  password?: PasswordVO;
  isDepersonalized: boolean;
}

export class UserEntity extends AggregateRoot<UserProps> {
  constructor(props: CreateEntityProps<CreateUserProps>) {
    const { props: entityProps, ...restProps } = props;

    const defaultProps = {
      isDepersonalized: false,
      isSuperAdmin: false,
    };

    super({ ...restProps, props: { ...defaultProps, ...entityProps } });
  }

  protected _id: ID;

  public get id(): ID {
    return this._id;
  }

  public set id(id: ID) {
    this._id = id;
  }

  public get email(): string {
    return this.props.email;
  }

  public get firstName() {
    return this.props.firstName;
  }

  public get isDepersonalized(): boolean {
    return this.props.isDepersonalized;
  }

  static create(props: CreateEntityProps<CreateUserProps>): UserEntity {
    const entity = new UserEntity(props);

    return entity;
  }

  public update(
    props: UpdateUserProps,
    options?: {
      isEmailExists?: boolean;
      isStudentOrParent?: boolean;
    },
  ): void {
    if (this.isDepersonalized) {
      throw new InvalidOperationException(
        `Нельзя изменить данные деперсонализированного пользователю`,
      );
    }

    if (options?.isStudentOrParent) {
      this.props.avatar = props.avatar;
      this.props.phoneNumber = props.phoneNumber;

      this.updatedAtNow();

      return;
    }

    const isNewEmailPassed =
      props.email &&
      props.email?.toLowerCase() !== this.props.email.toLowerCase();
    if (isNewEmailPassed && options?.isEmailExists) {
      throw new ConflictException(
        'Такой e-mail уже занят другим пользователем',
      );
    }
    this.props.lastName = props.lastName ?? this.props.lastName;
    this.props.firstName = props.firstName ?? this.props.firstName;
    this.props.email = props.email ?? this.props.email;

    this.props.middleName = props.middleName;
    this.props.avatar = props.avatar;
    this.props.phoneNumber = props.phoneNumber;
    this.props.description = props.description;

    this.updatedAtNow();
  }

  public delete(): void {
    this.deletedAtNow();
  }

  validate(): void {
    return;
  }
}
