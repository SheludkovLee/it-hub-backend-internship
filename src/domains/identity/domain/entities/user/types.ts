import { PartialField } from 'src/common/utils';

import { UserProps } from './user.entity';

export type UpdateUserProps = Partial<UserProps>;

export type CreateUserProps = PartialField<
  UserProps,
  'isDepersonalized' | 'isSuperAdmin'
>;

export type SetNewPasswordProps = {
  resetPasswordToken: string;
  passwordDecrypted: string;
};

export enum RoleType {
  SUPER_ADMIN = 'SUPER_ADMIN',
  ORGANIZATION_ADMIN = 'ORGANIZATION_ADMIN',
  SUBORGANIZATION_ADMIN = 'SUBORGANIZATION_ADMIN',
  TEACHER = 'TEACHER',
  STUDENT = 'STUDENT',
  STUDENT_PARENT = 'STUDENT_PARENT',
}
