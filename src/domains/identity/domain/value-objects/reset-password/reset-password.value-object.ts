import { isUUID } from 'class-validator';
import { DateVO, UUID, ValueObject } from 'src/libs/domain';
import { ArgumentInvalidException } from 'src/libs/exceptions';

export interface ResetPasswordVOProps {
  createdAt: DateVO;
  token: string;
}

export class ResetPasswordVO extends ValueObject<ResetPasswordVOProps> {
  protected validate(props: ResetPasswordVOProps): void {
    const isUuidToken = isUUID(props.token);

    if (!isUuidToken) {
      throw new ArgumentInvalidException(
        `Reset password contains invalid token`,
      );
    }
  }

  static generate(): ResetPasswordVO {
    const hash = UUID.generate();

    return new ResetPasswordVO({ token: hash.value, createdAt: DateVO.now() });
  }

  public get value() {
    return { createdAt: this.props.createdAt.toISOString(), token: this.token };
  }

  public get token(): string {
    return this.props.token;
  }

  public get createdAt(): DateVO {
    return this.props.createdAt;
  }

  public equals(vo: ResetPasswordVO): boolean {
    return vo.token === this.token;
  }
}
