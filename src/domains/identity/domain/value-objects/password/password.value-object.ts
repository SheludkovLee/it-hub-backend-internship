import { hashSync } from 'bcrypt';
import { ValueObject } from 'src/libs/domain';
import { ArgumentInvalidException } from 'src/libs/exceptions';

interface PasswordVOProps {
  encryptedPassword: string;
}

export class PasswordVO extends ValueObject<PasswordVOProps> {
  protected validate(props: PasswordVOProps): void {
    return;
  }

  public get encryptedPassword(): string {
    return this.props.encryptedPassword;
  }

  static fromDecrypted(decryptedPassword: string): PasswordVO {
    this.validateDecryptedPassword(decryptedPassword);

    const encryptedPassword = hashSync(decryptedPassword, 5);

    return new PasswordVO({ encryptedPassword });
  }

  private static validateDecryptedPassword(decryptedPassword: string): void {
    if (decryptedPassword.length < 8) {
      throw new ArgumentInvalidException(
        'Пароль должен быть длиннее 8 символов',
      );
    }

    if (decryptedPassword.length > 32) {
      throw new ArgumentInvalidException(
        'Пароль не может быть длиннее 32 символов',
      );
    }
  }
}
