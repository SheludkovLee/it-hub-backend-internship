import { UserCommandHandlersProviders } from './user';

export * from './user';

export const CommandHandlersProviders = [...UserCommandHandlersProviders];
