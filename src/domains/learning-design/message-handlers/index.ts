import { Provider } from '@nestjs/common';
import { UnitOfWork } from 'src/domains/learning-design/database';
import { MessageBus } from 'src/infra/communication/message-bus';
import { CommandBus } from 'src/libs/cqrs';

const messageHandlers = [];

export const messageHandlerProviders = messageHandlers.map(
  (messageHandlerClass): Provider => {
    return {
      provide: messageHandlerClass,
      useFactory: (
        commandBus: CommandBus,
        messageBus: MessageBus,
        unitOfWork: UnitOfWork,
      ) => {
        return new messageHandlerClass(
          commandBus,
          messageBus,
          unitOfWork,
          'learning_design',
        );
      },
      inject: [CommandBus, MessageBus, UnitOfWork],
    };
  },
);
