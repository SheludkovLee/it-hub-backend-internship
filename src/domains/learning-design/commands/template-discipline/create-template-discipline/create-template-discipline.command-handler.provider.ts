import { Provider } from '@nestjs/common';
import { CommandBus } from 'src/libs/cqrs';
import {
  AsyncDomainEventsPublisher,
  DomainEventsPublisher,
} from 'src/libs/domain';

import { UnitOfWork } from '../../../database';
import { CreateTemplateDisciplineCommandHandler } from './create-template-discipline.command-handler';

export const CreateTemplateDisciplineCommandHandlerProvider: Provider<CreateTemplateDisciplineCommandHandler> =
  {
    provide: CreateTemplateDisciplineCommandHandler,
    useFactory: (
      unitOfWork: UnitOfWork,
      domainEventsPublisher: DomainEventsPublisher,
      asyncDomainEventsPublisher: AsyncDomainEventsPublisher,
      commandBus: CommandBus,
    ) => {
      return new CreateTemplateDisciplineCommandHandler(
        unitOfWork,
        domainEventsPublisher,
        asyncDomainEventsPublisher,
        commandBus,
      );
    },
    inject: [
      UnitOfWork,
      DomainEventsPublisher,
      AsyncDomainEventsPublisher,
      CommandBus,
    ],
  };
