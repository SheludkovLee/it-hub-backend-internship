import {CommandHandler, CommandHandlerBase} from 'src/libs/cqrs';
import {ExceptionBase} from 'src/libs/exceptions';
import {Result} from 'src/libs/utils';

import {UnitOfWork} from '../../../database';
import {TemplateDisciplineEntity} from '../../../domain';
import {CreateTemplateDisciplineCommand} from './create-template-discipline.command';

@CommandHandler(CreateTemplateDisciplineCommand)
export class CreateTemplateDisciplineCommandHandler extends CommandHandlerBase<
    UnitOfWork,
    CreateTemplateDisciplineCommand,
    string
> {
    async handle(
        command: CreateTemplateDisciplineCommand,
    ): Promise<Result<string, ExceptionBase>> {
        try {
            const {payload, trxId} = command;

            const templateDiscipline = TemplateDisciplineEntity.create(payload);

            const templateDisciplineRepository =
                this.unitOfWork.getTemplateDisciplineRepository(trxId);

            await templateDisciplineRepository
                .insert(templateDiscipline)
                .then((res) => res.unwrap());

            return Result.ok(templateDiscipline.id.value);
        } catch (e) {
            return Result.fail(e);
        }
    }
}
