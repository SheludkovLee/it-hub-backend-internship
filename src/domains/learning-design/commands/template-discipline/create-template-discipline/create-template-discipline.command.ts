import { Command, CommandBase } from 'src/libs/cqrs';

interface Payload {
  id?: string;
  name: string;
  description?: string;
  studyHoursCount: number;
  maxScore: number;
  code?: string;
}

@Command('learning_design_create_template_discipline')
export class CreateTemplateDisciplineCommand extends CommandBase<Payload> {}
