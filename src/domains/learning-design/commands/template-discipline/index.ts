import {CreateTemplateDisciplineCommandHandlerProvider} from './create-template-discipline';
import {DeleteTemplateDisciplineCommandHandlerProvider} from './delete-template-discipline';
import {EditTemplateDisciplineByIdCommandHandlerProvider} from './edit-template-discipline-by-id';

export * from './create-template-discipline';
export * from './delete-template-discipline';
export * from './edit-template-discipline-by-id';

export const TemplateDisciplineCommandHandlersProviders = [
    CreateTemplateDisciplineCommandHandlerProvider,
    EditTemplateDisciplineByIdCommandHandlerProvider,
    DeleteTemplateDisciplineCommandHandlerProvider,
];
