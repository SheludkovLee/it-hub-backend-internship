import { Provider } from '@nestjs/common';
import { CommandBus } from 'src/libs/cqrs';
import {
  AsyncDomainEventsPublisher,
  DomainEventsPublisher,
} from 'src/libs/domain';

import { UnitOfWork } from '../../../database';
import { DeleteTemplateDisciplineCommandHandler } from './delete-template-discipline.command-handler';

export const DeleteTemplateDisciplineCommandHandlerProvider: Provider<DeleteTemplateDisciplineCommandHandler> =
  {
    provide: DeleteTemplateDisciplineCommandHandler,
    useFactory: (
      unitOfWork: UnitOfWork,
      domainEventsPublisher: DomainEventsPublisher,
      asyncDomainEventsPublisher: AsyncDomainEventsPublisher,
      commandBus: CommandBus,
    ) => {
      return new DeleteTemplateDisciplineCommandHandler(
        unitOfWork,
        domainEventsPublisher,
        asyncDomainEventsPublisher,
        commandBus,
      );
    },
    inject: [
      UnitOfWork,
      DomainEventsPublisher,
      AsyncDomainEventsPublisher,
      CommandBus,
    ],
  };
