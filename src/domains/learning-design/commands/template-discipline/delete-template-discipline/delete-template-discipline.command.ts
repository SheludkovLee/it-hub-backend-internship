import { Command, CommandBase } from 'src/libs/cqrs';

interface Payload {
  templateDisciplineId: string;
}

@Command('learning_design_delete_template_discipline')
export class DeleteTemplateDisciplineCommand extends CommandBase<Payload> {}
