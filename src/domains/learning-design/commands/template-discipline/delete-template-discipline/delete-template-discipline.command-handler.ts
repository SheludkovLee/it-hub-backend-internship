import { CommandHandler, CommandHandlerBase } from 'src/libs/cqrs';
import { UUID } from 'src/libs/domain';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { UnitOfWork } from '../../../database';
import { DeleteTemplateDisciplineCommand } from './delete-template-discipline.command';

@CommandHandler(DeleteTemplateDisciplineCommand)
export class DeleteTemplateDisciplineCommandHandler extends CommandHandlerBase<
  UnitOfWork,
  DeleteTemplateDisciplineCommand,
  void
> {
  async handle(
    command: DeleteTemplateDisciplineCommand,
  ): Promise<Result<void, ExceptionBase>> {
    try {
      const { payload, trxId } = command;

      const templateDisciplineRepository =
        this.unitOfWork.getTemplateDisciplineRepository(trxId);

      const loadTemplateDisciplineResult =
        await templateDisciplineRepository.loadById(
          new UUID(payload.templateDisciplineId),
        );

      const templateDiscipline = loadTemplateDisciplineResult.unwrap();

      templateDiscipline.delete();

      templateDisciplineRepository
        .delete(templateDiscipline)
        .then((res) => res.unwrap());

      return Result.ok();
    } catch (e) {
      return Result.fail(e);
    }
  }
}
