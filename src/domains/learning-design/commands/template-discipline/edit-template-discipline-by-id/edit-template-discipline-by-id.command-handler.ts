import { CommandHandler, CommandHandlerBase } from 'src/libs/cqrs';
import { UUID } from 'src/libs/domain';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { UnitOfWork } from '../../../database';
import { EditTemplateDisciplineByIdCommand } from './edit-template-discipline-by-id.command';

@CommandHandler(EditTemplateDisciplineByIdCommand)
export class EditTemplateDisciplineByIdCommandHandler extends CommandHandlerBase<
  UnitOfWork,
  EditTemplateDisciplineByIdCommand,
  string
> {
  async handle(
    command: EditTemplateDisciplineByIdCommand,
  ): Promise<Result<string, ExceptionBase>> {
    try {
      const { payload, trxId } = command;

      const templateDisciplineRepository =
        this.unitOfWork.getTemplateDisciplineRepository(trxId);

      const loadTemplateDisciplineResult =
        await templateDisciplineRepository.loadById(
          new UUID(payload.templateDisciplineId),
        );

      const templateDiscipline = loadTemplateDisciplineResult.unwrap();

      templateDiscipline.update(payload);

      await templateDisciplineRepository
        .update(templateDiscipline)
        .then((res) => res.unwrap());

      return Result.ok(templateDiscipline.id.value);
    } catch (e) {
      return Result.fail(e);
    }
  }
}
