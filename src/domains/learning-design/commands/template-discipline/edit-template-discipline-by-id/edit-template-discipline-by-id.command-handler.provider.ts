import { Provider } from '@nestjs/common';
import { CommandBus } from 'src/libs/cqrs';
import {
  AsyncDomainEventsPublisher,
  DomainEventsPublisher,
} from 'src/libs/domain';

import { UnitOfWork } from '../../../database';
import { EditTemplateDisciplineByIdCommandHandler } from './edit-template-discipline-by-id.command-handler';

export const EditTemplateDisciplineByIdCommandHandlerProvider: Provider<EditTemplateDisciplineByIdCommandHandler> =
  {
    provide: EditTemplateDisciplineByIdCommandHandler,
    useFactory: (
      unitOfWork: UnitOfWork,
      domainEventsPublisher: DomainEventsPublisher,
      asyncDomainEventsPublisher: AsyncDomainEventsPublisher,
      commandBus: CommandBus,
    ) => {
      return new EditTemplateDisciplineByIdCommandHandler(
        unitOfWork,
        domainEventsPublisher,
        asyncDomainEventsPublisher,
        commandBus,
      );
    },
    inject: [
      UnitOfWork,
      DomainEventsPublisher,
      AsyncDomainEventsPublisher,
      CommandBus,
    ],
  };
