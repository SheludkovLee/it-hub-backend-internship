import {Command, CommandBase} from 'src/libs/cqrs';

interface Payload {
    templateDisciplineId: string;
    name?: string;
    description?: string;
    studyHoursCount?: number;
    maxScore?: number;
    code?: string;
    materials?: string;
}

@Command('learning_design_edit_template_discipline_by_id')
export class EditTemplateDisciplineByIdCommand extends CommandBase<Payload> {
}
