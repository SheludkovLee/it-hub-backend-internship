import { TemplateDisciplineCommandHandlersProviders } from './template-discipline';

export * from './template-discipline';

export const CommandHandlersProviders = [
  ...TemplateDisciplineCommandHandlersProviders,
];
