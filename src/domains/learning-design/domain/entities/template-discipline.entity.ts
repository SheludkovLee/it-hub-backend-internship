import {
  AggregateRoot,
  CreateEntityProps,
  DateVO,
  ID,
  UUID,
} from 'src/libs/domain';
import { ArgumentInvalidException } from 'src/libs/exceptions';

import { UpdateTemplateDisciplineProps } from './types';

export interface TemplateDisciplineProps {
  name: string;
  description?: string;
  studyHoursCount: number;
  maxScore: number;
  code?: string;
  materials?: string;
}

interface CreateTemplateDisciplineProps {
  name: string;
  description?: string;
  studyHoursCount: number;
  maxScore: number;
  code?: string;
  materials?: string;
}

export class TemplateDisciplineEntity extends AggregateRoot<TemplateDisciplineProps> {
  constructor(props: CreateEntityProps<CreateTemplateDisciplineProps>) {
    super(props);
  }

  protected _id: ID;

  get id() {
    return this._id;
  }

  get name() {
    return this.props.name;
  }

  get description() {
    return this.props.description;
  }

  get maxScore() {
    return this.props.maxScore;
  }

  get materials() {
    return this.props.materials;
  }

  get studyHoursCount() {
    return this.props.studyHoursCount;
  }

  static create(props: CreateTemplateDisciplineProps) {
    const defaultProps = {
      code: '',
      description: '',
      materials: '',
    };

    const entity = new TemplateDisciplineEntity({
      id: UUID.generate(),
      createdAt: DateVO.now(),
      updatedAt: DateVO.now(),
      props: {
        ...props,
        ...defaultProps,
      },
    });

    // TODO event
    // entity.addEvent(
    //   TemplateDisciplineCreatedEvent.fromTemplateDiscipline(entity),
    // );

    return entity;
  }

  validate() {
    const MAX_STUDY_HOURS_COUNT = 500;
    if (this.props.studyHoursCount > MAX_STUDY_HOURS_COUNT) {
      throw new ArgumentInvalidException(
        'Количество часов изучения для ШД слишком велико',
      );
    }
  }

  update(props: UpdateTemplateDisciplineProps) {
    this.props.name = props.name ?? this.props.name;
    this.props.studyHoursCount =
      props.studyHoursCount ?? this.props.studyHoursCount;
    this.props.maxScore = props.maxScore ?? this.props.maxScore;
    this.props.materials = props.materials ?? this.props.materials;
    this.props.description = props.description ?? this.props.description;
    this.props.code = props.code ?? this.props.code;
    this.validate();
    this.updatedAtNow();

    // TODO event
    // this.addEvent(TemplateDisciplineUpdatedEvent.fromTemplateDiscipline(this));
  }

  delete() {
    this.deletedAtNow();
    // TODO event
    //this.addEvent(TemplateDisciplineDeletedEvent.fromTemplateDiscipline(this));
  }
}
