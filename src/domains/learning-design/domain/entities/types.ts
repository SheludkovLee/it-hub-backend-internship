import { TemplateDisciplineProps } from './template-discipline.entity';

export type UpdateTemplateDisciplineProps = Partial<TemplateDisciplineProps>;
