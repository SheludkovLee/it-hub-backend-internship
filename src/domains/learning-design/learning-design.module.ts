import {Module} from '@nestjs/common';
import {CommunicationModule} from 'src/infra/communication/communication.module';

import {CommandHandlersProviders} from './commands';
import {UnitOfWork} from './database';
import {EventsHandlersProviders} from './event-handlers';
import {eventMapperProviders} from './event-mappers';
import {messageHandlerProviders} from './message-handlers';

@Module({
    imports: [
        CommunicationModule.init({
            boundedContext: 'learning_design',
            outboxChanelName: 'learning_design_message_outbox',
            outboxTableName: 'learning_design_message_outbox',
        }),
    ],
    providers: [
        ...CommandHandlersProviders,
        ...EventsHandlersProviders,
        ...eventMapperProviders,
        ...messageHandlerProviders,
        UnitOfWork,
    ],
})
export class LearningDesignModule {
}
