import { Injectable } from '@nestjs/common';
import {
  MessageInboxRepositoryPort,
  MessageOutboxRepositoryPort,
  UnitOfWorkObjection,
} from 'src/libs/database';

import { MessageInboxRepository, MessageOutboxRepository } from '../repository';
import { TemplateDisciplineRepository } from '../repository/template-discipline';

@Injectable()
export class UnitOfWork extends UnitOfWorkObjection {
  getMessagesOutboxRepository(trxId?: string): MessageOutboxRepositoryPort {
    return new MessageOutboxRepository(this, trxId);
  }

  getMessageInboxRepository(trxId?: string): MessageInboxRepositoryPort {
    return new MessageInboxRepository(this, trxId);
  }

  getTemplateDisciplineRepository(trxId: string) {
    return new TemplateDisciplineRepository(this, trxId);
  }
}
