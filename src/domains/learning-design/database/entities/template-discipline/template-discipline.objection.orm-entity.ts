import { buildTableName } from 'database/utils';
import { PatchedModelObject, RelationMappingsThunk } from 'objection';
import { ObjectionOrmEntityBase } from 'src/libs/database';

export class TemplateDisciplineObjectionOrmEntity extends ObjectionOrmEntityBase {
  static tableName = buildTableName('learning_design', 'template_disciplines');

  name: string;
  description: string;
  studyHoursCount: number;
  maxScore: number;
  code: string;
  materials: string;

  static create(
    props: PatchedModelObject<TemplateDisciplineObjectionOrmEntity>,
  ): TemplateDisciplineObjectionOrmEntity {
    return this.fromJson(props);
  }

  static relationMappings: RelationMappingsThunk = () => {
    return {};
  };
}
