import { Mapper } from 'src/libs/database';
import { DateVO, UUID } from 'src/libs/domain';

import { TemplateDisciplineEntity } from '../../../domain';
import { TemplateDisciplineObjectionOrmEntity } from '../../entities';

export class TemplateDisciplineMapper
  implements
    Mapper<TemplateDisciplineEntity, TemplateDisciplineObjectionOrmEntity>
{
  toDomain(entity: TemplateDisciplineObjectionOrmEntity) {
    return new TemplateDisciplineEntity({
      id: UUID.create(entity.id),
      props: {
        name: entity.name,
        description: entity.description,
        studyHoursCount: entity.studyHoursCount,
        maxScore: entity.maxScore,
        code: entity.code,
        materials: entity.materials,
      },
      updatedAt: DateVO.create(entity.updatedAt),
      createdAt: DateVO.create(entity.createdAt),
    });
  }

  toOrm(entity: TemplateDisciplineEntity) {
    const props = entity.getPropsCopy();

    const orm = TemplateDisciplineObjectionOrmEntity.create({
      id: props.id.value,
      createdAt: props.createdAt.toISOString(),
      updatedAt: props.updatedAt.toISOString(),
      name: props.name,
      description: props.description,
      studyHoursCount: props.studyHoursCount,
      maxScore: props.maxScore,
      code: props.code,
      materials: props.materials,
    });

    return orm;
  }
}
