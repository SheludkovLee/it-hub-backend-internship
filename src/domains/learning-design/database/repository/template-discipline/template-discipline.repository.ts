import {
  ObjectionAggregateRepositoryBase,
  OrmEntityRelations,
} from 'src/libs/database';

import { TemplateDisciplineEntity } from '../../../domain';
import { TemplateDisciplineObjectionOrmEntity } from '../../entities';
import { TemplateDisciplineMapper } from './template-discipline.mapper';

export class TemplateDisciplineRepository extends ObjectionAggregateRepositoryBase<
  TemplateDisciplineEntity,
  TemplateDisciplineObjectionOrmEntity
> {
  protected model = TemplateDisciplineObjectionOrmEntity;
  protected mapper = new TemplateDisciplineMapper();

  protected get notFoundMessage(): string {
    return 'Шаблон дисциплина не найдена';
  }

  protected get relations(): OrmEntityRelations<TemplateDisciplineObjectionOrmEntity> {
    return {};
  }
}
