import { buildTableName } from 'database/utils';
import { MessageOutboxObjectionRepository } from 'src/libs/database';

export class MessageOutboxRepository extends MessageOutboxObjectionRepository {
  protected tableName = buildTableName('learning_design', 'message_outbox');
}
