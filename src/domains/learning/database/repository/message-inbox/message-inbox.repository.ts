import {buildTableName} from 'database/utils';
import {MessageInboxObjectionRepository} from 'src/libs/database/message-inbox';

export class MessageInboxRepository extends MessageInboxObjectionRepository {
    protected tableName = buildTableName('learning', 'message_inbox');
}
