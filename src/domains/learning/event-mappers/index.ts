import { MessageBus } from 'src/infra/communication/message-bus';
import { DomainEventsPublisher } from 'src/libs/domain';
import { createEventMapperProviders } from 'src/libs/messages/nest/create-event-mapper-providers';

import { UnitOfWork } from '../database';

const eventMappers = [];

export const eventMapperProviders = createEventMapperProviders(eventMappers)({
  unitOfWork: UnitOfWork,
  domainEventsPublisher: DomainEventsPublisher,
  messageBus: MessageBus,
});
