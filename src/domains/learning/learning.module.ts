import { Module } from '@nestjs/common';
import { CommunicationModule } from 'src/infra/communication/communication.module';

import { commandHandlerProviders } from './commands';
import { UnitOfWork } from './database';
import { eventHandlerProviders } from './event-handlers';
import { eventMapperProviders } from './event-mappers';
import { messageHandlerProviders } from './message-handlers';

@Module({
  imports: [
    CommunicationModule.init({
      boundedContext: 'learning',
      outboxChanelName: 'learning_message_outbox',
      outboxTableName: 'learning_message_outbox',
    }),
  ],
  providers: [
    ...messageHandlerProviders,
    ...commandHandlerProviders,
    ...eventMapperProviders,
    ...eventHandlerProviders,
    UnitOfWork,
  ],
})
export class LearningModule {}
