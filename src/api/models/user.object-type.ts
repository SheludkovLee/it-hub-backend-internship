import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType('User')
export class UserObjectType {
  @Field(() => ID)
  id: string;

  @Field(() => Date)
  createdAt: string;

  @Field(() => Date)
  updatedAt: string;

  @Field(() => String, { nullable: true, description: 'User avatar URL' })
  avatar?: string;

  @Field(() => String, { nullable: true, description: 'User first name' })
  firstName?: string;

  @Field(() => String, { nullable: true, description: 'User last name' })
  lastName?: string;

  @Field(() => String, { nullable: true, description: 'User middle name' })
  middleName?: string;

  @Field(() => String, { nullable: true, description: 'User phone number' })
  phoneNumber?: string;

  @Field(() => String, { description: 'User email' })
  email: string;

  @Field(() => String, { nullable: true, description: 'User description' })
  description?: string;

  @Field(() => Boolean)
  isSuperAdmin: boolean;
}
