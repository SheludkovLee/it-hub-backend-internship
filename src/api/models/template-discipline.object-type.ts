import {Field, Float, ObjectType} from '@nestjs/graphql';
import {ResolvedField} from 'src/common';
import {UUIDScalar} from 'src/libs/graphql/scalars';

@ObjectType('TemplateDiscipline')
export class TemplateDisciplineObjectType {
    @Field(() => UUIDScalar)
    id: string;

    @Field(() => Date)
    createdAt: string;

    @Field(() => Date)
    updatedAt: string;

    @Field(() => String)
    name: string;

    @Field(() => String, {nullable: true})
    description?: string;

    @Field(() => Float)
    studyHoursCount: number;

    @Field(() => Float)
    maxScore: number;

    @Field(() => String, {nullable: true})
    code?: string;

    @ResolvedField(() => Number)
    disciplinesCount?: number;
}
