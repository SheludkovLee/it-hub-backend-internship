import { BaseQueryService } from 'src/libs/api/base-classes/query-service.base-class';

import { TemplateDisciplineObjectType } from '../models/template-discipline.object-type';

export class TemplateDisciplineQueryService extends BaseQueryService<TemplateDisciplineObjectType> {
  async fetchByIds(ids: string[]): Promise<TemplateDisciplineObjectType[]> {
    const entities = await this.queryModels.learningDesign.templateDiscipline
      .query()
      .findByIds(ids);

    return entities;
  }

  async fetchMany(): Promise<TemplateDisciplineObjectType[]> {
    const entities =
      await this.queryModels.learningDesign.templateDiscipline.query();
    return entities;
  }

  async fetchById(
    id: TemplateDisciplineObjectType['id'],
  ): Promise<TemplateDisciplineObjectType> {
    const entity = await this.queryModels.learningDesign.templateDiscipline
      .query()
      .findById(id);

    return entity;
  }
}
