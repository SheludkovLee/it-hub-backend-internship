import { compare } from 'bcrypt';
import { raw } from 'objection';
import { BaseQueryService } from 'src/libs/api/base-classes/query-service.base-class';
import { ArgumentInvalidException } from 'src/libs/exceptions';
import { JwtService } from 'src/libs/jwt/jwt.service';

import { UserObjectType } from '../models/user.object-type';
import { SignInInput } from '../queries/user/sign-in/sign-in.input-type';

export class UserQueryService extends BaseQueryService<UserObjectType> {
  constructor(private readonly jwtService: JwtService) {
    super();
  }

  async fetchByIds(ids: string[]): Promise<UserObjectType[]> {
    return this.queryModels.identity.user.query().findByIds(ids);
  }

  async fetchMany(): Promise<UserObjectType[]> {
    return this.queryModels.identity.user.query().whereNull('deletedAt');
  }

  async fetchById(id: UserObjectType['id']): Promise<UserObjectType> {
    return this.queryModels.identity.user
      .query()
      .findById(id)
      .whereNull('deletedAt');
  }

  async signIn(input: SignInInput) {
    const invalidCredentialsException = new ArgumentInvalidException(
      'Неправильная почта или пароль',
    );

    const user = await this.queryModels.identity.user
      .query()
      .whereNull('deletedAt')
      .findOne(raw(`lower(email)`), input.email.toLowerCase());

    if (!user) {
      throw invalidCredentialsException;
    }

    if (!user.encryptedPassword) {
      throw invalidCredentialsException;
    }

    const isCorrectPassword = await compare(
      input.password,
      user.encryptedPassword,
    );

    if (!isCorrectPassword) {
      throw invalidCredentialsException;
    }

    const accessToken = await this.jwtService.createUserJwtToken(user.id);

    return { user: user, accessToken };
  }
}
