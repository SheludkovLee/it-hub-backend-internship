import {Injectable, Scope} from '@nestjs/common';
import * as DataLoader from 'dataloader';

import {GraphqlQueryServicesRoot} from '../query-services-root';

@Injectable({scope: Scope.REQUEST})
export class DisciplineDataLoader {
    public countByTemplateDisciplineId = new DataLoader<string, number>(
        async (ids: string[]) => {
            // TODO
            // const loadResult = await this.queryServices
            //   .getDisciplineQueryService()
            //   .fetchCountByTemplateDisciplineIds(ids as string[]);
            //
            // return ids.map((id) =>
            //   loadResult.find((item) => item.templateDisciplineId === id).count,
            // );

            return ids.map(() => 0);
        },
    );

    constructor(private readonly queryServices: GraphqlQueryServicesRoot) {
    }
}
