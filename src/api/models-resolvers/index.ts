import {TemplateDisciplineModelResolver} from './template-discipline.model-resolver';

export const modelsResolversProviders = [TemplateDisciplineModelResolver];
