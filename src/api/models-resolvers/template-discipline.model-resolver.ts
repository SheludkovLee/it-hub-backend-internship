import { ResolveField, Resolver } from '@nestjs/graphql';
import { isNil } from 'lodash';
import { DisciplineDataLoader } from 'src/api/data-loaders/discipline.data-loader';

import { TemplateDisciplineObjectType } from '../models/template-discipline.object-type';

@Resolver(() => TemplateDisciplineObjectType)
export class TemplateDisciplineModelResolver {
  constructor(private readonly disciplineDataLoader: DisciplineDataLoader) {}

  @ResolveField(() => Number)
  async disciplinesCount(parent: TemplateDisciplineObjectType) {
    if (!isNil(parent.disciplinesCount)) {
      return parent.disciplinesCount;
    }

    return this.disciplineDataLoader.countByTemplateDisciplineId.load(
      parent.id,
    );
  }
}
