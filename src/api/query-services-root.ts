import { Injectable } from '@nestjs/common';
import { objectionQueryModels } from 'src/libs/api/base-classes/query-service.base-class';
import { JwtService } from 'src/libs/jwt/jwt.service';

import { TemplateDisciplineQueryService } from './query-services/template-discipline.query-service';
import { UserQueryService } from './query-services/user.query-service';

@Injectable()
export class GraphqlQueryServicesRoot {
  public queryModels = objectionQueryModels;

  constructor(private readonly jwtService: JwtService) {}

  getUserQueryService(): UserQueryService {
    return new UserQueryService(this.jwtService);
  }

  getTemplateDisciplineQueryService(): TemplateDisciplineQueryService {
    return new TemplateDisciplineQueryService();
  }
}
