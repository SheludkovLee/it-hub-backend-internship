import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { CurrentUser } from 'src/common';
import { DeleteTemplateDisciplineCommand } from 'src/domains/learning-design/commands/template-discipline/delete-template-discipline';
import { CommandBus } from 'src/libs/cqrs';
import { VoidScalar } from 'src/libs/graphql/scalars';

import { DeleteTemplateDisciplineInput } from './delete-template-discipline.input';

@Resolver()
export class DeleteTemplateDisciplineResolver {
  constructor(private readonly commandBus: CommandBus) {}

  @Mutation(() => VoidScalar)
  async deleteTemplateDiscipline(
    @CurrentUser('id') userId: string,
    @Args('input') input: DeleteTemplateDisciplineInput,
  ): Promise<VoidScalar> {
    const result = await this.commandBus.execute<void>(
      new DeleteTemplateDisciplineCommand({
        payload: input,
        initiatorUserId: userId,
      }),
    );

    result.unwrap();

    return new VoidScalar();
  }
}
