import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { TemplateDisciplineObjectType } from 'src/api/models/template-discipline.object-type';
import { GraphqlQueryServicesRoot } from 'src/api/query-services-root';
import { CurrentUser } from 'src/common';
import { EditTemplateDisciplineByIdCommand } from 'src/domains/learning-design/commands/template-discipline/edit-template-discipline-by-id';
import { CommandBus } from 'src/libs/cqrs';

import { EditTemplateDisciplineByIdInput } from './edit-template-discipline-by-id.input';

@Resolver()
export class EditTemplateDisciplineByIdResolver {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryServicesRoot: GraphqlQueryServicesRoot,
  ) {}

  @Mutation(() => TemplateDisciplineObjectType)
  async editTemplateDisciplineById(
    @CurrentUser('id') userId: string,
    @Args('input') input: EditTemplateDisciplineByIdInput,
  ): Promise<TemplateDisciplineObjectType> {
    const result = await this.commandBus.execute<string>(
      new EditTemplateDisciplineByIdCommand({
        payload: {
          ...input,
        },
        initiatorUserId: userId,
      }),
    );

    return this.queryServicesRoot
      .getTemplateDisciplineQueryService()
      .fetchById(result.unwrap());
  }
}
