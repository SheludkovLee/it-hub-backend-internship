import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { TemplateDisciplineObjectType } from 'src/api/models/template-discipline.object-type';
import { GraphqlQueryServicesRoot } from 'src/api/query-services-root';
import { CurrentUser } from 'src/common';
import { CreateTemplateDisciplineCommand } from 'src/domains/learning-design/commands';
import { CommandBus } from 'src/libs/cqrs';

import { CreateTemplateDisciplineInput } from './create-template-discipline.input';

@Resolver()
export class CreateTemplateDisciplineResolver {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryServicesRoot: GraphqlQueryServicesRoot,
  ) {}

  @Mutation(() => TemplateDisciplineObjectType)
  async createTemplateDiscipline(
    @CurrentUser('id') userId: string,
    @Args('input') input: CreateTemplateDisciplineInput,
  ): Promise<TemplateDisciplineObjectType> {
    const result = await this.commandBus.execute<string>(
      new CreateTemplateDisciplineCommand({
        payload: input,
        initiatorUserId: userId,
      }),
    );

    return this.queryServicesRoot
      .getTemplateDisciplineQueryService()
      .fetchById(result.unwrap());
  }
}
