import { Field, Float, InputType } from '@nestjs/graphql';
import { IsOptional, Max, MaxLength, Min, MinLength } from 'class-validator';
import { IsNotBlank, TrimString } from 'src/common';

@InputType()
export class CreateTemplateDisciplineInput {
  @Field(() => String)
  @MinLength(1)
  @MaxLength(255)
  @IsNotBlank()
  name: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  @MinLength(1)
  @TrimString()
  description?: string;

  @Field(() => Float)
  @Min(1)
  @Max(999)
  studyHoursCount: number;

  @Field(() => Float)
  @Min(1)
  maxScore: number;

  @Field(() => String, { nullable: true })
  @IsOptional()
  @MinLength(1)
  @MaxLength(255)
  @TrimString()
  code?: string;
}
