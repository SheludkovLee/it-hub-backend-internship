import { templateDisciplineMutations } from './template-discipline';

export const mutationsResolversProviders = [...templateDisciplineMutations];
