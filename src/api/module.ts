import {CacheModule} from '@nestjs/cache-manager';
import {Module} from '@nestjs/common';
import {AppJwtModule} from 'src/libs/jwt/jwt.module';

import {dataLoadersProviders} from './data-loaders';
import {modelsResolversProviders} from './models-resolvers';
import {mutationsResolversProviders} from './mutations';
import {queryResolversProviders} from './queries';
import {GraphqlQueryServicesRoot} from './query-services-root';

@Module({
    imports: [CacheModule.register({}), AppJwtModule],
    providers: [
        GraphqlQueryServicesRoot,
        ...dataLoadersProviders,
        ...modelsResolversProviders,
        ...queryResolversProviders,
        ...mutationsResolversProviders,
    ],
})
export class ApiModule {
}
