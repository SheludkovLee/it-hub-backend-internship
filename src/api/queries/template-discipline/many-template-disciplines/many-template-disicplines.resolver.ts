import { Query, Resolver } from '@nestjs/graphql';
import { TemplateDisciplineObjectType } from 'src/api/models/template-discipline.object-type';
import { GraphqlQueryServicesRoot } from 'src/api/query-services-root';

@Resolver()
export class ManyTemplateDisciplinesResolver {
  constructor(private readonly queryServicesRoot: GraphqlQueryServicesRoot) {}

  @Query(() => [TemplateDisciplineObjectType])
  async manyTemplateDisciplines(): Promise<TemplateDisciplineObjectType[]> {
    return this.queryServicesRoot
      .getTemplateDisciplineQueryService()
      .fetchMany();
  }
}
