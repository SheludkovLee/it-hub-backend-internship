import { Args, Query, Resolver } from '@nestjs/graphql';
import { TemplateDisciplineObjectType } from 'src/api/models/template-discipline.object-type';
import { GraphqlQueryServicesRoot } from 'src/api/query-services-root';

import { TemplateDisciplineByIdInput } from './template-discipline-by-id.input';

@Resolver()
export class TemplateDisciplineByIdResolver {
  constructor(private readonly queryServicesRoot: GraphqlQueryServicesRoot) {}

  @Query(() => TemplateDisciplineObjectType)
  async templateDisciplineById(
    @Args('input') input: TemplateDisciplineByIdInput,
  ): Promise<TemplateDisciplineObjectType> {
    return this.queryServicesRoot
      .getTemplateDisciplineQueryService()
      .fetchById(input.templateDisciplineId);
  }
}
