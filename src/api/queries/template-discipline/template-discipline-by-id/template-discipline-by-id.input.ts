import { Field, InputType } from '@nestjs/graphql';
import { UUIDScalar } from 'src/libs/graphql/scalars';

@InputType()
export class TemplateDisciplineByIdInput {
  @Field(() => UUIDScalar)
  templateDisciplineId: string;
}
