import { Query, Resolver } from '@nestjs/graphql';
import { UserObjectType } from 'src/api/models/user.object-type';
import { CurrentRequestUser, CurrentUser } from 'src/common';

@Resolver()
export class MeResolver {
  @Query(() => UserObjectType)
  async me(@CurrentUser() iam: CurrentRequestUser): Promise<UserObjectType> {
    return iam;
  }
}
