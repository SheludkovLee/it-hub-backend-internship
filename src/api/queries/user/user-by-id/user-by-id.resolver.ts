import { Args, Query, Resolver } from '@nestjs/graphql';
import { UserObjectType } from 'src/api/models/user.object-type';
import { GraphqlQueryServicesRoot } from 'src/api/query-services-root';
import { CurrentRequestUser, CurrentUser } from 'src/common';

import { UserByIdInput } from './user-by-id.input-type';

@Resolver()
export class UserByIdResolver {
  constructor(private readonly queryServicesRoot: GraphqlQueryServicesRoot) {}

  @Query(() => UserObjectType)
  async userById(
    @Args('input') input: UserByIdInput,
    @CurrentUser() iam: CurrentRequestUser,
  ): Promise<UserObjectType> {
    if (input.userId === iam.id) {
      return iam;
    }
    return this.queryServicesRoot.getUserQueryService().fetchById(input.userId);
  }
}
