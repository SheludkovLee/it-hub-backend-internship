import { Field, InputType } from '@nestjs/graphql';
import { UUIDScalar } from 'src/libs/graphql/scalars';

@InputType()
export class UserByIdInput {
  @Field(() => UUIDScalar)
  userId: string;
}
