import { ForbiddenError } from '@nestjs/apollo';
import { Args, Context, Query, Resolver } from '@nestjs/graphql';
import { GraphqlQueryServicesRoot } from 'src/api/query-services-root';
import { Public } from 'src/common';
import { AppRequest } from 'src/common/types/app-request.type';
import { ArgumentInvalidException } from 'src/libs/exceptions';

import { SignInInput } from './sign-in.input-type';
import { SignInPayload } from './sign-in.payload';

@Public()
@Resolver()
export class SignInResolver {
  constructor(private readonly queryServicesRoot: GraphqlQueryServicesRoot) {}

  @Query(() => SignInPayload)
  async signIn(
    @Args('input') input: SignInInput,
    @Context() context: { req: AppRequest },
  ): Promise<SignInPayload> {
    try {
      const result = await this.queryServicesRoot
        .getUserQueryService()
        .signIn(input);

      context.req.user = result.user;

      return new SignInPayload(result);
    } catch (error) {
      if (error instanceof ArgumentInvalidException) {
        throw new ForbiddenError(error.message);
      }

      throw error;
    }
  }
}
