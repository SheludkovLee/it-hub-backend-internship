import { Field, ObjectType } from '@nestjs/graphql';
import { UserObjectType } from 'src/api/models/user.object-type';

@ObjectType()
export class SignInPayload {
  @Field(() => UserObjectType)
  user: UserObjectType;

  @Field(() => String)
  accessToken: string;

  constructor(data: SignInPayload) {
    this.user = data.user;
    this.accessToken = data.accessToken;
  }
}
