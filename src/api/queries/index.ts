import { ManyTemplateDisciplinesResolver } from 'src/api/queries/template-discipline/many-template-disciplines/many-template-disicplines.resolver';
import { TemplateDisciplineByIdResolver } from 'src/api/queries/template-discipline/template-discipline-by-id/template-discipline-by-id.resolver';
import { MeResolver } from 'src/api/queries/user/me/me.resolver';
import { UserByIdResolver } from 'src/api/queries/user/user-by-id/user-by-id.resolver';

export const queryResolversProviders = [
  UserByIdResolver,
  MeResolver,
  ManyTemplateDisciplinesResolver,
  TemplateDisciplineByIdResolver,
];
