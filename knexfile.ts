import * as dotenv from 'dotenv';
import type { Knex } from 'knex';
import { knexSnakeCaseMappers } from 'objection';
import { join } from 'path';

dotenv.config();
const { entries } = Object;

const getConnectionConfig = () => {
  /* eslint-disable @typescript-eslint/no-non-null-assertion */
  const host = process.env.POSTGRES_HOST!;
  const port = process.env.POSTGRES_PORT!;
  const user = process.env.POSTGRES_USER!;
  const password = process.env.POSTGRES_PASSWORD!;
  const database = process.env.POSTGRES_DB_NAME!;
  /* eslint-enable @typescript-eslint/no-non-null-assertion */

  const connectionConfig = { host, port, user, password, database };

  const isUndefined = ([, value]: [string, unknown]) => value === undefined;
  const undefinedVariable = entries(connectionConfig).find(isUndefined);
  if (undefinedVariable) {
    const [variableName] = undefinedVariable;
    const message = `"${variableName}" variable is required`;
    throw new Error(message);
  }

  return connectionConfig;
};

const getMigrationsDirs = () => {
  const baseMigrationsDir = join(__dirname, 'database', 'migrations');
  const migrationsDirs = [
    join(baseMigrationsDir, 'identity'),
    join(baseMigrationsDir, 'learning'),
    join(baseMigrationsDir, 'learning-design'),
  ];
  return [...migrationsDirs, baseMigrationsDir];
};

const connectionConfig = getConnectionConfig();
const migrationsDirs = getMigrationsDirs();

const config: Knex.Config = {
  ...knexSnakeCaseMappers(),
  client: 'pg',
  connection: connectionConfig,
  pool: {
    min: 2,
    max: ['production', 'preproduction'].includes(process.env.APP_ENVIRONMENT)
      ? 200
      : 80,
    acquireTimeoutMillis: 12000000,
  },
  migrations: {
    loadExtensions: ['.ts'],
    tableName: 'migrations',
    directory: migrationsDirs,
    stub: join(__dirname, 'database', 'migration.stub'),
  },
  seeds: {
    directory: join(__dirname, 'database', 'seeds'),
    stub: join(__dirname, 'database', 'seed.stub'),
  },
};

export default config;
