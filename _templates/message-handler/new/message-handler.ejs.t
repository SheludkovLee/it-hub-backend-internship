---
to: src/domains/<%= module %>/message-handlers/<%= entity %>/<%= name %>.message-handler.ts
---
<%
 Message = 'I' + h.changeCase.pascal(name) + 'Message'
 MessageHandler = Message + 'Handler'
 MessageToken = h.changeCase.paramCase(name)
%>
import { MessageBus } from 'src/infra/communication/message-bus';
import { CommandBus } from 'src/libs/cqrs';
import { MessageHandler } from 'src/libs/messages/message-handler.base';

export class <%= MessageHandler %> extends MessageHandler<<%= Message %>> {
  getMessageToken() {
    return '<%= MessageToken %>';
  }

  async execute(message: <%= Message %>) {
    return this.commandBus.execute(
      new YourCommand({
        payload: {
          organizationId: message.payload.organizationId,
          userId: message.payload.newAdminId,
        },
        trxId: message.trxId,
      }),
    );
  }
}
