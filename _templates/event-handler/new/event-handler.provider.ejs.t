---
to: src/domains/<%= module %>/event-handlers/<%= entity %>/<%= name %>/<%= name %>.event-handler.provider.ts
---
<%
 Event = h.changeCase.pascal(name) + 'Event'
 EventHandler = Event + 'Handler'
 Provider = EventHandler + 'Provider'
%>
import { FactoryProvider } from '@nestjs/common';
import { CommandBus } from 'src/libs/cqrs';
import { DomainEventsPublisher } from 'src/libs/domain';

import { UnitOfWork } from '../../../database/unit-of-work';
import { <%= EventHandler %> } from './<%= name %>.event-handler';

export const <%= Provider %>: FactoryProvider<<%= EventHandler %>> =
  {
    provide: <%= EventHandler %>,
    useFactory: (
      unitOfWork: UnitOfWork,
      domainEventsPublisher: DomainEventsPublisher,
      commandBus: CommandBus,
    ) => {
      return new <%= EventHandler %>(
        unitOfWork,
        domainEventsPublisher,
        commandBus,
      );
    },
    inject: [UnitOfWork, DomainEventsPublisher, CommandBus],
  };
