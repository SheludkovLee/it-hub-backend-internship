---
to: src/domains/<%= module %>/event-handlers/<%= entity %>/<%= name %>/<%= name %>.event-handler.ts
---
<%
 Event = h.changeCase.pascal(name) + 'Event'
 EventHandler = Event + 'Handler'
%>
import { CommandBus } from 'src/libs/cqrs';
import {
  DomainEventHandlerBase,
  DomainEventsPublisher,
  EventsHandler,
} from 'src/libs/domain';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { UnitOfWork } from '../../../database/unit-of-work';
import { <%= Event %> } from '../../../domain/events';

@EventsHandler(<%= Event %>)
export class <%= EventHandler %> extends DomainEventHandlerBase<
  UnitOfWork,
  <%= Event %>
> {
  constructor(
    unitOfWork: UnitOfWork,
    domainEventsPublisher: DomainEventsPublisher,
    private readonly commandBus: CommandBus,
  ) {
    super(unitOfWork, domainEventsPublisher);
  }

  async handle(
    event: <%= Event %>,
  ): Promise<Result<void, ExceptionBase>> {
    try {

      return Result.ok();
    } catch (err) {
      return Result.fail(err);
    }
  }
}
