---
to: src/domains/<%= module %>/graphql/resolvers/<%= entity %>/mutations/<%= name %>/<%= name %>.resolver.ts
---
<%
  MutationName = h.changeCase.camel(name)
  ResolverName = h.changeCase.pascal(name) + 'Resolver'
%>
import { Mutation, Resolver } from '@nestjs/graphql';
import { CommandBus } from 'src/libs/cqrs';

@Resolver()
export class <%= ResolverName %> {
  constructor(private readonly commandBus: CommandBus) {}

  @Mutation(() => type)
  async <%= MutationName %>(): Promise<type> {
    const result = await this.commandBus.execute<>();

    return;
  }
}
