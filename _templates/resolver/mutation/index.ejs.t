---
to: src/domains/<%= module %>/graphql/resolvers/<%= entity %>/mutations/<%= name %>/index.ts
---
<%
  QueryName = h.changeCase.camel(name)
  ResolverName = h.changeCase.pascal(name) + 'Resolver'
%>
export * from './<%= name %>.resolver';