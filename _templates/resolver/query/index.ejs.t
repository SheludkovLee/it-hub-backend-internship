---
to: src/domains/<%= module %>/graphql/resolvers/<%= entity %>/query/<%= name %>/index.ts
---
<%
  QueryName = h.changeCase.camel(name)
  ResolverName = h.changeCase.pascal(name) + 'Resolver'
%>
export * from './<%= name %>.resolver';