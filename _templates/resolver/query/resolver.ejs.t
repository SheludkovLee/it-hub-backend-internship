---
to: src/domains/<%= module %>/graphql/resolvers/<%= entity %>/query/<%= name %>/<%= name %>.resolver.ts
---
<%
  QueryName = h.changeCase.camel(name)
  ResolverName = h.changeCase.pascal(name) + 'Resolver'
%>
import { Mutation, Resolver } from '@nestjs/graphql';
import { QueryBus } from 'src/libs/cqrs';

@Resolver()
export class <%= ResolverName %> {
  constructor(private readonly queryBus: QueryBus) {}

  @Query(() => type)
  async <%= QueryName %>(): Promise<type> {
    const result = await this.queryBus.execute<>();

    return;
  }
}
