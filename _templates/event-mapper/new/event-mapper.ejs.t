---
to: src/domains/<%= module %>/event-mappers/<%= entity %>/<%= name %>/<%= name %>.event-mapper.ts
---
<%
 Event = h.changeCase.pascal(name) + 'Event'
 EventMapper = Event + 'Mapper'
%>
import { EventsHandler } from 'src/libs/domain';
import { ExceptionBase } from 'src/libs/exceptions';
import { DomainEventMapper } from 'src/libs/messages/domain-event-mapper.base';
import { Result } from 'src/libs/utils';
import { Message } from 'src/libs/messages/message.base';

import { <%= Event %> } from '../../../domain';

@EventsHandler(<%= Event %>)
export class <%= EventMapper %> extends DomainEventMapper {
  execute(
    event: <%= Event %>,
  ): Result<Message, ExceptionBase> {
    try {
      return Result.ok();
    } catch (e) {
      return Result.fail(e);
    }
  }
}
