---
to: src/domains/<%= module %>/event-mappers/<%= entity %>/<%= name %>/index.ts
---
export * from './<%= name %>.event-mapper';
