module.exports = [
  {
    type: 'input',
    name: 'model',
    message: 'Enter the model name',
  },
  {
    type: 'input',
    name: 'query',
    message: 'Enter the query name',
  },
];
