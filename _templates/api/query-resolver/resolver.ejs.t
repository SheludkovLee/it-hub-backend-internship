---
to: src/api/queries/<%= model %>/<%= query %>/<%= query %>.resolver.ts
---
<%
  QueryName = h.changeCase.camel(query)
  ResolverName = h.changeCase.pascal(query) + 'Resolver'
  InputName = h.changeCase.pascal(query) + 'Input'
%>
import { Args, Query, Resolver } from '@nestjs/graphql';
import { GraphqlQueryServicesRoot } from 'src/api/query-services-root';
import { GetPermissionsChecker } from 'src/common/decorators/get-permissions-checker.decorator';
import { PermissionsChecker } from 'src/common/guards/permissions/permissions-checker';

import { <%= InputName %> } from './<%= query %>.input';

@Resolver()
export class <%= ResolverName %> {
  constructor(private readonly queryServicesRoot: GraphqlQueryServicesRoot) {}

  @Query(() => type)
  async <%= QueryName %>(@Args('input') input: <%= InputName %>, @GetPermissionsChecker() permissionsChecker: PermissionsChecker): Promise<type> {
    return this.queryServicesRoot.getBuildingAreaQueryService().fetchMany();
  }
}

