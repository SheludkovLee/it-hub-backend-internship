---
to: src/api/queries/<%= model %>/<%= query %>/<%= query %>.payload.ts
---
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class <%= h.changeCase.pascal(query) %>Payload {
  @Field()
  data: string;
}
