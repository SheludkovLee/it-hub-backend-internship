---
to: src/api/queries/<%= model %>/<%= query %>/<%= query %>.input.ts
---
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class <%= h.changeCase.pascal(query) %>Input {
  @Field()
  data: string;
}