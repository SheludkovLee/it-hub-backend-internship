---
to: src/api/mutations/<%= model %>/<%= mutation %>/<%= mutation %>.input.ts
---
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class <%= h.changeCase.pascal(mutation) %>Input {
  @Field()
  data: string;
}