---
to: src/api/mutations/<%= model %>/<%= mutation %>/<%= mutation %>.resolver.ts
---
<%
  MutationName = h.changeCase.camel(mutation)
  ResolverName = h.changeCase.pascal(mutation) + 'Resolver'
  InputName = h.changeCase.pascal(mutation) + 'Input'
%>
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { GraphqlQueryServicesRoot } from 'src/api/query-services-root';
import { CommandBus } from 'src/libs/cqrs';
import { GetPermissionsChecker } from 'src/common/decorators/get-permissions-checker.decorator';
import { PermissionsChecker } from 'src/common/guards/permissions/permissions-checker';

import { <%= InputName %> } from './<%= mutation %>.input';

@Resolver()
export class <%= ResolverName %> {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryServicesRoot: GraphqlQueryServicesRoot,
  ) {}

  @Mutation(() => type)
  async <%= MutationName %>(@Args('input') input: <%= InputName %>, @GetPermissionsChecker() permissionsChecker: PermissionsChecker): Promise<type> {
    const result = await this.commandBus.execute<BuildingAreaEntity>(
      new CreateBuildingAreaCommand({ payload: input }),
    );

    return this.queryServicesRoot
      .getBuildingAreaQueryService()
      .fetchById(result.unwrap().id);
  }
}

