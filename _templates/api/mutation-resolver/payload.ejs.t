---
to: src/api/mutations/<%= model %>/<%= mutation %>/<%= mutation %>.payload.ts
---
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class <%= h.changeCase.pascal(mutation) %>Payload {
  @Field()
  data: string;
}
