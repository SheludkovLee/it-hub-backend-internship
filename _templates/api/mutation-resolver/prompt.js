module.exports = [
  {
    type: 'input',
    name: 'model',
    message: 'Enter the model name',
  },
  {
    type: 'input',
    name: 'mutation',
    message: 'Enter the mutation name',
  },
];
