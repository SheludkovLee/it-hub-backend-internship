# Генераторы данных -- must have чтобы не получать комментарии по кодстайлу на кодревью 🤩

## Analytics -- генераторы для скраперов и ORM-сущностей Clickhouse-а

Скрапер -- класс который отвечает за сбор данных которые затем пойдут в кликхаус. Скрапер сходит в базу, сделает первичное форматирование данных и передаст данные в Буфер. Буфер сохранит данные в БД (таблица clickhouse_buffer) в нужном для кликхаусе формате. Затем, Релизер возьмет из Буфера данные и отправит их в кликхаус.
