---
to: src/domains/<%= module %>/database/repository/<%= entity %>/<%= entity %>.mapper.ts
---
<%
  EntityName = h.changeCase.pascal(entity) + 'Entity'
  OrmEntityName = h.changeCase.pascal(entity) + 'OrmEntity'
  MapperName = h.changeCase.pascal(entity) + 'Mapper'
%>
import { Mapper } from 'src/libs/database';
import { DateVO, UUID } from 'src/libs/domain';

import { <%= EntityName %> } from '../../../domain';
import { <%= OrmEntityName %> } from '../../entities';

export class <%= MapperName %>
  implements Mapper<<%= EntityName %>, <%= OrmEntityName %>>
{
  toDomain(entity: <%= OrmEntityName %>): <%= EntityName %> {
    return new <%= EntityName %>({
      id: new UUID(entity.id),
      props: {},
      updatedAt: new DateVO(entity.updatedAt),
      createdAt: new DateVO(entity.createdAt),
    });
  }

  toOrm(entity: <%= EntityName %>): <%= OrmEntityName %> {
    const props = entity.getPropsCopy();

    return <%= OrmEntityName %>.create({});
  }
}
