---
to: src/domains/<%= module %>/database/repository/<%= entity %>/<%= entity %>.repository.ts
---
<%
  RepositoryName = h.changeCase.pascal(entity) + 'Repository'
  EntityName = h.changeCase.pascal(entity) + 'Entity'
  OrmEntityName = h.changeCase.pascal(entity) + 'OrmEntity'
  MapperName = h.changeCase.pascal(entity) + 'Mapper'
%>
import { ObjectionAggregateRepositoryBase } from 'src/libs/database';

import { <%= EntityName %> } from '../../../domain';
import { <%= OrmEntityName %> } from '../../entities';
import { <%= MapperName %> } from './<%= entity %>.mapper';

export class <%= RepositoryName %>
  extends ObjectionAggregateRepositoryBase<
    <%= EntityName %>,
    <%= OrmEntityName %>
  >
{
  protected model = <%= OrmEntityName %>;
  protected mapper = new <%= MapperName %>();
}