---
to: src/domains/<%= module %>/database/repository/<%= entity %>/index.ts
---
export * from './<%= entity %>.mapper';
export * from './<%= entity %>.repository';
