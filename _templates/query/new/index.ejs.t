---
to: src/domains/<%= module %>/queries/<%= entity %>/<%= name %>/index.ts
---
<%
  QueryName = h.changeCase.pascal(name) + 'Query'
  QueryToken = h.changeCase.snake(name)
%>
export * from './<%= name %>.query-handler';
export * from './<%= name %>.query-handler.provider';
export * from './<%= name %>.query';