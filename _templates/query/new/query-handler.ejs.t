---
to: src/domains/<%= module %>/queries/<%= entity %>/<%= name %>/<%= name %>.query-handler.ts
---
<%
  QueryName = h.changeCase.pascal(name) + 'Query'
  QueryHandlerName = QueryName + 'Handler'
%>
import { QueryHandler, QueryHandlerBase } from 'src/libs/cqrs';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { ReadRepository } from '../../../database';
import { <%= QueryName %> } from './<%= name %>.query';

@QueryHandler(<%= QueryName %>)
export class <%= QueryHandlerName %> extends QueryHandlerBase<
  <%= QueryName %>,
  ReturnType,
  ReadRepository
> {
  handle(
    query: <%= QueryName %>,
  ): Promise<Result<ReturnType, ExceptionBase>> {
    return Result.ok();
  }
}
