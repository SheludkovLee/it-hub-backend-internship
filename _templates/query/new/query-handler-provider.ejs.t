---
to: src/domains/<%= module %>/queries/<%= entity %>/<%= name %>/<%= name %>.query-handler.provider.ts
---
<%
  QueryName = h.changeCase.pascal(name) + 'Query'
  QueryHandlerName = QueryName + 'Handler'
  QueryHandlerProvider = QueryHandlerName + 'Provider'
%>
import { QueryBus } from 'src/libs/cqrs';
import { Provider } from '@nestjs/common';

import { ReadRepository } from '../../../database';
import { <%= QueryHandlerName %> } from './<%= name %>.query-handler';

export const <%= QueryHandlerProvider %>: Provider<<%= QueryHandlerName %>> =
  {
    provide: <%= QueryHandlerName %>,
    useFactory: (queryBus: QueryBus, repository: ReadRepository) => {
      return new <%= QueryHandlerName %>(queryBus, repository);
    },
    inject: [QueryBus, ReadRepository],
  };
