---
to: src/domains/<%= module %>/queries/<%= entity %>/<%= name %>/<%= name %>.query.ts
---
<%
  QueryName = h.changeCase.pascal(name) + 'Query'
  QueryToken = h.changeCase.snake(name)
%>
import { Query, QueryBase } from 'src/libs/cqrs';

interface Payload {}

@Query('<%= QueryToken %>')
export class <%= QueryName %> extends QueryBase<Payload> {}
