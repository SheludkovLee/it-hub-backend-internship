---
to: src/domains/<%= module %>/domain/entities/<%= entity %>.entity.ts
---
<%
  EntityName = h.changeCase.pascal(entity)
%>
import { Entity, CreateEntityProps, ID } from 'src/libs/domain';

export interface <%= EntityName %>Props {}

export class <%= EntityName %>Entity extends Entity<<%= EntityName %>Props> {
  protected _id: ID;

  constructor(props: CreateEntityProps<<%= EntityName %>Props>) {
    super(props);
  }

  public validate(): void {
    return;
  }
}
