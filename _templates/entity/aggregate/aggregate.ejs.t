---
to: src/domains/<%= module %>/domain/entities/<%= entity %>.entity.ts
---
<%
  AggregateName = h.changeCase.pascal(entity)
%>
import { AggregateRoot, CreateEntityProps, ID } from 'src/libs/domain';

export interface <%= AggregateName %>Props {}

export class <%= AggregateName %>Entity extends AggregateRoot<<%= AggregateName %>Props> {
  protected _id: ID;

  constructor(props: CreateEntityProps<<%= AggregateName %>Props>) {
    super(props);
  }

  public validate(): void {
    return;
  }
}
