---
to: src/domains/<%= module %>/domain/events/<%= entity %>/<%= name %>/index.ts
---
export * from './<%= name %>.event';
