---
to: src/domains/<%= module %>/domain/events/<%= entity %>/<%= name %>/<%= name %>.event.ts
---
<%
 Entity = h.changeCase.pascal(entity)
 EntityName = h.changeCase.pascal(entity) + 'Entity'
 EventName = h.changeCase.pascal(name) + 'Event'
 EventToken = module + h.changeCase.snake(name)
%>
import { DomainEvent, DomainEventBase } from 'src/libs/domain';

import { <%= EntityName %> } from '../../../entities';

interface Payload {}

@DomainEvent('<%= EventToken %>')
export class <%= EventName %> extends DomainEventBase<Payload> {
  static from<%= Entity %>(entity: <%= EntityName %>): <%= EventName %> {
    const props = entity.getPropsCopy();

    return new <%= EventName %>({
      payload: {},
    });
  }
}