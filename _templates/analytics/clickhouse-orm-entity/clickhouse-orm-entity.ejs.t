---
to: src/domains/<%= context %>/analytics/entities/<%= entity %>.orm-entity.ts
---
<%
  EntityName = h.changeCase.pascal(entity)
%>
import { BaseClickhouseOrmEntity } from 'src/libs/clickhouse/orm-entities/base.orm-entity';
import { BaseClickhouseTableValidator } from 'src/libs/clickhouse/orm-entities/base.table-validator';

interface <%= EntityName %>ClickhouseOrmEntityProps {}

export class <%= EntityName %>ClickhouseOrmEntity extends BaseClickhouseOrmEntity<<%= EntityName %>ClickhouseOrmEntityProps> {}

export class <%= EntityName %>ClickhouseTableValidator extends BaseClickhouseTableValidator<<%= EntityName %>ClickhouseOrmEntity> {
  constructor() {
    super({
      type: 'object',
      properties: {

      },
      required: [

      ],
    });
  }
}
