module.exports = [
  {
    type: 'input',
    name: 'context',
    message: 'Enter the context name',
  },
  {
    type: 'input',
    name: 'entity',
    message: 'Enter the entity name',
  },
];
