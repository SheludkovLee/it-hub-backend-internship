---
to: src/domains/<%= context %>/analytics/scrapers/<%= scraper %>/<%= scraper %>.scraper.ts
---
<%
  ScraperName = h.changeCase.pascal(scraper)
%>
import { Injectable } from '@nestjs/common';
import { BaseClickhouseEventsScraper } from 'src/libs/clickhouse/scrapers/base.scraper';
import { ClickhouseBufferRegister } from 'src/libs/clickhouse/buffer/buffer-register';

@Injectable()
export class <%= ScraperName %>Scraper extends BaseClickhouseEventsScraper {
  constructor(buffer: ClickhouseBufferRegister) {
    super(buffer);
  }

  async scrape(): Promise<boolean> {
    return true;
  }
}


