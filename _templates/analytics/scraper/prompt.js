module.exports = [
  {
    type: 'input',
    name: 'context',
    message: 'Enter the context name',
  },
  {
    type: 'input',
    name: 'scraper',
    message: 'Enter the scraper name',
  },
];
