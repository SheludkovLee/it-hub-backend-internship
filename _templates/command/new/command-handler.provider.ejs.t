---
to: src/domains/<%= module %>/commands/<%= entity %>/<%= name %>/<%= name %>.command-handler.provider.ts
---
<%HandlerName = h.changeCase.pascal(name) + 'CommandHandler'
  ProviderName = HandlerName + 'Provider' %>
import { CommandBus } from 'src/libs/cqrs';
import {
  AsyncDomainEventsPublisher,
  DomainEventsPublisher,
} from 'src/libs/domain';
import { Provider } from '@nestjs/common';

import { UnitOfWork } from '../../../database';
import { <%= HandlerName %> } from './<%= name %>.command-handler';

export const <%= ProviderName %>: Provider<<%= HandlerName %>> = {
  provide: <%= HandlerName %>,
  useFactory: (
       unitOfWork: UnitOfWork,
       domainEventsPublisher: DomainEventsPublisher,
       asyncDomainEventsPublisher: AsyncDomainEventsPublisher,
       commandBus: CommandBus,
  ) => {
       return new <%= HandlerName %>(
           unitOfWork,
           domainEventsPublisher,
           asyncDomainEventsPublisher,
           commandBus,
       );
  },
  inject: [UnitOfWork, DomainEventsPublisher, AsyncDomainEventsPublisher, CommandBus],
};
