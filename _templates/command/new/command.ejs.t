---
to: src/domains/<%= module %>/commands/<%= entity %>/<%= name %>/<%= name %>.command.ts
---
<%
 PascaledName = h.changeCase.pascal(name)
 CommandName = PascaledName + 'Command'
 ModuleName = h.changeCase.pascal(module)
 CommandToken = h.changeCase.snake(ModuleName + PascaledName)
%>
import { Command, CommandBase } from 'src/libs/cqrs';

interface Payload {}

@Command('<%= CommandToken %>')
export class <%= CommandName %> extends CommandBase<Payload> {}
