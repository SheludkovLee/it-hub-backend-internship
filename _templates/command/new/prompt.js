module.exports = [
  {
    type: 'input',
    name: 'module',
    message: 'Enter the target domain',
  },
  {
    type: 'input',
    name: 'entity',
    message: 'Enter the entity name',
  },
  {
    type: 'input',
    name: 'name',
    message: 'Enter the name for command',
  },
];
