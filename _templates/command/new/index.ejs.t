---
to: src/domains/<%= module %>/commands/<%= entity %>/<%= name %>/index.ts
---
export * from './<%= name %>.command';
export * from './<%= name %>.command-handler';
export * from './<%= name %>.command-handler.provider';
