---
to: src/domains/<%= module %>/commands/<%= entity %>/<%= name %>/<%= name %>.command-handler.ts
---
<%CommandName = h.changeCase.pascal(name) + 'Command'
  HandlerName = CommandName + 'Handler' %>
import { CommandHandler, CommandHandlerBase } from 'src/libs/cqrs';
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { UnitOfWork } from '../../../database';
import { <%= CommandName %> } from './<%= name %>.command';

@CommandHandler(<%= CommandName %>)
export class <%= HandlerName %> extends CommandHandlerBase<
  UnitOfWork,
  <%= CommandName %>,
  void
> {
  async handle(command: <%= CommandName %>): Promise<Result<void, ExceptionBase>>{
      try {
          return Result.ok();
      } catch(e) {
          return Result.fail(e);
      }
  }
}
