---
to: src/domains/notification/message-mappers/<%= entity %>/<%= name %>/index.ts
---
export * from './<%= name %>.notification-mapper';
