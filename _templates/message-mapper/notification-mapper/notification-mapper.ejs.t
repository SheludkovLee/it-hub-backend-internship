---
to: src/domains/notification/message-mappers/<%= entity %>/<%= name %>/<%= name %>.notification-mapper.ts
---
<%
 Message = h.changeCase.pascal(name) + 'Message'
 IMessage = 'I' + Message
 NotificationMapper = Message + 'NotificationMapper'
%>
import { NotificationUnitOfWorkPort } from 'src/domains/notification/database';
import { NotificationEntity } from 'src/domains/notification/domain';
import { NotificationBodyVO } from 'src/domains/notification/domain/vo';
import { MessageToNotificationMapper } from 'src/domains/notification/message-mappers';
import { MessageBus } from 'src/infra/communication/message-bus';
import { <%= Message %> } from 'src/infra/messages';
import { UUID } from 'src/libs/domain';
import { Result } from 'src/libs/utils';

export class <%= NotificationMapper %> extends MessageToNotificationMapper<<%= IMessage %>> {
  constructor(
    readonly unitOfWork: NotificationUnitOfWorkPort,
    readonly messageBus: MessageBus,
  ) {
    super(unitOfWork, messageBus);
  }

  getMessageToken() {
    return '';
  }

  async execute(message: <%= IMessage %>) {
    return Result.ok(
      NotificationEntity.create({
        userId: new UUID(),
        key: '',
        title: '',
        body: new NotificationBodyVO({
          text: 'text',
          bindings: [],
        }),
      }),
    );
  }
}
