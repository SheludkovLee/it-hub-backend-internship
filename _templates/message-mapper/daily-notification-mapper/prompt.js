module.exports = [
  {
    type: 'input',
    name: 'entity',
    message: 'Enter the entity name',
  },
  {
    type: 'input',
    name: 'name',
    message: 'Enter the message name',
  },
];
