---
to: src/domains/<%= module %>/database/entities/<%= entity %>.orm-entity.ts
---
<%
  OrmEntityName = h.changeCase.pascal(entity)
  TableName = h.changeCase.snake(entity)
%>
import { buildTableName } from 'database/utils';
import { PatchedModelObject, RelationMappingsThunk } from 'objection';
import { ObjectionOrmEntityBase } from 'src/libs/database';

export class <%= OrmEntityName %>OrmEntity extends ObjectionOrmEntityBase {
  static tableName = buildTableName('learning', '<%= TableName %>');

  static create(
    props: PatchedModelObject<<%= OrmEntityName %>OrmEntity>,
  ): <%= OrmEntityName %>OrmEntity {
    return this.fromJson(props);
  }

  static relationMappings: RelationMappingsThunk = () => {
    return {};
  };
}

