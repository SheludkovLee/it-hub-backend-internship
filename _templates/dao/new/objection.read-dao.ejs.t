---
to: src/domains/<%= module %>/database/read-model/<%= entity %>/<%= name %>/<%= name %>.objection.read-dao.ts
---
<%
  OrmEntityName = h.changeCase.pascal(entity) + 'OrmEntity'
  ObjectionOrmEntityName = h.changeCase.pascal(entity) + 'ObjectionOrmEntity'
  DaoName = h.changeCase.pascal(name) + 'ReadDao'
  ObjectionDaoName = h.changeCase.pascal(name) + 'ObjectionReadDao'
%>
import { ExceptionBase } from 'src/libs/exceptions';
import { Result } from 'src/libs/utils';

import { <%= ObjectionOrmEntityName %>, <%= OrmEntityName %> } from '../../../entities';
import { <%= DaoName %> } from './<%= name %>.read-dao';

export class <%= ObjectionDaoName %> extends <%= DaoName %> {
  async query(
    params: any,
  ): Promise<Result<<%= OrmEntityName %>, ExceptionBase>> {
    try {
      const result = await <%= ObjectionOrmEntityName %>.query();

      return Result.ok();
    } catch (e) {
      return Result.fail(e);
    }
  }
}
