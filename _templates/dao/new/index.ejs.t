---
to: src/domains/<%= module %>/database/read-model/<%= entity %>/<%= name %>/index.ts
---

export * from './<%= name %>.objection.read-dao';
export * from './<%= name %>.read-dao';