---
to: src/domains/<%= module %>/database/read-model/<%= entity %>/<%= name %>/<%= name %>.read-dao.ts
---
<%
  OrmEntityName = h.changeCase.pascal(entity) + 'OrmEntity'
  DaoName = h.changeCase.pascal(name) + 'ReadDao'
%>
import { ReadDaoBase } from 'src/libs/database';

import { <%= OrmEntityName %> } from '../../../entities';

export abstract class <%= DaoName %> extends ReadDaoBase<
  <%= OrmEntityName %>,
  any
> {}
