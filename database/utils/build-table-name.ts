import { BoundedContext } from '../types';

export const buildTableName = (ctx: BoundedContext, tableName: string) =>
  [ctx, tableName].join('_');
