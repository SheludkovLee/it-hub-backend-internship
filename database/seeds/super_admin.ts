import 'reflect-metadata';

import { hashSync } from 'bcrypt';
import { Knex } from 'knex';
import { UUID } from 'src/libs/domain';

import { buildTableName } from '../utils';

const setPassword = (password: string) => hashSync(password, 2);

export async function seed(knex: Knex): Promise<any> {
  const password = 'qwertyuiop';

  const superAdmins: {
    id: string;
    email: string;
    encryptedPassword: string;
    isSuperAdmin: boolean;
    updatedAt: string;
    createdAt: string;
  }[] = [
    {
      id: UUID.generate().value,
      email: 'super-1@seed.ru',
      encryptedPassword: setPassword(password),
      isSuperAdmin: true,
      updatedAt: new Date().toISOString(),
      createdAt: new Date().toISOString(),
    },
    {
      id: UUID.generate().value,
      email: 'super-2@seed.ru',
      encryptedPassword: setPassword(password),
      isSuperAdmin: true,
      updatedAt: new Date().toISOString(),
      createdAt: new Date().toISOString(),
    },
    {
      id: UUID.generate().value,
      email: 'super-admin@newlxp.ru',
      encryptedPassword: setPassword('upRCg2jx2yR9zEAs'),
      isSuperAdmin: true,
      updatedAt: new Date().toISOString(),
      createdAt: new Date().toISOString(),
    },
  ];

  await knex(buildTableName('identity', 'users')).insert(superAdmins);
}
