export const BOUNDED_CONTEXTS = [
  'identity',
  'learning',
  'learning_design',
  'organization_management',
  'role_system',
  'schedule',
  'notification',
] as const;
