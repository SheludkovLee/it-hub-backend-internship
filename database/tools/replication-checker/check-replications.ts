import 'reflect-metadata';

import { Knex } from 'knex';
import { snakeCase } from 'lodash';

import { replicationMap } from './replications-map.constants';

export interface ReplicationCheckerPayload {
  originId: string;
  replicationId: string;
  replicationName: string;
  originName: string;
  wrongColumns: string[];
}

export async function checkReplications(
  knex: Knex,
): Promise<ReplicationCheckerPayload[]> {
  const result: ReplicationCheckerPayload[][] = [];
  for (const originTableName in replicationMap) {
    const { replications, primaryColumns } = replicationMap[originTableName];

    for (const replication of replications) {
      const {
        tableName: replicationTableName,
        isSoftDeleted,
        replicatedColumns,
      } = replication;
      const qb = knex(originTableName);

      const selectOriginId = primaryColumns
        .map((id) => `"${originTableName}"."${id}"`)
        .join(` || ', ' || `);
      const selectReplicationId = primaryColumns
        .map((id) => `"${replicationTableName}"."${id}"`)
        .join(` || ', ' || `);

      qb.select(
        knex.raw(`${selectOriginId} as "originId"`),
        knex.raw(`${selectReplicationId} as "replicationId"`),
        knex.raw(`'${originTableName}' as "originName"`),
        knex.raw(`'${replicationTableName}' as "replicationName"`),
      );

      replicatedColumns
        .filter((column) => !primaryColumns.includes(column))
        .forEach((column) => {
          qb.select(
            knex.raw(
              `CASE WHEN "${originTableName}"."${snakeCase(
                column,
              )}" != "${replicationTableName}"."${snakeCase(
                column,
              )}" THEN '${column}' ELSE NULL END as "${column}"`,
            ),
          );
        });

      if (isSoftDeleted) {
        qb.select(
          knex.raw(`'${replicationTableName}.deleted_at' as "deletedAt"`),
        );
      }

      qb.fullOuterJoin(replicationTableName, function () {
        primaryColumns.forEach((id) =>
          this.on(`${originTableName}.${id}`, `${replicationTableName}.${id}`),
        );
      });

      qb.orWhere((builder) => {
        primaryColumns.forEach((id) =>
          builder
            .orWhere(`${originTableName}.${id}`, 'IS', null)
            .orWhere(`${replicationTableName}.${id}`, 'IS', null),
        );
      });

      qb.orWhere((builder) => {
        replicatedColumns
          .filter((column) => !primaryColumns.includes(column))
          .forEach((column) => {
            builder.orWhereRaw(
              `${originTableName}.${snakeCase(
                column,
              )} != ${replicationTableName}.${snakeCase(column)}`,
            );
          });
      });

      let payload: (ReplicationCheckerPayload & { deletedAt: string })[] =
        await qb;

      if (isSoftDeleted) {
        payload = payload.filter(
          (row) => !(row.replicationId && row.deletedAt),
        );
      }

      payload = payload.map((row) => ({
        originId: row.originId,
        replicationId: row.replicationId,
        originName: row.originName,
        replicationName: row.replicationName,
        deletedAt: row.deletedAt,
        wrongColumns: replicatedColumns
          .map((column) => row[column])
          .filter(Boolean),
      }));

      result.push(payload);
    }
  }

  return result.flat();
}
