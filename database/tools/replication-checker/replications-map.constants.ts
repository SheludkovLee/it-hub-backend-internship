import { buildTableName } from '../../utils';

export type ReplicationOptions = {
  replications: {
    tableName: string;
    replicatedColumns: string[];
    isAutoInsertEnabled: boolean;
    isAutoDeleteEnabled: boolean;
    isAutoUpdateEnabled: boolean;
    isSoftDeleted: boolean;
  }[];
  primaryColumns: string[];
};

export const replicationMap: Record<string, ReplicationOptions> = {
  [buildTableName('organization_management', 'organizations')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'organizations'),
        replicatedColumns: ['id'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: true,
      },
      {
        tableName: buildTableName('learning_design', 'organizations'),
        replicatedColumns: ['id'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
      {
        tableName: buildTableName('learning', 'organizations'),
        replicatedColumns: ['id'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
      {
        tableName: buildTableName('notification', 'organizations'),
        replicatedColumns: ['id', 'timezone'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
    ],
    primaryColumns: ['id'],
  },
  [buildTableName('organization_management', 'suborganizations')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'suborganizations'),
        replicatedColumns: ['id', 'organizationId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: true,
      },
      {
        tableName: buildTableName('learning', 'suborganizations'),
        replicatedColumns: ['id', 'organizationId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
      {
        tableName: buildTableName('learning_design', 'suborganizations'),
        replicatedColumns: ['id', 'organizationId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
      {
        tableName: buildTableName('notification', 'suborganizations'),
        replicatedColumns: ['id', 'organizationId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
    ],
    primaryColumns: ['id'],
  },
  [buildTableName('identity', 'users')]: {
    replications: [
      {
        tableName: buildTableName('notification', 'users'),
        replicatedColumns: ['id', 'email', 'isSuperAdmin'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
    ],
    primaryColumns: ['id'],
  },
  [buildTableName('learning', 'teachers')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'teachers'),
        replicatedColumns: ['id'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: true,
      },
    ],
    primaryColumns: ['id'],
  },
  [buildTableName('organization_management', 'suborganization_admins')]: {
    replications: [
      {
        tableName: buildTableName('notification', 'suborganization_admins'),
        replicatedColumns: ['suborganizationId', 'userId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
    ],
    primaryColumns: ['suborganization_id', 'user_id'],
  },
  [buildTableName('organization_management', 'organization_admins')]: {
    replications: [
      {
        tableName: buildTableName('notification', 'organization_admins'),
        replicatedColumns: ['organizationId', 'userId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
    ],
    primaryColumns: ['organization_id', 'user_id'],
  },
  [buildTableName('learning', 'teachers_suborganizations')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'teachers_suborganizations'),
        replicatedColumns: ['teacherId', 'suborganizationId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: true,
      },
      {
        tableName: buildTableName('notification', 'teachers_suborganizations'),
        replicatedColumns: ['teacherId', 'suborganizationId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
    ],
    primaryColumns: ['teacher_id', 'suborganization_id'],
  },
  [buildTableName('learning_design', 'disciplines_groups')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'disciplines_groups'),
        replicatedColumns: ['id'],
        isAutoInsertEnabled: false,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: false,
        isSoftDeleted: true,
      },
    ],
    primaryColumns: ['id'],
  },
  [buildTableName('learning_design', 'template_disciplines')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'template_disciplines'),
        replicatedColumns: [
          'id',
          'disciplinesGroupId',
          'suborganizationId',
          'studyHoursCount',
        ],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: true,
      },
    ],
    primaryColumns: ['id'],
  },
  [buildTableName('learning', 'disciplines')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'disciplines'),
        replicatedColumns: [
          'id',
          'archivedAt',
          'suborganizationId',
          'studyHoursCount',
        ],
        isAutoInsertEnabled: false,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: false,
        isSoftDeleted: true,
      },
    ],
    primaryColumns: ['id'],
  },
  [buildTableName('learning', 'teachers_disciplines')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'teachers_disciplines'),
        replicatedColumns: ['teacherId', 'disciplineId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: true,
      },
    ],
    primaryColumns: ['teacher_id', 'discipline_id'],
  },
  [buildTableName('learning', 'learning_groups')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'learning_groups'),
        replicatedColumns: ['id', 'suborganizationId'],
        isAutoInsertEnabled: false,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: false,
        isSoftDeleted: true,
      },
    ],
    primaryColumns: ['id'],
  },
  [buildTableName('learning', 'students_learning_groups')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'students_learning_groups'),
        replicatedColumns: [
          'studentId',
          'learningGroupId',
          'isActivated',
          'enrolledAt',
          'expelledAt',
        ],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: true,
      },
    ],
    primaryColumns: ['student_id', 'learning_group_id'],
  },
  [buildTableName('learning', 'study_periods')]: {
    replications: [
      {
        tableName: buildTableName('schedule', 'study_periods'),
        replicatedColumns: ['id', 'organizationId', 'startDate', 'endDate'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: true,
      },
    ],
    primaryColumns: ['id'],
  },
  [buildTableName('learning_design', 'specialties')]: {
    replications: [
      {
        tableName: buildTableName('learning', 'specialties'),
        replicatedColumns: ['id', 'suborganizationId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
    ],
    primaryColumns: ['id'],
  },
  [buildTableName('learning', 'students_parents')]: {
    replications: [
      {
        tableName: buildTableName('notification', 'students_parents'),
        replicatedColumns: ['studentId', 'parentId'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
    ],
    primaryColumns: ['student_id', 'parent_id'],
  },
  [buildTableName('learning', 'students_specialties')]: {
    replications: [
      {
        tableName: buildTableName('notification', 'students_specialties'),
        replicatedColumns: ['studentId', 'specialtyId'],
        isAutoInsertEnabled: false,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: false,
        isSoftDeleted: false,
      },
    ],
    primaryColumns: ['student_id', 'specialty_id'],
  },
  [buildTableName('learning_design', 'template_sections')]: {
    replications: [
      {
        tableName: buildTableName('learning', 'template_sections'),
        replicatedColumns: ['id'],
        isAutoInsertEnabled: true,
        isAutoDeleteEnabled: true,
        isAutoUpdateEnabled: true,
        isSoftDeleted: false,
      },
    ],
    primaryColumns: ['id'],
  },
};
