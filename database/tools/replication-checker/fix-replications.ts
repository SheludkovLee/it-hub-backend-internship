import 'reflect-metadata';

import { Knex } from 'knex';
import { isNil, pick, snakeCase } from 'lodash';

import { ReplicationCheckerPayload } from './check-replications';
import { replicationMap } from './replications-map.constants';

export interface ReplicationNeededToFix {
  idsToSave: string[][];
  idsToDelete: string[][];
  idsToUpdate: string[][];
  origin: string;
}

export async function fixReplications(
  knex: Knex,
  payload: ReplicationCheckerPayload[],
): Promise<void> {
  const env = process.env.APP_ENVIRONMENT;
  const replicationNeededToFixes: Record<string, ReplicationNeededToFix> = {};
  payload.forEach((payload) => {
    replicationNeededToFixes[payload.replicationName] = {
      idsToSave: [],
      idsToDelete: [],
      idsToUpdate: [],
      origin: payload.originName,
    };
  });

  for (const row of payload) {
    if (row.originId && !row.replicationId) {
      replicationNeededToFixes[row.replicationName].idsToSave.push(
        row.originId.split(', '),
      );
    }
    if (row.replicationId && !row.originId) {
      replicationNeededToFixes[row.replicationName].idsToDelete.push(
        row.replicationId.split(', '),
      );
    }

    if (row.replicationId && row.originId && row.wrongColumns?.length) {
      replicationNeededToFixes[row.replicationName].idsToUpdate.push(
        row.replicationId.split(', '),
      );
    }
  }

  for (const replicationName in replicationNeededToFixes) {
    const { origin, idsToDelete, idsToSave, idsToUpdate } =
      replicationNeededToFixes[replicationName];
    const { replications, primaryColumns } = replicationMap[origin];
    const {
      replicatedColumns,
      isAutoDeleteEnabled,
      isAutoInsertEnabled,
      isAutoUpdateEnabled,
    } = replications.find(
      (replication) => replication.tableName === replicationName,
    );

    const qbToSelectForSave = knex(origin);
    const qbToSelectForUpdate = knex(origin);

    const qbToDelete = knex(replicationName);
    const qbToSave = knex(replicationName);

    const wherePrimaryColumnsClause = primaryColumns.join(', ');

    const whereInsertValuesClause = idsToSave
      .map((ids) => `(` + ids.map((id) => `'${id}'`) + ')')
      .join(',');
    const whereDeleteValuesClause = idsToDelete
      .map((ids) => `(` + ids.map((id) => `'${id}'`) + ')')
      .join(',');
    const whereUpdateValuesClause = idsToUpdate
      .map((ids) => `(` + ids.map((id) => `'${id}'`) + ')')
      .join(',');

    qbToSelectForSave.whereRaw(
      knex.raw(
        `(${wherePrimaryColumnsClause}) in (${whereInsertValuesClause})`,
      ),
    );
    qbToSelectForUpdate.whereRaw(
      knex.raw(
        `(${wherePrimaryColumnsClause}) in (${whereUpdateValuesClause})`,
      ),
    );

    qbToDelete.where(
      knex.raw(
        `(${wherePrimaryColumnsClause}) in (${whereDeleteValuesClause})`,
      ),
    );

    const canInsert = idsToSave?.length && isAutoInsertEnabled;
    const canDelete = idsToDelete?.length && isAutoDeleteEnabled;
    const canUpdate = idsToUpdate?.length && isAutoUpdateEnabled;

    if (!canInsert && !canDelete && !canUpdate) {
      continue;
    }

    if (canInsert) {
      const rowsToInsert: string[] = await qbToSelectForSave;
      // TODO добавить в replications-map.constants типы колонок, чтобы по-человечески кастить
      const valuesSql = rowsToInsert
        .map(
          (row) =>
            '(' +
            Object.entries(pick(row, replicatedColumns))
              .map(([column, val]) => {
                if (isNil(val)) {
                  return 'NULL';
                }
                // типа колонки с датами
                if (column.includes('At')) {
                  return "'" + new Date(val).toISOString() + "'";
                }
                return "'" + val + "'";
              })
              .join(',') +
            ')',
        )
        .join(',');
      const columnsSql =
        '(' +
        replicatedColumns.map((column) => `"${snakeCase(column)}"`).join(',') +
        ')';
      qbToSave.insert(knex.raw(`${columnsSql} values ${valuesSql}`));
    }

    const qbsForUpdate = [];
    if (canUpdate) {
      const rowsToUpdate: object[] = await qbToSelectForUpdate;
      rowsToUpdate.forEach((row) => {
        const updateColumnsSql = Object.entries(
          pick(
            row,
            replicatedColumns.filter((cl) => !primaryColumns.includes(cl)),
          ),
        )
          .map(([column, val]) => {
            let formattedValue = "'" + val + "'";
            if (isNil(val)) {
              formattedValue = 'NULL';
            }
            // типа колонки с датами
            if (column.includes('At')) {
              formattedValue = "'" + new Date(val).toISOString() + "'";
            }

            return `${snakeCase(column)} = ${formattedValue}`;
          })
          .join(',');

        const whereValuesClauseSql = primaryColumns
          .map((primary) => `'${row[primary]}'`)
          .join(',');

        const qbToUpdate = knex.raw(
          `UPDATE ${replicationName} SET ${updateColumnsSql} where (${wherePrimaryColumnsClause}) in (${whereValuesClauseSql})`,
        );
        qbsForUpdate.push(qbToUpdate);
      });
    }

    if (canDelete) {
      qbToDelete.delete();
    }

    if (env === 'production') {
      console.log(
        `${replicationName} - SQL_TO_SAVE:`,
        canInsert ? qbToSave.toSQL().toNative().sql : '',
      );
      console.log(
        `${replicationName} - SQL_TO_DELETE:`,
        canDelete ? qbToDelete.toSQL().toNative().sql : '',
      );
      console.log(
        `${replicationName} - SQL_TO_UPDATE:`,
        canUpdate && qbsForUpdate?.length
          ? qbsForUpdate.map((qb) => qb.toSQL().toNative().sql).join(';\n')
          : '',
      );
      continue;
    }

    if (canInsert) {
      await qbToSave;
    }
    if (canUpdate) {
      await Promise.all(qbsForUpdate);
    }
    if (canDelete) {
      await qbToDelete;
    }
  }
}
