import { Knex, knex } from 'knex';

import config from '../../../knexfile';
import { checkReplications } from './check-replications';
import { fixReplications } from './fix-replications';

type ReplicationCheckerCommand = 'run' | 'fix';

export async function replicationChecker(knex: Knex) {
  const command: ReplicationCheckerCommand = process
    .argv[2] as ReplicationCheckerCommand;

  if (command === 'run') {
    const result = await checkReplications(knex);
    console.table(result, [
      'replicationName',
      'originId',
      'replicationId',
      'wrongColumns',
    ]);
  }

  if (command === 'fix') {
    const replicationsBeforeFix = await checkReplications(knex);
    await fixReplications(knex, replicationsBeforeFix);
    console.table('REPLICATION-FIX EXECUTED');
  }
}

const bootstrap = () => replicationChecker(knex(config));

bootstrap();
