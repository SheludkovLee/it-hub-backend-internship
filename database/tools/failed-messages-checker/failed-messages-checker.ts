import { Knex, knex } from 'knex';

import config from '../../../knexfile';
import { checkFailedMessages } from './check-failed-messages';

export async function failedMessagesChecker(knex: Knex) {
  const result = await checkFailedMessages(knex);
  console.table(result, ['context', 'tokens', 'count']);
}

const bootstrap = () => failedMessagesChecker(knex(config));

bootstrap();
