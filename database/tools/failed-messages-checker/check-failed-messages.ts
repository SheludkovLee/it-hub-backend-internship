import 'reflect-metadata';

import { Knex } from 'knex';

export interface CheckFailedMessagesPayload {
  context: string;
  tokens: string[];
  count: number;
}

export async function checkFailedMessages(
  knex: Knex,
): Promise<CheckFailedMessagesPayload[]> {
  const messages: CheckFailedMessagesPayload[] = await knex
    .select(
      `message.context`,
      knex.raw(`array_agg(distinct message.token) as tokens`),
      knex.raw(`count(message.token)::int as count`),
    )
    .fromRaw(
      `
            (select imi.*, 'identity' as context from identity_message_inbox imi union
            select ldmi.*, 'learning_design' as context from learning_design_message_inbox ldmi union
            select lmi.*, 'learning' as context from learning_message_inbox lmi union
            select nmi.*, 'notification' as context from notification_message_inbox nmi union
            select ommi.*, 'organization_management' as context from organization_management_message_inbox ommi union
            select smi.*, 'schedule' as context from schedule_message_inbox smi
            ) message`,
    )
    .whereRaw(
      `message.status = 'FAILED' and date_occurred > (CURRENT_DATE - INTERVAL '1 day')`,
    )
    .groupBy('message.context');

  return messages;
}
