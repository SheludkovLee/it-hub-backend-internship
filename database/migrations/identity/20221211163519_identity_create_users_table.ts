import { buildTableName } from 'database/utils';
import type { Knex } from 'knex';

const tableName = buildTableName('identity', 'users');

export async function up(knex: Knex) {
  await knex.schema.createTable(tableName, (table) => {
    table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary();

    table.string('email').notNullable().index();
    table.string('first_name').nullable();
    table.string('middle_name').nullable();
    table.string('last_name').nullable();
    table.string('avatar').nullable();
    table.string('description').nullable();
    table.boolean('is_super_admin').notNullable();
    table.string('encrypted_password').nullable();

    table.timestamp('deleted_at').nullable();

    table.timestamps(true, true);
  });
}

export async function down(knex: Knex) {
  return knex.schema.dropTable(tableName);
}
