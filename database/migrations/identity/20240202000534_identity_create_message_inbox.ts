import { buildTableName } from 'database/utils';
import type { Knex } from 'knex';

const tableName = buildTableName('identity', 'message_inbox');

export async function up(knex: Knex) {
  await knex.schema.createTable(tableName, (t) => {
    t.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary();
    t.string('token').notNullable();
    t.string('trxId').notNullable();
    t.string('status').notNullable();
    t.jsonb('payload').notNullable();
    t.string('source_context').notNullable();
    t.jsonb('error').nullable();
    t.timestamp('dateOccurred').notNullable().defaultTo(knex.fn.now());
  });
}

export async function down(knex: Knex) {
  await knex.schema.dropTable(tableName);
}
