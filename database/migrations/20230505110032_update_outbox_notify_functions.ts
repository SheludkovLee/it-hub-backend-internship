import {buildTableName} from 'database/utils';
import type {Knex} from 'knex';

import {BoundedContext} from '../types';

const buildOutboxConfig = (context: BoundedContext) => {
    const tableName = buildTableName(context, 'message_outbox');
    const notificationFunctionName = `notify_${tableName}`;
    const notificationChannelName = tableName;

    return {tableName, notificationFunctionName, notificationChannelName};
};

const outboxConfigs = [
    buildOutboxConfig('identity'),
    buildOutboxConfig('learning'),
    buildOutboxConfig('learning_design'),
];

export async function up(knex: Knex) {
    const promises = outboxConfigs.map(async (config) => {
        await knex.schema.raw(`
      CREATE OR REPLACE FUNCTION ${config.notificationFunctionName}()
      RETURNS trigger AS
      $BODY$
        BEGIN
          PERFORM pg_notify('${config.notificationChannelName}', NEW.id::text);
          RETURN NULL;
        END;
      $BODY$
      LANGUAGE plpgsql VOLATILE COST 100;
    `);
    });
    await Promise.all(promises);
}

export async function down(knex: Knex) {
    const promises = outboxConfigs.map(async (config) => {
        await knex.schema.raw(`
      CREATE OR REPLACE FUNCTION ${config.notificationFunctionName}()
      RETURNS trigger AS
      $BODY$
        BEGIN
          PERFORM pg_notify('${config.notificationChannelName}', row_to_json(NEW)::text);
          RETURN NULL;
        END;
      $BODY$
      LANGUAGE plpgsql VOLATILE COST 100;
    `);
    });
    await Promise.all(promises);
}
