import {buildTableName} from 'database/utils';
import type {Knex} from 'knex';

// this name was renamed in rename_template_discipline_table migration
const tableName = buildTableName('learning_design', 'template_disciplines');

export async function up(knex: Knex) {
    return knex.schema.createTable(tableName, (table) => {
        table.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary();

        table.text('name').notNullable();
        table.text('description').notNullable().defaultTo(knex.raw(`''`));
        table.text('materials').nullable();

        table.float('study_hours_count').notNullable();
        table.float('max_score').notNullable();
        table.text('code').notNullable().defaultTo(knex.raw(`''`));

        table.timestamp('deleted_at');
        table.timestamps(true, true);
    });
}

export async function down(knex: Knex) {
    return knex.schema.dropTable(tableName);
}
