import { buildTableName } from 'database/utils';
import type { Knex } from 'knex';

const tableName = buildTableName('learning_design', 'message_outbox');
const notificationFunctionName = `notify_${tableName}`;
const notificationTriggerName = `notify_${tableName}`;
const notificationChannelName = tableName;

export async function up(knex: Knex) {
  await knex.schema.createTable(tableName, (t) => {
    t.uuid('id').defaultTo(knex.raw('uuid_generate_v4()')).primary();
    t.string('token').notNullable();
    t.string('trxId').notNullable();
    t.string('status').notNullable();
    t.jsonb('payload').notNullable();
    t.string('context').notNullable();
    t.timestamp('dateOccurred').notNullable().defaultTo(knex.fn.now());
  });

  await knex.schema.raw(`
    CREATE OR REPLACE FUNCTION ${notificationFunctionName}()
    RETURNS trigger AS
    $BODY$
      BEGIN
        PERFORM pg_notify('${notificationChannelName}', row_to_json(NEW)::text);
        RETURN NULL;
      END;
    $BODY$
    LANGUAGE plpgsql VOLATILE COST 100;
  `);

  await knex.schema.raw(`
    CREATE TRIGGER ${notificationTriggerName}
      AFTER INSERT
      ON "${tableName}"
      FOR EACH ROW
    EXECUTE PROCEDURE ${notificationFunctionName}();
  `);
}

export async function down(knex: Knex) {
  await knex.schema.dropTable(tableName);
  await knex.schema.raw(`
    DROP FUNCTION IF EXISTS ${notificationFunctionName}();
  `);
  await knex.schema.raw(`
    DROP TRIGGER IF EXISTS ${notificationTriggerName} ON "${tableName}"
  `);
}
