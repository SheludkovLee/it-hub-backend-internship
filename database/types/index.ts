import { BOUNDED_CONTEXTS } from '../constants';

type BoundedContextTuple = typeof BOUNDED_CONTEXTS;
type BoundedContext = BoundedContextTuple[number];

export { BoundedContext };
