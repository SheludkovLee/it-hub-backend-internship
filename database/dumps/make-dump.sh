echo "⛏️ Открываем тунель в приватную подсеть БД..."

read -p "Введите путь до ssh ключа для продового сервера " ssh_key_path

ssh -f -N -M -S database/dumps/ssh-socket-file -L 7777:rc1a-xrlths9rpi2rfbbi.mdb.yandexcloud.net:6432 gerasimenko-d@51.250.80.29 -p 18541 -i $ssh_key_path -v

sleep 2

echo "✅ Тунель открыт"

echo "🤩 Выгружаем дамп из БД"

read -p "Введите пароль от продовой БД " prod_pg_password

docker run --network="host" --rm -e PGPASSWORD=$prod_pg_password postgres:15 pg_dump --host=host.docker.internal --port=7777 --username=ithubpostgresqlro -Fc -a ithubdb  > database/dumps/completed-dumps/ithubdb.dump

echo "🏠 Дамп выгружен, ищите его в database/dumps/completed-dumps"

echo "🧨 Закрываем тунель"

ssh -S database/dumps/ssh-socket-file -O exit gerasimenko-d@51.250.80.29

echo "🤵 Тунель закрыт. Bye bye..."