echo "🤑 Начинаем накатывать дамп на локальную БД"

echo "🧱 Создаем отдельную БД под дамп, название: ithubdb."

PGPASSWORD="ithub" createdb --host=localhost --port=5432 --username=ithub ithubdb

echo "🧑‍💻 БД создана"

echo "🕊️ Прогоняем миграции для новой БД, создаем схему"

export POSTGRES_DB_NAME=ithubdb && yarn migrate:latest

echo "🥰 Миграции прогнали"

echo "🫵 Восстанавливаем дамп в новую БД."

PGPASSWORD="ithub" pg_restore --host=localhost --port=5432 --dbname=ithub --username=ithub --create -v database/dumps/completed-dumps/ithubdb.dump

echo "🦸‍♂️ Дамп восстановлен. Название новой БД ithubdb, теперь ты можешь к ней подключиться"