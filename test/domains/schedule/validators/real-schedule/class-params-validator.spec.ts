import {
  IClass,
  ILearningGroup,
  ISuborganization,
} from 'src/domains/schedule/validators/interfaces';
import { ClassParamsValidator } from 'src/domains/schedule/validators/real-schedule';
import { UUID } from 'src/libs/domain';

describe('ClassParamsValidator', () => {
  describe('updateLearningGroup', () => {
    it('обновление учебной группы и пересчет ошибок, при увеличении дневной нагрузки до допустимой', async () => {
      const fakeSuborganization: ISuborganization = {
        id: UUID.generate().value,
        organizationId: UUID.generate().value,
      };

      const learningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 2,
        suborganizationId: '1',
        studentsCount: 0,
      };

      const classes = [
        {
          id: '1',
          from: new Date(2023, 4, 1, 9, 0),
          to: new Date(2023, 4, 1, 10, 0),
          learningGroupId: '1',
          suborganizationId: '1',
        },
        {
          id: '2',
          from: new Date(2023, 4, 1, 10, 0),
          to: new Date(2023, 4, 1, 11, 0),
          learningGroupId: '1',
          suborganizationId: '1',
        },
      ];

      const validator = ClassParamsValidator.create({
        suborganization: fakeSuborganization,
        learningGroup,
        isNewClass: true,
        classes,
        from: new Date(2023, 4, 1, 11, 0),
        to: new Date(2023, 4, 1, 12, 0),
      });

      await validator.validate();

      expect(validator.errors).toEqual({
        learningGroup: ['Превышена дневная нагрузка учебной группы'],
      });

      const updatedLearningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 3,
        suborganizationId: '1',
        studentsCount: 0,
      };

      validator.updateLearningGroup({
        learningGroup: updatedLearningGroup,
      });

      await validator.validate();

      expect(validator.errors).toEqual({});
    });

    it('обновление учебной группы и пересчет ошибок, при увеличении дневной нагрузки до допустимой', async () => {
      const fakeSuborganization: ISuborganization = {
        id: UUID.generate().value,
        organizationId: UUID.generate().value,
      };

      const learningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 2,
        suborganizationId: '1',
        studentsCount: 0,
      };

      const classes: IClass[] = [
        {
          id: '1',
          from: new Date(2023, 4, 1, 9, 0),
          to: new Date(2023, 4, 1, 10, 0),
          learningGroupId: '1',
          suborganizationId: '1',
          errors: {
            learningGroup: ['Превышена дневная нагрузка учебной группы'],
          },
        },
        {
          id: '2',
          from: new Date(2023, 4, 1, 9, 0),
          to: new Date(2023, 4, 1, 10, 0),
          learningGroupId: '1',
          suborganizationId: '1',
          errors: {
            learningGroup: ['Превышена дневная нагрузка учебной группы'],
          },
        },
        {
          id: '3',
          from: new Date(2023, 4, 1, 9, 0),
          to: new Date(2023, 4, 1, 10, 0),
          learningGroupId: '1',
          suborganizationId: '1',
          errors: {
            learningGroup: ['Превышена дневная нагрузка учебной группы'],
          },
        },
      ];

      const validator = ClassParamsValidator.create({
        suborganization: fakeSuborganization,
        learningGroup,
        isNewClass: false,
        from: new Date(2023, 4, 1, 9, 0),
        to: new Date(2023, 4, 1, 10, 0),
        classes,
      });

      await validator.validate();

      expect(validator.errors).toEqual({
        learningGroup: ['Превышена дневная нагрузка учебной группы'],
      });

      const updatedLearningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 3,
        suborganizationId: '1',
        studentsCount: 0,
      };

      validator.updateLearningGroup({
        learningGroup: updatedLearningGroup,
      });

      await validator.validate();

      expect(validator.props.learningGroup).toEqual(updatedLearningGroup);
      expect(validator.errors).toEqual({});
    });

    it('обновление учебной группы и пересчет ошибок, при уменьшении дневной нагрузки', async () => {
      const fakeSuborganization: ISuborganization = {
        id: UUID.generate().value,
        organizationId: UUID.generate().value,
      };

      const learningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 3,
        suborganizationId: '1',
        studentsCount: 0,
      };

      const classes: IClass[] = [
        {
          id: '1',
          from: new Date(2023, 4, 1, 9, 0),
          to: new Date(2023, 4, 1, 10, 0),
          learningGroupId: learningGroup.id,
          suborganizationId: '1',
        },
        {
          id: '2',
          from: new Date(2023, 4, 1, 9, 0),
          to: new Date(2023, 4, 1, 10, 0),
          learningGroupId: learningGroup.id,
          suborganizationId: '1',
        },
      ];

      const validator = ClassParamsValidator.create({
        suborganization: fakeSuborganization,
        learningGroup,
        isNewClass: true,
        classes,
        from: new Date(2023, 4, 1, 9, 0),
        to: new Date(2023, 4, 1, 10, 0),
      });

      await validator.validate();

      expect(validator.errors).toEqual({});

      const updatedLearningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 2,
        suborganizationId: '1',
        studentsCount: 0,
      };

      validator.updateLearningGroup({
        learningGroup: updatedLearningGroup,
      });

      await validator.validate();

      expect(validator.props.learningGroup).toEqual(updatedLearningGroup);
      expect(validator.errors).toEqual({
        learningGroup: ['Превышена дневная нагрузка учебной группы'],
      });
    });

    it('удаление занятия учебной группы и пересчет ошибок', async () => {
      const fakeSuborganization: ISuborganization = {
        id: UUID.generate().value,
        organizationId: UUID.generate().value,
      };

      const learningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 2,
        suborganizationId: '1',
        studentsCount: 0,
      };

      const classes: IClass[] = [
        {
          id: '1',
          from: new Date(2023, 4, 1, 9, 0),
          to: new Date(2023, 4, 1, 10, 0),
          learningGroupId: '1',
          suborganizationId: '1',
          errors: {
            learningGroup: ['Превышена дневная нагрузка учебной группы'],
          },
        },
        {
          id: '2',
          from: new Date(2023, 4, 1, 9, 0),
          to: new Date(2023, 4, 1, 10, 0),
          learningGroupId: '1',
          suborganizationId: '1',
          errors: {
            learningGroup: ['Превышена дневная нагрузка учебной группы'],
          },
        },
        {
          id: '3',
          from: new Date(2023, 4, 1, 9, 0),
          to: new Date(2023, 4, 1, 10, 0),
          learningGroupId: '1',
          suborganizationId: '1',
          errors: {
            learningGroup: ['Превышена дневная нагрузка учебной группы'],
          },
        },
      ];

      let validator = ClassParamsValidator.create({
        suborganization: fakeSuborganization,
        learningGroup,
        isNewClass: false,
        classes,
        from: new Date(2023, 4, 1, 9, 0),
        to: new Date(2023, 4, 1, 10, 0),
      });

      await validator.validate();

      expect(validator.errors).toEqual({
        learningGroup: ['Превышена дневная нагрузка учебной группы'],
      });

      classes.pop();

      validator = ClassParamsValidator.create({
        suborganization: fakeSuborganization,
        learningGroup,
        isNewClass: false,
        classes,
        from: new Date(2023, 4, 1, 9, 0),
        to: new Date(2023, 4, 1, 10, 0),
      });

      await validator.validate();

      expect(validator.errors).toEqual({});
    });
  });
});
