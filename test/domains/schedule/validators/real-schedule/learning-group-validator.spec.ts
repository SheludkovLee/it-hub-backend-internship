import { LearningGroupValidator } from 'src/domains/schedule/validators/real-schedule/learning-group-validator';

describe('LearningGroupValidator', () => {
  it('Превышена дневная нагрузка учебной группы для нового занятия', async () => {
    const classes = [
      {
        id: '1',
        from: new Date('2023-05-26T09:00:00'),
        to: new Date('2023-05-26T10:00:00'),
        learningGroupId: '1',
        suborganizationId: '1',
      },
      {
        id: '2',
        from: new Date('2023-05-26T10:00:00'),
        to: new Date('2023-05-26T11:00:00'),
        learningGroupId: '1',
        suborganizationId: '1',
      },
    ];

    const learningGroup = {
      id: '1',
      maxClassesPerDay: 2,
      suborganizationId: '1',
      studentsCount: 0,
    };

    const validator = new LearningGroupValidator({
      learningGroup,
      isNewClass: true,
      classes,
      from: new Date('2023-05-26T11:00:00'),
      to: new Date('2023-05-26T12:00:00'),
    });

    await validator.validate();

    expect(validator.errors).toContain(
      'Превышена дневная нагрузка учебной группы',
    );
  });

  it('Превышена дневная нагрузка учебной группы для существующего занятия', async () => {
    const classes = [
      {
        id: '1',
        from: new Date('2023-05-26T09:00:00'),
        to: new Date('2023-05-26T10:00:00'),
        learningGroupId: '1',
        suborganizationId: '1',
      },
      {
        id: '2',
        from: new Date('2023-05-26T10:00:00'),
        to: new Date('2023-05-26T11:00:00'),
        learningGroupId: '1',
        suborganizationId: '1',
      },
    ];

    const learningGroup = {
      id: '1',
      maxClassesPerDay: 2,
      suborganizationId: '1',
      studentsCount: 0,
    };

    const validator = new LearningGroupValidator({
      learningGroup,
      isNewClass: false,
      classes,
      from: new Date('2023-05-26T11:00:00'),
      to: new Date('2023-05-26T12:00:00'),
    });

    await validator.validate();

    expect(validator.errors).toStrictEqual([]);
  });

  // it('У учебной группы в этот день есть занятия на других площадках', () => {
  //   const firstBuildingAreaId = '1';
  //   const secondBuildingAreaId = '2';

  //   const learningGroup = {
  //     id: '1',
  //     maxClassesPerDay: 2,
  //     suborganizationId: '1',
  //   };

  //   const classes = [
  //     {
  //       id: '1',
  //       from: new Date('2023-05-26T09:00:00'),
  //       to: new Date('2023-05-26T10:00:00'),
  //       learningGroupId: '1',
  //       classroom: {
  //         id: '1',
  //         capacity: 20,
  //         buildingAreaId: firstBuildingAreaId,
  //       },
  //     },
  //   ];

  //   const validator = new LearningGroupValidator({
  //     learningGroup,
  //     isNewClass: true,
  //     buildingAreaId: secondBuildingAreaId,
  //     classes,
  //   });

  //   expect(validator.errors).toContain(
  //     'У учебной группы в этот день есть занятия на других площадках',
  //   );
  // });
});
