import {
  ILearningGroup,
  IModule,
  ISuborganization,
} from 'src/domains/schedule/validators/interfaces';
import { ModuleClassParamsValidator } from 'src/domains/schedule/validators/module';
import { UUID } from 'src/libs/domain';

describe('ModuleClassParamsValidator', () => {
  describe('updateLearningGroup', () => {
    it('обновление учебной группы и пересчет ошибок, при увеличении дневной нагрузки до допустимой', async () => {
      const fakeSuborganization: ISuborganization = {
        id: UUID.generate().value,
        organizationId: UUID.generate().value,
      };

      const learningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 2,
        suborganizationId: '1',
        studentsCount: 0,
      };

      const module: IModule = {
        id: '1',
        durationInWeeks: 1,
        suborganizationId: '1',
        classes: [
          {
            id: '1',
            from: '09:00',
            to: '10:00',
            dayIndex: 0,
            learningGroupId: '1',
            moduleId: '1',
          },
          {
            id: '2',
            from: '10:00',
            to: '11:00',
            dayIndex: 0,
            learningGroupId: '1',
            moduleId: '1',
          },
        ],
      };

      const validator = ModuleClassParamsValidator.create({
        suborganization: fakeSuborganization,
        learningGroup,
        isNewClass: true,
        module,
        dayIndex: 0,
      });

      await validator.validate();

      expect(validator.errors).toEqual({
        learningGroup: ['Превышена дневная нагрузка учебной группы'],
      });

      const updatedLearningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 3,
        suborganizationId: '1',

        studentsCount: 0,
      };

      validator.updateLearningGroup({
        learningGroup: updatedLearningGroup,
      });

      await validator.validate();

      expect(validator.props.learningGroup).toEqual(updatedLearningGroup);
      expect(validator.errors).toEqual({});
    });

    it('обновление учебной группы и пересчет ошибок, при увеличении дневной нагрузки до допустимой', async () => {
      const fakeSuborganization: ISuborganization = {
        id: UUID.generate().value,
        organizationId: UUID.generate().value,
      };

      const learningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 2,
        suborganizationId: '1',
        studentsCount: 0,
      };

      const module: IModule = {
        id: '1',
        durationInWeeks: 1,
        suborganizationId: '1',
        classes: [
          {
            id: '1',
            from: '09:00',
            to: '10:00',
            dayIndex: 0,
            learningGroupId: '1',
            moduleId: '1',
            errors: {
              learningGroup: ['Превышена дневная нагрузка учебной группы'],
            },
          },
          {
            id: '2',
            from: '10:00',
            to: '11:00',
            dayIndex: 0,
            learningGroupId: '1',
            moduleId: '1',
            errors: {
              learningGroup: ['Превышена дневная нагрузка учебной группы'],
            },
          },
          {
            id: '3',
            from: '11:00',
            to: '12:00',
            dayIndex: 0,
            learningGroupId: '1',
            moduleId: '1',
            errors: {
              learningGroup: ['Превышена дневная нагрузка учебной группы'],
            },
          },
        ],
      };

      const validator = ModuleClassParamsValidator.create({
        suborganization: fakeSuborganization,
        learningGroup,
        isNewClass: false,
        module,
        dayIndex: 0,
      });

      await validator.validate();

      expect(validator.errors).toEqual({
        learningGroup: ['Превышена дневная нагрузка учебной группы'],
      });

      const updatedLearningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 3,
        suborganizationId: '1',

        studentsCount: 0,
      };

      validator.updateLearningGroup({
        learningGroup: updatedLearningGroup,
      });

      await validator.validate();

      expect(validator.props.learningGroup).toEqual(updatedLearningGroup);
      expect(validator.errors).toEqual({});
    });

    it('обновление учебной группы и пересчет ошибок, при уменьшении дневной нагрузки', async () => {
      const fakeSuborganization: ISuborganization = {
        id: UUID.generate().value,
        organizationId: UUID.generate().value,
      };

      const learningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 3,
        suborganizationId: '1',
        studentsCount: 0,
      };

      const module: IModule = {
        id: '1',
        durationInWeeks: 1,
        suborganizationId: '1',
        classes: [
          {
            id: '1',
            from: '09:00',
            to: '10:00',
            dayIndex: 0,
            learningGroupId: '1',
            moduleId: '1',
          },
          {
            id: '2',
            from: '10:00',
            to: '11:00',
            dayIndex: 0,
            learningGroupId: '1',
            moduleId: '1',
          },
        ],
      };

      const validator = ModuleClassParamsValidator.create({
        suborganization: fakeSuborganization,
        learningGroup,
        isNewClass: true,
        module,
        dayIndex: 0,
      });

      await validator.validate();

      expect(validator.errors).toEqual({});

      const updatedLearningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 2,
        suborganizationId: '1',
        studentsCount: 0,
      };

      validator.updateLearningGroup({
        learningGroup: updatedLearningGroup,
      });

      await validator.validate();

      expect(validator.props.learningGroup).toEqual(updatedLearningGroup);
      expect(validator.errors).toEqual({
        learningGroup: ['Превышена дневная нагрузка учебной группы'],
      });
    });

    it('удаление занятия учебной группы и пересчет ошибок', async () => {
      const fakeSuborganization: ISuborganization = {
        id: UUID.generate().value,
        organizationId: UUID.generate().value,
      };

      const learningGroup: ILearningGroup = {
        id: '1',
        maxClassesPerDay: 2,
        suborganizationId: '1',
        studentsCount: 0,
      };

      const module: IModule = {
        id: '1',
        durationInWeeks: 1,
        suborganizationId: '1',
        classes: [
          {
            id: '1',
            from: '09:00',
            to: '10:00',
            dayIndex: 0,
            learningGroupId: '1',
            moduleId: '1',
            errors: {
              learningGroup: ['Превышена дневная нагрузка учебной группы'],
            },
          },
          {
            id: '2',
            from: '10:00',
            to: '11:00',
            dayIndex: 0,
            learningGroupId: '1',
            moduleId: '1',
            errors: {
              learningGroup: ['Превышена дневная нагрузка учебной группы'],
            },
          },
          {
            id: '3',
            from: '11:00',
            to: '12:00',
            dayIndex: 0,
            learningGroupId: '1',
            moduleId: '1',
            errors: {
              learningGroup: ['Превышена дневная нагрузка учебной группы'],
            },
          },
        ],
      };

      let validator = ModuleClassParamsValidator.create({
        suborganization: fakeSuborganization,
        learningGroup,
        isNewClass: false,
        module,
        dayIndex: 0,
      });

      await validator.validate();

      expect(validator.errors).toEqual({
        learningGroup: ['Превышена дневная нагрузка учебной группы'],
      });

      module.classes.pop();

      validator = ModuleClassParamsValidator.create({
        suborganization: fakeSuborganization,
        learningGroup,
        isNewClass: false,
        module,
        dayIndex: 0,
      });

      await validator.validate();

      expect(validator.errors).toEqual({});
    });
  });
});
