import {
  ILearningGroup,
  IModule,
} from 'src/domains/schedule/validators/interfaces';
import { LearningGroupValidator } from 'src/domains/schedule/validators/module/learning-group-validator';

describe('LearningGroupValidator', () => {
  it('Превышена дневная нагрузка учебной группы для нового занятия', async () => {
    const learningGroup: ILearningGroup = {
      id: '1',
      maxClassesPerDay: 2,
      suborganizationId: '1',
      studentsCount: 0,
    };

    const module: IModule = {
      id: '1',
      durationInWeeks: 1,
      suborganizationId: '1',
      classes: [
        {
          id: '1',
          from: '09:00',
          to: '10:00',
          dayIndex: 0,
          learningGroupId: '1',
          moduleId: '1',
        },
        {
          id: '2',
          from: '10:00',
          to: '11:00',
          dayIndex: 0,
          learningGroupId: '1',
          moduleId: '1',
        },
      ],
    };

    const validator = new LearningGroupValidator({
      learningGroup,
      isNewClass: true,
      module,
      dayIndex: 0,
    });

    await validator.validate();

    expect(validator.errors).toContain(
      'Превышена дневная нагрузка учебной группы',
    );
  });

  it('Превышена дневная нагрузка учебной группы для существующего занятия', () => {
    const learningGroup: ILearningGroup = {
      id: '1',
      maxClassesPerDay: 2,
      suborganizationId: '1',
      studentsCount: 0,
    };

    const module: IModule = {
      id: '1',
      durationInWeeks: 1,
      suborganizationId: '1',
      classes: [
        {
          id: '1',
          from: '09:00',
          to: '10:00',
          dayIndex: 0,
          learningGroupId: '1',
          moduleId: '1',
        },
        {
          id: '2',
          from: '10:00',
          to: '11:00',
          dayIndex: 0,
          learningGroupId: '1',
          moduleId: '1',
        },
      ],
    };

    const validator = new LearningGroupValidator({
      learningGroup,
      isNewClass: false,
      module,
      dayIndex: 0,
    });

    expect(validator.errors).toStrictEqual([]);
  });

  it('У учебной группы в этот день есть занятия на других площадках', async () => {
    const learningGroup: ILearningGroup = {
      id: '1',
      maxClassesPerDay: 2,
      suborganizationId: '1',
      studentsCount: 0,
    };

    const firstBuildingAreaId = '1';
    const secondBuildingAreaId = '2';

    const module: IModule = {
      id: '1',
      durationInWeeks: 1,
      suborganizationId: '1',
      classes: [
        {
          id: '1',
          from: '09:00',
          to: '10:00',
          dayIndex: 0,
          learningGroupId: '1',
          classroom: {
            id: '1',
            capacity: 20,
            buildingAreaId: firstBuildingAreaId,
          },
          moduleId: '1',
        },
      ],
    };

    const validator = new LearningGroupValidator({
      learningGroup,
      isNewClass: true,
      buildingAreaId: secondBuildingAreaId,
      module,
      dayIndex: 0,
    });

    await validator.validate();

    expect(validator.errors).toContain(
      'У учебной группы в этот день есть занятия на других площадках',
    );
  });
});
