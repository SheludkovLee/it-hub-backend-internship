import { ClassroomValidator } from 'src/domains/schedule/validators/common';
import {
  IClassroom,
  ILearningGroup,
} from 'src/domains/schedule/validators/interfaces';

describe('ClassroomValidator', () => {
  it('Аудитория слишком мала для учебной группы', async () => {
    const learningGroup: ILearningGroup = {
      id: '1',
      maxClassesPerDay: 2,
      suborganizationId: '1',
      studentsCount: 2,
    };

    const classroom: IClassroom = {
      id: '1',
      buildingAreaId: '1',
      capacity: 1,
    };

    const validator = new ClassroomValidator({
      learningGroup,
      classroom,
    });

    await validator.validate();

    expect(validator.errors).toContain(
      'Аудитория слишком мала для учебной группы',
    );
  });
});
