import 'reflect-metadata';

import { UserEntity } from 'src/domains/identity/domain';
import { DateVO, UUID } from 'src/libs/domain';

describe('User domain entity', () => {
  let user: UserEntity;

  beforeEach(() => {
    user = new UserEntity({
      id: UUID.generate(),
      props: {
        email: 'example@mail.com',
        firstName: 'firstName',
        lastName: 'lastName',
      },
      createdAt: DateVO.now(),
      updatedAt: DateVO.now(),
    });
  });

  it('Update user profile', () => {
    const newProps = {
      firstName: 'firstName1',
      lastName: 'lastName1',
    };
    user.update(newProps);

    const newUserProps = user.getPropsCopy();

    expect(newUserProps.firstName).toBe(newProps.firstName);
    expect(newUserProps.lastName).toBe(newProps.lastName);
  });
});
