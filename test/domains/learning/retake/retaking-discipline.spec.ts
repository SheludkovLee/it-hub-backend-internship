import 'reflect-metadata';

import {
  RetakingDisciplineContentEntity,
  RetakingDisciplineContentKind,
  RetakingDisciplineEntity,
} from 'src/domains/learning/domain/entities';
import { UUID } from 'src/libs/domain';

describe('Дисциплна для пересдачи', () => {
  it('Создание дисциплины для пересдачи', () => {
    const disciplineId = UUID.generate();
    const retakingDisciplineId = UUID.generate();

    const discipline = RetakingDisciplineEntity.create({
      id: retakingDisciplineId,
      props: {
        disciplineId: disciplineId,
        countDaysForOpen: 3,
        content: [
          new RetakingDisciplineContentEntity({
            id: UUID.generate(),
            props: {
              name: 'Задание 1',
              retakingDisciplineId: retakingDisciplineId,
              maxScore: 10,
              body: '',
              order: 0,
              kind: RetakingDisciplineContentKind.TASK,
            },
          }),
          new RetakingDisciplineContentEntity({
            id: UUID.generate(),
            props: {
              name: 'Задание 2',
              retakingDisciplineId: retakingDisciplineId,
              maxScore: 10,
              body: '',
              order: 1,
              kind: RetakingDisciplineContentKind.TASK,
            },
          }),
          new RetakingDisciplineContentEntity({
            id: UUID.generate(),
            props: {
              name: 'Задание 3',
              retakingDisciplineId: retakingDisciplineId,
              maxScore: 10,
              body: '',
              order: 2,
              kind: RetakingDisciplineContentKind.INFO,
            },
          }),
        ],
      },
    });

    expect(discipline.id).toBe(retakingDisciplineId);
    expect(discipline.disciplineId).toBe(disciplineId);

    expect(discipline.content).toHaveLength(3);
  });

  it('Удаление контента из дисциплины для пересдачи', () => {
    const disciplineId = UUID.generate();
    const retakingDisciplineId = UUID.generate();

    const discipline = RetakingDisciplineEntity.create({
      id: retakingDisciplineId,
      props: {
        disciplineId: disciplineId,
        countDaysForOpen: 3,
        content: [
          new RetakingDisciplineContentEntity({
            id: UUID.generate(),
            props: {
              name: 'Задание 1',
              retakingDisciplineId: retakingDisciplineId,
              maxScore: 10,
              body: '',
              order: 0,
              kind: RetakingDisciplineContentKind.TASK,
            },
          }),
          new RetakingDisciplineContentEntity({
            id: UUID.generate(),
            props: {
              name: 'Задание 2',
              retakingDisciplineId: retakingDisciplineId,
              maxScore: 10,
              body: '',
              order: 1,
              kind: RetakingDisciplineContentKind.TASK,
            },
          }),
          new RetakingDisciplineContentEntity({
            id: UUID.generate(),
            props: {
              name: 'Задание 3',
              retakingDisciplineId: retakingDisciplineId,
              maxScore: 10,
              body: '',
              order: 2,
              kind: RetakingDisciplineContentKind.INFO,
            },
          }),
        ],
      },
    });

    expect(discipline.id).toBe(retakingDisciplineId);
    expect(discipline.disciplineId).toBe(disciplineId);

    expect(discipline.content).toHaveLength(3);

    discipline.upsertContent([]);

    expect(discipline.id).toBe(retakingDisciplineId);
    expect(discipline.disciplineId).toBe(disciplineId);

    expect(discipline.content).toHaveLength(0);
  });

  it('Обновление контента дисциплины для пересдачи', () => {
    const disciplineId = UUID.generate();
    const retakingDisciplineId = UUID.generate();

    const discipline = RetakingDisciplineEntity.create({
      id: retakingDisciplineId,
      props: {
        disciplineId: disciplineId,
        countDaysForOpen: 3,
        content: [
          new RetakingDisciplineContentEntity({
            id: UUID.generate(),
            props: {
              name: 'Задание 1',
              retakingDisciplineId: retakingDisciplineId,
              maxScore: 10,
              body: '',
              order: 0,
              kind: RetakingDisciplineContentKind.TASK,
            },
          }),
          new RetakingDisciplineContentEntity({
            id: UUID.generate(),
            props: {
              name: 'Задание 2',
              retakingDisciplineId: retakingDisciplineId,
              maxScore: 10,
              body: '',
              order: 1,
              kind: RetakingDisciplineContentKind.TASK,
            },
          }),
          new RetakingDisciplineContentEntity({
            id: UUID.generate(),
            props: {
              name: 'Задание 3',
              retakingDisciplineId: retakingDisciplineId,
              maxScore: 10,
              body: '',
              order: 2,
              kind: RetakingDisciplineContentKind.INFO,
            },
          }),
        ],
      },
    });

    expect(discipline.id).toBe(retakingDisciplineId);
    expect(discipline.disciplineId).toBe(disciplineId);

    expect(discipline.content).toHaveLength(3);

    discipline.upsertContent([
      {
        id: UUID.generate(),
        name: 'Задание 3',
        maxScore: 10,
        body: '',
        order: 0,
        kind: RetakingDisciplineContentKind.INFO,
      },
      {
        id: UUID.generate(),
        name: 'Задание 1',
        maxScore: 10,
        body: '',
        order: 2,
        kind: RetakingDisciplineContentKind.TASK,
      },
    ]);

    expect(discipline.content).toHaveLength(2);

    expect(discipline.content[0].name).toBe('Задание 3');
    expect(discipline.content[0].order).toBe(0);

    expect(discipline.content[1].name).toBe('Задание 1');
    expect(discipline.content[1].order).toBe(1);
  });
});
