import 'reflect-metadata';

import { RetakeEntity } from 'src/domains/learning/domain/entities';
import { UUID } from 'src/libs/domain';

describe('Пересдача', () => {
  it('Создание пересдачи', () => {
    const disciplineId = UUID.generate();
    const contentFillingTeacherId = UUID.generate();

    const retake = new RetakeEntity({
      id: UUID.generate(),
      props: {
        disciplineId: disciplineId,
        contentFillingTeacherId: contentFillingTeacherId,
      },
    });

    expect(retake.disciplineId).toBeDefined();
    expect(retake.contentFillingTeacherId).toBeDefined();
    expect(retake.id).toBeDefined();
  });

  it('Поставить оценку студенту за пересдачу', () => {
    const disciplineId = UUID.generate();

    const studentId = UUID.generate();
    const score = 7;
    const maxScore = 10;
    const scoreInPercent = (score / maxScore) * 100;

    const retake = RetakeEntity.create({
      disciplineId: disciplineId,
    });

    expect(retake.getPropsCopy().disciplineScores).toHaveLength(0);
    expect(retake.disciplineId).toBeDefined();
    expect(retake.id).toBeDefined();

    retake.enterRetakeScore(studentId, score, maxScore);

    expect(retake.getPropsCopy().disciplineScores).toHaveLength(1);

    let studentScore = retake.getPropsCopy().disciplineScores[0];

    expect(studentScore.getPropsCopy().disciplineId).toBe(retake.disciplineId);
    expect(studentScore.getPropsCopy().retakeScoreInPercent).toBe(
      scoreInPercent,
    );

    retake.clearRetakeScore(studentId);

    studentScore = retake.getPropsCopy().disciplineScores[1];

    expect(studentScore.getPropsCopy().disciplineId).toBe(retake.disciplineId);
    expect(studentScore.getPropsCopy().retakeScoreInPercent).toBe(null);
  });
});
