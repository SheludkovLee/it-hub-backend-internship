import 'reflect-metadata';

import {
  RetakingGroupEntity,
  StudentRetakingGroupEntity,
} from 'src/domains/learning/domain/entities';
import { CompositeUUID, UUID } from 'src/libs/domain';

describe('Группа студентов на пересдачу', () => {
  it('Создание группы студентов на пересдачу', () => {
    const studentInGroupCount = 5;
    const retakers = [];
    const retakingGroupId = UUID.generate();

    for (let i = 0; i < studentInGroupCount; ++i) {
      const studentId = UUID.generate();

      retakers.push(
        new StudentRetakingGroupEntity({
          id: CompositeUUID.create(studentId.value, retakingGroupId.value),
          props: {
            studentId: studentId,
            retakingGroupId: retakingGroupId,
            isRespectfulReason: false,
          },
        }),
      );
    }

    const group = new RetakingGroupEntity({
      id: retakingGroupId,
      props: {
        name: 'Группа 1',
        disciplineId: UUID.generate(),
        students: retakers,
      },
    });

    expect(group.disciplineId).toBeDefined();
    expect(group.students).toHaveLength(studentInGroupCount);
  });
});
