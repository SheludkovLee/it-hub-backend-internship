install-deps:
	yarn install

start-dev:
	yarn start:dev

start-prod:
	yarn start:prod

prepare-env-file:
	cp .env.example .env

dc-up:
	docker-compose up --abort-on-container-exit

migrate-make:
	yarn migrate:make $(name)

migrate-make-identity:
	yarn migrate:make:identity identity_$(name)

migrate-make-learning:
	yarn migrate:make:learning learning_$(name)

migrate-make-learning-design:
	yarn migrate:make:learning-design learning_design_$(name)

migrate-make-organization-management:
	yarn migrate:make:organization-management organization_management_$(name)

migrate-make-role-system:
	yarn migrate:make:role-system role_system_$(name)

migrate-make-schedule:
	yarn migrate:make:schedule schedule_$(name)

migrate-make-cross-context:
	yarn migrate:make:cross-context cross_context_$(name)

migrate-latest:
	yarn migrate:latest

migrate-rollback:
	yarn migrate:rollback

seed-make:
	yarn seed:make $(name)

seed-run:
	yarn seed:run

command:
	yarn generate:command

query-resolver:
	yarn generate:query-resolver

mutation-resolver:
	yarn generate:mutation-resolver

dao:
	yarn generate:dao

aggregate-repository:
	yarn generate:aggregate-repository

entity-repository:
	yarn generate:entity-repository

event:
	yarn generate:event

message-handler:
	yarn generate:message-handler

event-mapper:
	yarn generate:event-mapper

query:
	yarn generate:query

clickhouse-entity:
	yarn generate:clickhouse-entity

clickhouse-scraper:
	yarn generate:scraper
